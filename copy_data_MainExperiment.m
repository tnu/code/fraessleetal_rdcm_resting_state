function copy_data_MainExperiment(site)
% Copies all the preprocessed data (fmriprep & mriqc) for the BSNIP analysis
% from the project folder to a defined folder (typically on scratch).
%
% Input:
%   site        -- site to be copied
% 
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2020
% Copyright 2020 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% List of all recording sites 
Site_List = {'Baltimore','Chicago','Dallas','Hartford'};


% specified sites
if ( isempty(site) )
    site_analyze = 1:length(Site_List);
else
    site_analyze = site;
end


% iterate over sites
for site = site_analyze
    
    % copy exclusion lists
    if ( site == 1 )
        
        % copy exclusion files
        copyfile(fullfile(FilenameInfo.LongTermStorage_Path,'exclusions*txt'),FilenameInfo.Data_Path)
        
        % check if folder exists
        if ( ~exist(fullfile(FilenameInfo.Data_Path,'Information'),'dir') )
            mkdir(fullfile(FilenameInfo.Data_Path,'Information'))
        end
        
        % copy subject information files
        copyfile(fullfile(FilenameInfo.LongTermStorage_Path,'Information'),fullfile(FilenameInfo.Data_Path,'Information'))
        
    end
    
    % Output progress
    disp(['Copy data from site: ' Site_List{site}])
    
    % check if folder exists
    if ( ~exist(fullfile(FilenameInfo.Data_Path,Site_List{site}),'dir') )
        mkdir(fullfile(FilenameInfo.Data_Path,Site_List{site}))
    end
    
    % specify the foldername for respective site
    copyfile(fullfile(FilenameInfo.LongTermStorage_Path,Site_List{site}),fullfile(FilenameInfo.Data_Path,Site_List{site}))
	
end

end