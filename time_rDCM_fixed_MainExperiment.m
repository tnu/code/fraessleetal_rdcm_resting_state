function time_rDCM_fixed_MainExperiment(model,parcellation_type,subgroup)
% Evaluates the mean run time for inferring effective connectivity parameters 
% in a whole-brain network for the participants from the BSNIP study as 
% inferred using the regression dynamic causal modeling (rDCM) toolbox.
%
% Input:
%   model               -- model to be analyzed
%   parcellation_type   -- parcellation scheme: (1) Glasser 2016, (2) AAL, (3) Brainetome 2016, (4) Glasser 2016 & Buckner 2011
%   subgroup            -- (0) all participants, (1) restrict to healthy controls
% 
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2020
% Copyright 2020 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% close all figures
close all

% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% add path to several relevant toolboxes
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'PBAR')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'fdr_bh')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'cbrewer')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'append_pdfs')))

% analysis file- and foldernames
parcellations = {'Glasser2016','AAL','Brainnetome2016','Glasser2016_Buckner2011'};


% set the SPM path and the path to the experiment
foldername = fullfile(FilenameInfo.Data_Path,'grouplevel_dcm',parcellations{parcellation_type});


% load the results array
temp    = load(fullfile(foldername,'regressionDCM_connectome', ['PosteriorParameterEstimates_model' num2str(model) '.mat']));
results = temp.results;


% generate subject infromation if necessary
if ( ~exist(fullfile(FilenameInfo.Data_Path,'Information','Subject_Information.mat'),'file') )
    get_subject_information_MainExperiment()
end


% load the subject information
temp            = load(fullfile(FilenameInfo.Data_Path,'Information','Subject_Information.mat'));
Subject_type    = temp.Subject_type;


% load the subject information regarding confounds
temp2           = load(fullfile(FilenameInfo.Data_Path,'Information','Subject_Confounds.mat'));
Subject_age     = temp2.Subject_age;
Subject_gender  = temp2.Subject_gender;
Subject_site    = temp2.Subject_site;


% different tests
Subject_type_test = Subject_type;


% find the subjects that are not included in the analysis
Subject_type_test((~isfinite(results.A_Matrix_AllSubjects{1,1}))==1) = NaN;


% restrict to healthy controls only
if ( subgroup == 1 )
    Subject_type_test(Subject_type_test~=3) = NaN;
end


% asign the data
A_matrix_allSubjects     = results.A_Matrix_AllSubjects;
Time_rDCM_AllSubjects    = results.Time_rDCM_AllSubjects;


% check whether you have the right subjects (should not be necessary)
if ( length(temp.Subject_name) ~= length(A_matrix_allSubjects{1,1}) )
    for int = 1:length(results.AllSubjects)
        vector(int) = any(strcmp(results.AllSubjects(int),temp.Subject_name));
    end
    
    for int = 1:size(A_matrix_allSubjects,1)
        for int2 = 1:size(A_matrix_allSubjects,2)
            A_matrix_allSubjects{int,int2} = A_matrix_allSubjects{int,int2}(vector==1);
        end
    end
end

% excluded subjects
excluded_subjects = ~isfinite(A_matrix_allSubjects{1,1}) | ~isfinite(Subject_type_test) | ~isfinite(Subject_age) | ~isfinite(Subject_gender) | ~isfinite(Subject_site);


% display the number of subjects
disp(['Found: ' num2str(sum(excluded_subjects==0)) ' out of ' num2str(length(Subject_type_test)) ' subjects'])


% get the run-time statistics (rDCM)
mean_runtime_allSubjects = mean(Time_rDCM_AllSubjects{1}(excluded_subjects==0));
std_runtime_allSubjects  = std(Time_rDCM_AllSubjects{1}(excluded_subjects==0));

% get the range of run-times (rDCM)
max_runtime_allSubjects  = max(Time_rDCM_AllSubjects{1}(excluded_subjects==0));
min_runtime_allSubjects  = min(Time_rDCM_AllSubjects{1}(excluded_subjects==0));


% title
fprintf('\nComputational burden: \n')

% run-times
fprintf('Run-time (rDCM):\t mean = %4.2fs, std = %4.2fs, range = %4.2fs - %4.2fs \n',round(mean_runtime_allSubjects,2),round(std_runtime_allSubjects,2),round(min_runtime_allSubjects,2),round(max_runtime_allSubjects,2))

end