function estimate_rDCM_fixed_MainExperiment(site,subject,model,parcellation_type,scale)
% Infer whole-brain effective connectivity for the resting-state dataset of
% the B-SNIP1 study using regression DCM.
% 
% This function prepares the respective DCM model. Futher, it applies rDCM 
% to the model, which means assuming a fully (all-to-all) connected model,
% which is then pruned during model inversion to yield a sparse
% representation of network connectivity.
% 
% Input:
%   site                -- location: (1) Baltimore, (2) Chicago, (3) Dallas, (4) Hartford
%   subject             -- subject to analyze
%   model               -- model to analyze
%   parcellation_type   -- parcellation scheme: (1) Glasser 2016, (2) AAL, (3) Brainetome 2016, (4) Glasser 2016 & Buckner 2011
%   
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2020
% Copyright 2020 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% add the rDCM toolbox
addpath(genpath(fullfile(FilenameInfo.TAPAS_Path,'rDCM')))
addpath(FilenameInfo.SPM_Path)

% initialize spm
spm_jobman('initcfg')

% scaling or no scaling of data
scale_name = {'_noScale',''};

% get the site list
Site_List = {'Baltimore','Chicago','Dallas','Hartford'};

% set the parcellation
parcellations = {'Glasser2016','AAL','Brainnetome2016','Glasser2016_Buckner2011'};


% asign the foldernames
pre_foldername = FilenameInfo.Data_Path;

% define the different names
foldername = fullfile(pre_foldername,Site_List{site},'derivatives','fmriprep');


% define the filename
filename = ['DCM_RestingState_model' num2str(model)];


% get the subject list
Subject_List = dir(fullfile(foldername,'sub*html'));


% site name
Site = Site_List{site};


% choose which subjects to analyze
if ( isempty(subject) )
    subject_analyze = 1:length(Subject_List);
else
    subject_analyze = subject;
end


% iterate overs subjects
for subject = subject_analyze
    
    % asign subject name
    try 
        Subject = Subject_List(subject).name(1:end-5);
        fprintf(['\nProcessing: ' Subject ' (' Site ')\n'])
    catch
        fprintf(['\nNo more subjects (' Site ')\n'])
        break
    end
    
    
    % clear relevant files
    clear DCM 
    clear output 
    clear options
    
    
    % define the standard option settings
    options.scale      = scale;
    options.estimateVL = 0;

    % asign the scale
    scale = options.scale;
    
    
    % check whether SPM exists
    if ( ~exist(fullfile(foldername, Subject, 'firstlevel', 'SPM.mat'),'file') )
        disp('Loading data...')
        disp('Found number of regions: 0 (skip)')
        continue
    end
    

    % load the SPM
    load(fullfile(foldername, Subject, 'firstlevel', 'SPM.mat'))

    % asign the data of the respective subject to the DCM
    VOI_foldername = fullfile(foldername, Subject, 'firstlevel', ['timeseries_VOI_' parcellations{parcellation_type}]);


    % get the ROI names that are not present in all subjects
    temp_ROI = load(fullfile(pre_foldername, 'VoI_coordinates', ['timeseries_VOI_' parcellations{parcellation_type}], 'MissingROIs.mat'));
    
    % remove ROIs that are not present in all subjects
    for ROI_int = 1:length(temp_ROI.allMissing_regions)
        if ( parcellation_type ~= 3 )
            delete(fullfile(VOI_foldername,['VOI_' temp_ROI.allMissing_regions{ROI_int}(1:(find(temp_ROI.allMissing_regions{ROI_int}=='_',1,'last'))) '*.mat']))
        else
            delete(fullfile(VOI_foldername,['VOI_*' temp_ROI.allMissing_regions{ROI_int}(5:(find(temp_ROI.allMissing_regions{ROI_int}=='_',1,'last'))) '*.mat']))
        end
    end
    
    % get the VOI file in the folder
    VOI_files = dir(fullfile(VOI_foldername, 'VOI*.mat'));

    % display progress
    disp('Loading data...')
    disp(['Found number of regions: ' num2str(length(VOI_files))])

    % load the VOI time series for all regions of interes                                                                                                                            
    for number_of_regions = 1:length(VOI_files)

        % load the files
        load(fullfile(VOI_foldername,VOI_files(number_of_regions).name),'xY');
        DCM.xY(number_of_regions) = xY;

    end

    % number of regions
    DCM.n = length(DCM.xY);

    % number of time points
    DCM.v = length(DCM.xY(1).u);

    % specify the TR
    DCM.Y.dt  = SPM.xY.RT;

    % specify the Y component of the DCM file
    DCM.Y.X0 = DCM.xY(1).X0;

    % asign the data to the Y structure
    for i = 1:DCM.n
        DCM.Y.y(:,i)  = DCM.xY(i).u;
        DCM.Y.name{i} = DCM.xY(i).name;
    end

    % define the covariance matrix
    DCM.Y.Q = spm_Ce(ones(1,DCM.n)*DCM.v);
    
    
    % specify empty driving input
    DCM.U.u     = zeros(size(DCM.Y.y,1)*16, 1);
    DCM.U.name  = {'null'};
    DCM.U.dt    = DCM.Y.dt/16;
    
    
    % DCM parameters
    DCM.delays = repmat(SPM.xY.RT/2,DCM.n,1);
    
    % get the echo time
    foldername_mriqc = fullfile(FilenameInfo.Data_Path,Site_List{site},'derivatives','mriqc',Subject,'func');
    fid              = fopen(fullfile(foldername_mriqc,[Subject '_task-rest_bold.json']));
    json_text        = textscan(fid,'%s');
    TE               = str2double(json_text{1}{9});

    % asign echo time
    DCM.TE = TE;
    
    
    % DCM options
    DCM.options.nonlinear  = 0;
    DCM.options.two_state  = 0;
    DCM.options.stochastic = 0;
    DCM.options.nograph    = 0;


    % load the structural connectome
    temp_adj         = load(fullfile(FilenameInfo.Parcellation_path, 'parcellation', parcellations{parcellation_type}, 'Adjacency_matrix.mat'));
    adjacency_matrix = temp_adj.adjacency_matrix;

    % get all regions where signal was detected
    vector          = 1:size(adjacency_matrix,1);
    include_regions = setdiff(vector,temp_ROI.allMissing_regions_ind);

    % get the adjacency matrix
    adjacency_matrix_cut = adjacency_matrix(include_regions,include_regions);
    adjacency_matrix_cut = adjacency_matrix_cut - diag(diag(adjacency_matrix_cut));
    
    
    % connectivity structure of respective model:
    %   Model 1: No driving inputs
    if ( model == 1 )
        DCM.a = adjacency_matrix_cut + eye(DCM.n);
        DCM.b = zeros(DCM.n,DCM.n,size(DCM.U.u,2));
        DCM.c = zeros(DCM.n,size(DCM.U.u,2));
        DCM.d = zeros(DCM.n,DCM.n,0);
    end


    % check whether the folder exists
    if ( ~exist(fullfile(foldername, Subject, 'firstlevel_dcm', parcellations{parcellation_type}),'dir') )
        mkdir(fullfile(foldername, Subject, 'firstlevel_dcm', parcellations{parcellation_type}))
    end


    % detrend and scale the data
    if ( options.scale )

        % detrend data
        DCM.Y.y = spm_detrend(DCM.Y.y);

        % scale data
        scale_factor   = max(max((DCM.Y.y))) - min(min((DCM.Y.y)));
        scale_factor   = 4/max(scale_factor,4);
        DCM.Y.y        = DCM.Y.y*scale_factor;

    end


    % set the prior type of the endogenous matrix
    DCM.options.wide_priors = 0;


    % create the directory
    if ( ~exist(fullfile(foldername, Subject, 'firstlevel_dcm', parcellations{parcellation_type}, ['regressionDCM' scale_name{scale+1} '_connectome']),'dir') )
        mkdir(fullfile(foldername, Subject, 'firstlevel_dcm', parcellations{parcellation_type}, ['regressionDCM' scale_name{scale+1} '_connectome']))
    end


    % display the progress
    disp(['Evaluating subject ' num2str(subject) ' - model ' num2str(model)])
    disp(' ')


    % shift the input regressor slightly
    options.u_shift = 0;
    options.filter_str = 4;


    % empirical analysis
    type = 'r';
    
    
    % get time
    currentTimer = tic;

    
    % run the rDCM analysis
    [output, options] = tapas_rdcm_estimate(DCM, type, options, 1);

    % output elapsed time
    time_rDCM = toc(currentTimer);
    disp(['Elapsed time is ' num2str(time_rDCM) ' seconds.'])


    % store the estimation time
    output.time_rDCM = time_rDCM;


    % create the directory
    if ( ~exist(fullfile(foldername, Subject, 'firstlevel_dcm', parcellations{parcellation_type}, ['regressionDCM' scale_name{scale+1} '_connectome']),'dir') )
        mkdir(fullfile(foldername, Subject, 'firstlevel_dcm', parcellations{parcellation_type}, ['regressionDCM' scale_name{scale+1} '_connectome']))
    end

    % save the estimated results
    if ( ~isempty(output) )
        save(fullfile(foldername, Subject, 'firstlevel_dcm', parcellations{parcellation_type}, ['regressionDCM' scale_name{scale+1} '_connectome'], [filename '_rDCM.mat']),'output','-v7.3')
    end

    % save the options file
    if ( ~isempty(options) )
        save(fullfile(foldername, Subject, 'firstlevel_dcm', parcellations{parcellation_type}, ['regressionDCM' scale_name{scale+1} '_connectome'], [filename '_options.mat']),'options')
    end

end

end
