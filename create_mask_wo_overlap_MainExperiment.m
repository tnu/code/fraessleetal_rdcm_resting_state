function create_mask_wo_overlap_MainExperiment(mode,mfile,mfolder)
% Generate volumes of masks used for the Dynamic Causal Models (DCMs) for 
% the B-SNIP dataset. Parameter estimates have been computed using 
% regression DCM (rDCM).
% 
% This function obtains a measure related to graph-theoretical indices and 
% asigns them to the respective parcel. This is then written as a nifti
% image. The nifti image can then be displayed using illustration tools 
% like the Human Connectome Workbench.
% 
% Input:
%   GTmatrix            -- matrix with graph-theoretical measures
%   
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2020
% Copyright 2020 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% add SPM to path
addpath(FilenameInfo.SPM_Path)

% initialize spm
spm_jobman('initcfg')


% get the folder containing the masks that one wishes to combine
switch mode
    case 2
        mask_folder_temp = fullfile('RSN','modes');
    case 3
        mask_folder_temp = fullfile('RSN','nodes');
end


% if mask file not defined
if ( isempty(mfile) )
    mfile = strrep(mask_folder_temp,'/','_');
end

% if mask folder not defined
if ( isempty(mfolder) )
    mfolder = fullfile(FilenameInfo.Data_Path,'Figures',strrep(mask_folder_temp,'/','_'),'VOI');
end


%complete mask folder
mask_folder = fullfile(FilenameInfo.Parcellation_path,mask_folder_temp);

% get all the mask files
all_regions = dir(fullfile(mask_folder, '*.nii'));


% create new folder for results
if ( ~exist(mfolder,'dir') )
    mkdir(mfolder)
end


% delete previous files
warning off
delete(fullfile(mfolder,[mfile '.nii']))
warning on


% all regions in the respective order
switch mode
    case 2
        Nr_regions = [3 2 4 1];
        ind_mask   = [1 2 3 4];
    case 3
        Nr_regions = [1 2 4 10 3 5 11 9 15 6 12 7 13 8 14];
        ind_mask   = [-15 -14 -13 -12 -5 -4 -3 2 3 10 11 12 13 14 15];
end


% calculate the volumes of the graph theoretical results
for region = 1:length(Nr_regions)
    
    disp(['Region: ' all_regions(Nr_regions(region)).name])

    % input image
    Vi = fullfile(mask_folder,all_regions(Nr_regions(region)).name);
    
    % output image
    Vo = fullfile(mfolder,all_regions(Nr_regions(region)).name);
    
    % set the flags
    flags.dmtx   = 0;
    flags.mask   = 0;
    flags.interp = 0;
    flags.dtype  = 4;
    
    
    % specify the expression
    f = [num2str(ind_mask(region)) ' .* (i1>0)'];
    
    % write the image
    Vo_new = spm_imcalc(Vi,Vo,f,flags);
    
    
    % create overall output image
    if ( region == 1 )
        
        % input image
        Vi = fullfile(mfolder,all_regions(Nr_regions(region)).name);
        
        % output image
        Vo = fullfile(mfolder,[mfile '.nii']);

        % set the flags
        flags.dmtx   = 0;
        flags.mask   = 0;
        flags.interp = 0;
        flags.dtype  = 4;

        % specify the expression
        f = 'i1';    

        % write the image
        Vo_new = spm_imcalc(Vi,Vo,f,flags);
        
    else

        % specify input and output image
        Vi = [cellstr(fullfile(mfolder,[mfile '.nii'])); cellstr(fullfile(mfolder,all_regions(Nr_regions(region)).name))];
        Vo = fullfile(mfolder,[mfile '.nii']);

        % specify the expression
        f = 'i1 + (i2 .* (i1 == 0))';
        
        % write the image
        Vo_new = spm_imcalc(Vi,Vo,f,flags);
        
    end
    
    % delete temporary files
    delete(fullfile(mfolder,all_regions(Nr_regions(region)).name))
    
end

% folder to a template with appropriate voxel size
template_folder = fullfile(FilenameInfo.Parcellation_path,'parcellation','Brainnetome2016');

% specify input and output image
Vi = [cellstr(fullfile(template_folder,'template.nii')); cellstr(fullfile(mfolder,[mfile '.nii']))];
Vo = fullfile(mfolder,[mfile '.nii']);

% specify the expression
f = 'i1 + i2 - i1';

% write the image to reslice to a lower resolution (speeds up illustration)
Vo_new = spm_imcalc(Vi,Vo,f,flags);

% line breaks
fprintf('\n\n')

end
