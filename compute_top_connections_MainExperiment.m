function matrix = compute_top_connections_MainExperiment(matrix_all,percentage,number,subjects_include)
% Takes an adjacency matrix and computes the top "percentage" connections
% either for each subject individually or for an average adjacency matrix.
% 
% Input:
%   matrix_all          -- adjacency matrix (either containing multiple
%                          subjects, one subject, or an average)
%   percentage          -- perceptage of top connections
%   number              -- number of top connections (alterantive to percetage)
%   subjects_include	-- which subjects to include
%   
% Output:
%   matrix              -- adjacency matrix containing only the top
%                          connections
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2019
% Copyright 2019 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% percentage of top connections
if ( isempty(percentage) && isempty(number) )
    percentage = 1;
elseif ( ~isempty(percentage) && isempty(number) )
    if ( percentage >= 1 )
        percentage = percentage / 100;
    end
end

% looking at single-subject or average adjacency matrices?
if ( ~iscell(matrix_all) )
    matrix_all_temp = matrix_all;
    matrix_all      = cell(size(matrix_all_temp));
    for int = 1:size(matrix_all_temp,1)
        for int2 = 1:size(matrix_all_temp)
            matrix_all{int,int2}(1) = matrix_all_temp(int,int2);
        end
    end
end


% analyze all subjects
if ( nargin < 4 )
    subjects_include = 1:length(matrix_all{1,1});
end

% create empty matrix
matrix_temp = NaN(size(matrix_all,1),size(matrix_all,2),length(subjects_include));

% iterate over subjects
for nr_sub = 1:length(subjects_include)
    
    % empty matrix
    matrix_sub = NaN(size(matrix_all));
    
    % subject-specific matrix
    for int = 1:size(matrix_all,1)
        for int2 = 1:size(matrix_all,2)
            matrix_sub(int,int2) = matrix_all{int,int2}(subjects_include(nr_sub));
        end
    end
    
    % find the threshold and the binary mask of top connections
    matrix_nodiag               = matrix_sub - diag(diag(matrix_sub));
    matrix_nodiag_list          = matrix_nodiag(:);
    matrix_nodiag_list_sorted   = sort(abs(matrix_nodiag_list),'descend');
    matrix_nodiag_list_sorted   = matrix_nodiag_list_sorted(matrix_nodiag_list_sorted~=0);
    if ( ~isempty(percentage) )
        threshold               = matrix_nodiag_list_sorted(round(length(matrix_nodiag_list_sorted)*percentage));
    else
        threshold               = matrix_nodiag_list_sorted(number);
    end
    mask_binary               	= abs(matrix_nodiag) >= threshold;

    % threshold the endogenous connectivity matrix
    matrix_temp(:,:,nr_sub)	= matrix_nodiag .* mask_binary + diag(diag(matrix_sub));
    
end

% average over subjects
matrix = mean(matrix_temp,3);

end