function check_indvidual_coordinates_MainExperiment(mode,parcellation_type,restrict_sample)
% Get the center coordinate of the regions of interest (ROIs) for each 
% subject individually from the BSNIP study. The function then plots a 3D
% image illustrating the location of the individual ROIs to visually
% inspect the coordinates for outliers.
% 
% Input:
%   mode                -- analysis: (1) saccade network, (2) resting state networks, (3) whole-brain network
%   parcellation_type   -- parcellation scheme: (1) modes (RSN), (2) nodes (RSN)
%                                               (1) Glasser 2016, (2) AAL, (3) Brainnetome 2016, (4) Glasser 2016 & Buckner 2011
%   restrict_sample     -- exlude subjects: (0) no, (1) yes
% 
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2019
% Copyright 2019 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% set the different parcellation options
switch mode    
    case 2
        parcellations = {'modes','nodes'};
    case 3
        parcellations = {'Glasser2016','AAL','Brainnetome2016','Glasser2016_Buckner2011'};
end


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end

% get the mode list
mode_List = {'timeseries_VOI_saccade',['timeseries_VOI_RSN_' parcellations{parcellation_type}],['timeseries_VOI_' parcellations{parcellation_type}]};
fig_List  = {'saccade/VOI',['RSN_' parcellations{parcellation_type} '/VOI'],[parcellations{parcellation_type} '/VOI']}; 

% set the paths
MATLAB_path = FilenameInfo.Matlab_Path;
foldername  = FilenameInfo.Data_Path;


% add relevant paths
addpath(fullfile(MATLAB_path,'bipolar_colormap'))


% close all figures
close all

% foldername for the center coordinates
VoI_foldername = fullfile(foldername,'VoI_coordinates',mode_List{mode});


% load the coordinates file
temp                  = load(fullfile(VoI_foldername,'VoI_CenterCoordinates_AllSubjects.mat'));
center_allregions     = temp.center_allregions;
center_allregions_new = cell(size(center_allregions,2),1);
Subject_List_all      = temp.Subject_List_all;

% get the excluded subjects
excluded_subjects = textread(fullfile(foldername,'exclusions.txt'),'%s');

% iterate over all subjects
vector = ones(length(Subject_List_all),1);
for int = 1:length(Subject_List_all)
    for int2 = 1:length(excluded_subjects)
        if ( strcmp(Subject_List_all{int},excluded_subjects{int2}) )
            vector(int) = 0;
        end
    end
end

% get the excluded subjects based on rDCM
excluded_subjects = textread(fullfile(foldername,'exclusions_rDCM.txt'),'%s');

% iterate over all subjects
for int = 1:length(Subject_List_all)
    for int2 = 1:length(excluded_subjects)
        if ( strcmp(Subject_List_all{int},excluded_subjects{int2}) )
            vector(int) = 0;
        end
    end
end


% find the indexes of the included subjects
index_vector = find(vector==1);


% get all subjects
allSubjects      = 1:size(center_allregions,1);

if ( ~restrict_sample )
    subjects_analyze = allSubjects;
else
    subjects_analyze = index_vector;
end


% re-order the data
for nr_subject = 1:length(subjects_analyze)
    
    % get subject number
    subject = subjects_analyze(nr_subject);
    
    % store the coordinates in new structure
    for region = 1:size(center_allregions,2)
        if ( ~isempty(center_allregions{subject,region}) & (isfinite(center_allregions{subject,region})) )
            center_allregions_new{region}(nr_subject,:) = center_allregions{subject,region};
        else
            center_allregions_new{region}(nr_subject,:) = [NaN NaN NaN];
        end
    end
end


% get all the subjects that should be included
missing_ROIs = NaN(size(center_allregions_new{1},1),length(center_allregions_new));

% for those subjects, find missing regions
for int = 1:size(missing_ROIs,1)
    for int2 = 1:size(missing_ROIs,2)
        if ( isempty(center_allregions_new{int2}(int,:)) )
            missing_ROIs(int,int2) = 2;
        else
            if ( ~any(isfinite(center_allregions_new{int2}(int,:))) )
                missing_ROIs(int,int2) = 1;
            else
                missing_ROIs(int,int2) = 0;
            end
        end
    end
end


% compute the number of regions present in all subjects
NR_ROIs = sum(sum(missing_ROIs,1)==0);
ind_ROIs = find(sum(missing_ROIs,1)~=0);



% check if figure folder exists
if ( ~exist(fullfile(foldername,'Figures',fig_List{mode}),'dir') )
    mkdir(fullfile(foldername,'Figures',fig_List{mode}))
else
    
    % delete previous plots of necessary
    if ( exist(fullfile(foldername,'Figures',fig_List{mode},'VOI_coordinates.ps'),'file') )
        delete(fullfile(foldername,'Figures',fig_List{mode},'VOI_coordinates.ps'))
    end
end


% plot the missing regions
figure('units','normalized','outerposition',[0 0 1 1]);
imagesc(missing_ROIs)
axis square
colormap('bipolar')
caxis([-1 1])
ylabel('subjects','FontSize',14)
xlabel('regions','FontSize',14)
title('Overview of ROIs','FontSize',14)
print(gcf, '-dpsc', fullfile(foldername,'Figures',fig_List{mode},'VOI_coordinates'), '-fillpage', '-append')


% get names of ROIs
switch mode
    case 1
        mask_info{1,1} = 'MFG';
        mask_info{2,1} = 'IPS_left';
        mask_info{3,1} = 'IPS_right';
        mask_info{4,1} = 'FEF_left';
        mask_info{5,1} = 'FEF_right';
    case 2
        mask_info = create_RSN_information_MainExperiment(parcellation_type);
    case 3
        mask_info = create_mask_information_MainExperiment(parcellation_type);
end

% get and store the missing regions
allMissing_regions     = mask_info(ind_ROIs);
allMissing_regions_ind = [];
for int = 1:length(ind_ROIs)
    if ( isempty(allMissing_regions_ind) )
        allMissing_regions_ind(1) = ind_ROIs(int);
    else
        allMissing_regions_ind(end+1) = ind_ROIs(int);
    end
	
    if ( mod(ind_ROIs(int),2) == 1)
        allMissing_regions_ind(end+1) = ind_ROIs(int)+1;
    else
        allMissing_regions_ind(end+1) = ind_ROIs(int)-1;
    end
end

allMissing_regions_ind = unique(allMissing_regions_ind);

% display the number of subjects in the restricted sample
fprintf('\nFound: %d subjects with %d regions (final: %d regions)\n',length(subjects_analyze),NR_ROIs,size(center_allregions,2)-length(allMissing_regions_ind))
fprintf(['Missing regions: ' repmat('%s | ',1,length(ind_ROIs)) '\n'], mask_info{ind_ROIs})


if ( ~isempty(allMissing_regions) && ~isempty(allMissing_regions_ind) )
    save(fullfile(VoI_foldername,'MissingROIs.mat'),'allMissing_regions','allMissing_regions_ind')
end


% open the figure
figure('units','normalized','outerposition',[0 0 1 1]);
rng(0);
NR_regions  = size(center_allregions,2);

% set the color code
if ( mode == 3 )
    a = colormap(jet(NR_regions/2));
    randomized_order = randperm(size(a,1));
    a = a(randomized_order,:);
    color_style = NaN(size(a,1)*2,size(a,2));
    color_style(1:2:end,:) = a;
    color_style(2:2:end,:) = a;
else
    a = colormap(jet(NR_regions));
    color_style = a;
end


% plot the location of the regions in a 3D plot
title('Individual Coordinates of ROIs')
for regions = 1:length(center_allregions_new)
    plot3(center_allregions_new{regions}(:,1),center_allregions_new{regions}(:,2),center_allregions_new{regions}(:,3),'.','Color',color_style(regions,:))
    hold on
end
view(-52,12)
xlabel('x [mm]')
ylabel('y [mm]')
zlabel('z [mm]')
xlim([-80 80])
ylim([-100 80])
zlim([-60 100])
axis square
print(gcf, '-dpsc', fullfile(foldername,'Figures',fig_List{mode},'VOI_coordinates'), '-fillpage', '-append')


% plot the location of the regions in the horizontal view
figure('units','normalized','outerposition',[0 0 1 1])
for regions = 1:length(center_allregions_new)
    plot(center_allregions_new{regions}(:,1),center_allregions_new{regions}(:,2),'.','Color',color_style(regions,:))
    hold on
%     plot(mean(center_allregions_new{regions}(vector,1)),mean(center_allregions_new{regions}(vector,2)),'+','Color',color_style(regions,:),'MarkerSize',10,'LineWidth',2)
end
xlabel('x [mm]','FontSize',12)
ylabel('y [mm]','FontSize',12)
xlim([-80 80])
ylim([-100 80])
axis square
title('Individual Coordinates of ROIs [horizontal]')


% plot the location of the regions in the sagital view
figure('units','normalized','outerposition',[0 0 1 1]);
for regions = 1:length(center_allregions_new)
    plot(center_allregions_new{regions}(:,2),center_allregions_new{regions}(:,3),'.','Color',color_style(regions,:))
    hold on
%     plot(mean(center_allregions_new{regions}(vector,2)),mean(center_allregions_new{regions}(vector,3)),'+','Color',color_style(regions,:),'MarkerSize',12,'LineWidth',3)
end
xlabel('y [mm]','FontSize',12)
ylabel('z [mm]','FontSize',12)
xlim([-100 80])
ylim([-60 100])
axis square
title('Individual Coordinates of ROIs [sagittal]')
print(gcf, '-dpsc', fullfile(foldername,'Figures',fig_List{mode},'VOI_coordinates'), '-fillpage', '-append')


% plot the location of the regions in the horizontal view
figure('units','normalized','outerposition',[0 0 1 1]);
if ( mode == 3 )
    for regions = 1:2:length(center_allregions_new)
        plot(mean([center_allregions_new{regions}(1,2) center_allregions_new{regions+1}(1,2)]),mean([center_allregions_new{regions}(1,3) center_allregions_new{regions+1}(1,3)]),'.','Color',color_style(regions,:))
        hold on
        if ( regions <= 360 )
            text(mean([center_allregions_new{regions}(1,2) center_allregions_new{regions+1}(1,2)]),mean([center_allregions_new{regions}(1,3) center_allregions_new{regions+1}(1,3)]),['R' num2str(regions) ': ' strrep(mask_info{regions,1}(1:end-5),'_','\_')],'FontSize',6)
        else
            text(mean([center_allregions_new{regions}(1,2) center_allregions_new{regions+1}(1,2)]),mean([center_allregions_new{regions}(1,3) center_allregions_new{regions+1}(1,3)]),['R' num2str(regions) ': Cereb' strrep(mask_info{regions,1}(end-5),'_','\_')],'FontSize',6)
        end
    end
else
    for regions = 1:length(center_allregions_new)
        plot(center_allregions_new{regions}(1,2),center_allregions_new{regions}(1,3),'.','Color',color_style(regions,:))
        hold on
        text(center_allregions_new{regions}(1,2),center_allregions_new{regions}(1,3),['R' num2str(regions) ': ' strrep(mask_info{regions,1},'_','\_')],'FontSize',6)
    end
end

xlabel('y [mm]','FontSize',12)
ylabel('z [mm]','FontSize',12)
xlim([-100 80])
ylim([-60 100])
axis square
title('Individual Coordinates of ROIs [sagittal]')    
print(gcf, '-dpsc', fullfile(foldername,'Figures',fig_List{mode},'VOI_coordinates'), '-fillpage', '-append')



% get all the regions
region_ind = 1:size(missing_ROIs,2);

% get the included regions
region_ind_cut = setdiff(region_ind,allMissing_regions_ind);


% convert into the BrainNet format
fileBrainNet = fullfile(foldername,'Figures',fig_List{mode},'VOI_coordinates.node');
if ( exist(fileBrainNet,'file') )
    delete(fileBrainNet)
end

% open an empty file
fileID = fopen(fileBrainNet,'w');

% get randomized color
rng(2406);
color_BrainNet = randperm(length(region_ind_cut)/2);

% write the mean coordinates
for region = 1:length(region_ind_cut)
    for subject = 1:2%size(center_allregions_new{region_ind_cut(region)},1)
        
        % get the coordiantes for each region in every subject
        x_coord = center_allregions_new{region_ind_cut(region)}(subject,1);
        y_coord = center_allregions_new{region_ind_cut(region)}(subject,2);
        z_coord = center_allregions_new{region_ind_cut(region)}(subject,3);
        
        % write the coordinate and set the color
        fprintf(fileID,[num2str(x_coord) '\t' num2str(y_coord) '\t' num2str(z_coord) '\t' num2str(color_BrainNet(ceil(region/2))) '\t' '0.5\n']);
    
    end
end

% close the node file again
fclose(fileID);


% reminder
fprintf('\n IMPORTANT: Need to change colormap dimension in BrainNet.m \n')

end
