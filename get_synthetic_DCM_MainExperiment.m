function get_synthetic_DCM_MainExperiment(dcm_variant)
% Obtains the estimates from the inversion of the synthetic DCMs based on 
% either regression DCM or spectral DCM. This function computes the 
% root mean squared error (RMSE, number of sign errors (SE) and the 
% correlation between true and estimated parameter values.
% 
% Input:
%   dcm_variant         -- DCM variant utilized for model inversion
%                           (1) regression DCM; (2) spectral DCM
% 
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2020
% Copyright 2020 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% add SPM and other toolboxed
addpath(FilenameInfo.SPM_Path)
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'PBAR')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'fdr_bh')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'cbrewer')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'append_pdfs')))


% initialize spm
spm_jobman('initcfg')


% method name
method_folder = {'regressionDCM','spectralDCM'};


% set the number of "synthetic" subjects
nr_simulations = 1:20;


% results arrays
RMSE_allSubjects = NaN(length(nr_simulations),4,3,3);
CORR_allSubjects = NaN(length(nr_simulations),4,3,3);
SE_allSubjects   = NaN(length(nr_simulations),4,3,3);


% TR counter
TR_counter = 0;

% iterate over true models
for TR = [2 1 0.5]
    
    % TR counter
    TR_counter = TR_counter + 1;
    
    % SNR counter
    SNR_counter = 0;
    
    for SNR = [0.5 1 3]
        
        % increase SNR counter
        SNR_counter = SNR_counter + 1;
        
        for model = 1:4

            % results folder
            results_folder = fullfile(FilenameInfo.Data_Path,'simulations_dcm','small_network',method_folder{dcm_variant},['TR_' num2str(TR) '_SNR_' num2str(SNR)]);

            % get all subjects
            for subject = nr_simulations

                % load the estimated model
                if ( subject < 10 )
                    temp = load(fullfile(results_folder, ['DCM_model_t' num2str(model) '_e' num2str(model) '_0' num2str(subject) '.mat']));
                else
                    temp = load(fullfile(results_folder, ['DCM_model_t' num2str(model) '_e' num2str(model) '_' num2str(subject) '.mat']));
                end

                % get the results
                if ( dcm_variant == 1 )
                    results = temp.output;
                elseif ( dcm_variant == 2)
                    results = temp.DCM;
                end

                % get the estimated parameters and remove diagonal
                estim_param = results.Ep.A + diag(NaN(1,size(results.Ep.A,1)));
                estim_param = estim_param(:);

                % get the true parameters and remove diagonal
                true_param = results.GT.Tp.A + diag(NaN(1,size(results.GT.Tp.A,1)));
                true_param = true_param(:);

                % find off-diagonal elements
                vector = isfinite(estim_param);

                % compute RMSE
                RMSEtemp = sqrt(mean((estim_param(vector) - true_param(vector)).^2));

                % compute correlation
                CORRtemp = corrcoef(estim_param(vector),true_param(vector));
                CORRtemp = CORRtemp(2,1);

                % compute the number of sign errors
                SEtemp = sum((estim_param(vector).*true_param(vector))<0);

                % asign the RMSE and correlation coefficient
                RMSE_allSubjects(subject,model,TR_counter,SNR_counter) = RMSEtemp;
                CORR_allSubjects(subject,model,TR_counter,SNR_counter) = CORRtemp;
                SE_allSubjects(subject,model,TR_counter,SNR_counter)   = SEtemp;

            end
        end
    end
end


% asign the results
results.RMSE_allSubjects = RMSE_allSubjects;
results.CORR_allSubjects = CORR_allSubjects;
results.SE_allSubjects   = SE_allSubjects;


% store the results
save(fullfile(FilenameInfo.Data_Path,'simulations_dcm','small_network',method_folder{dcm_variant},'Simulations.mat'),'results')

end
