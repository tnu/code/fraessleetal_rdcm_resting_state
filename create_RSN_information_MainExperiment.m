function mask_info = create_RSN_information_MainExperiment(parcellation_type)
% Create a cell array that contains all the relevant information on the 
% mask that have been created for the resting-state-networks (RSN) analysis
% using spectral dynamic causal modelling (spDCM). The masks are based on 
% masks provided by XXX.
%
% Input:
%   parcellation_type	-- parcellation scheme: (1) modes, (2) nodes
% 
% Output:
%   mask_info               -- relevant information of the masks
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2018
% Copyright 2018 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% specify the regions
switch parcellation_type
    case 1
        mask_info{1,1} = 'DMN';
        mask_info{2,1} = 'DAN';
        mask_info{3,1} = 'SAN';
        mask_info{4,1} = 'CEN';
    case 2
        mask_info{1,1}  = 'PCC';
        mask_info{2,1}  = 'aMPFC';
        mask_info{3,1}  = 'lAG';
        mask_info{4,1}  = 'rAG';
        mask_info{5,1}  = 'dACC';
        mask_info{6,1}  = 'lAI';
        mask_info{7,1}  = 'rAI';
        mask_info{8,1}  = 'laPFC';
        mask_info{9,1}  = 'raPFC';
        mask_info{10,1} = 'lFEF';
        mask_info{11,1} = 'rFEF';
        mask_info{12,1} = 'lIFG';
        mask_info{13,1} = 'rIFG';
        mask_info{14,1} = 'lIPS';
        mask_info{15,1} = 'rIPS';
end

% display how many regions where found
disp(['# of regions found: ' num2str(length(mask_info))]) 

end