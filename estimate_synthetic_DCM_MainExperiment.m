function estimate_synthetic_DCM_MainExperiment(dcm_variant,nr_simulations,TR,SNR)
% Run the model inversion of the synthetic DCMs for a given "synthetic"
% subject using either regression DCM or spectral DCM. For each subject,
% this function inverts data from each "true" model with all the models
% included in the model space. This allows testing parameter recovery 
% and model architecture recovery.
% 
% Input:
%   dcm_variant         -- DCM variant utilized for model inversion
%                           (1) regression DCM; (2) spectral DCM
%   nr_simulations      -- simulated variations of true model ("subjects")
%   TR                  -- repetition time
%   SNR                 -- signal-to-noise ratio
% 
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2020
% Copyright 2020 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end

% add SPM and rDCM toolbox to path
addpath(genpath(fullfile(FilenameInfo.TAPAS_Path,'rDCM')))
addpath(FilenameInfo.SPM_Path)


% initialize spm
spm_jobman('initcfg')


% method name
method_folder = {'regressionDCM','spectralDCM'};


% set the number of "synthetic" subjects
if ( isempty(nr_simulations) )
    nr_simulations = 1:20;
end


% iterate over "synthetic" subjects
for subject = nr_simulations

    % display progress
    disp(['-- Subject ' num2str(subject) ': Run Model inversion -----------------'])

    % iterate over true models
    for true_model = 1:4

        % get all the model and load the DCM
        files = dir(fullfile(FilenameInfo.Data_Path,'simulations_dcm','small_network','data',['TR_' num2str(TR) '_SNR_' num2str(SNR)],['DCM_model' num2str(true_model) '_*mat']));
        temp  = load(fullfile(FilenameInfo.Data_Path,'simulations_dcm','small_network','data',['TR_' num2str(TR) '_SNR_' num2str(SNR)],files(subject).name));
        DCM   = temp.DCM;

        % iterate over models to estimate
        for estimated_model = 1:4

            % display the progress
            disp(['Model inversion (true model ' num2str(true_model) ' & estimated model ' num2str(estimated_model) ')'])

            % get the structure of the estimated model
            files_temp = dir(fullfile(FilenameInfo.Data_Path,'simulations_dcm','small_network','data',['TR_' num2str(TR) '_SNR_' num2str(SNR)],['DCM_model' num2str(estimated_model) '_*mat']));
            DCM_temp   = load(fullfile(FilenameInfo.Data_Path,'simulations_dcm','small_network','data',['TR_' num2str(TR) '_SNR_' num2str(SNR)],files_temp(subject).name));
            
            % store ground-truth structure and parameter values in DCM
            DCM.GT.a    = DCM.a;
            DCM.GT.Tp   = DCM.Tp;
            
            % set the A-matrix of the to-be-estimated DCM
            DCM.a = DCM_temp.DCM.a;
            
            % remove fields
            try, DCM = rmfield(DCM,'M'); end
            try, DCM = rmfield(DCM,'Ep'); end
            
            
            % set the random seed to ensure there is no effect of loop
            rng(1111)
            
            
            % results folder
            results_folder = fullfile(FilenameInfo.Data_Path,'simulations_dcm','small_network',method_folder{dcm_variant},['TR_' num2str(TR) '_SNR_' num2str(SNR)]);
            
            
            % create results directory (one per true model)
            if ( ~exist(results_folder,'dir') )
                mkdir(results_folder)
            end
            
            
            % invert the models using either of the two DCM variants
            if ( dcm_variant == 1 )
                
                % Experimental inputs (empty input)
                DCM.U.u     = zeros(size(DCM.Y.y,1)*16, 1);
                DCM.U.name  = {'null'};
                DCM.U.dt    = DCM.Y.dt/16;
                
                % no driving inputs
                DCM.b = zeros(DCM.n,DCM.n,size(DCM.U.u,2));
                DCM.c = zeros(DCM.n,size(DCM.U.u,2));
                DCM.d = zeros(DCM.n,DCM.n,0);
                
                % shift the input regressor slightly
                options.u_shift = 0;

                % empirical analysis
                type = 'r';
                
                % run the model inversion
                [output, options] = tapas_rdcm_estimate(DCM, type, options, 1);
                
                % store ground truth
                output.GT = DCM.GT;
                
                % store the results
                if ( ~isempty(output) )
                    if ( subject < 10 )
                        save(fullfile(results_folder, ['DCM_model_t' num2str(true_model) '_e' num2str(estimated_model) '_0' num2str(subject) '.mat']), 'output')
                    else
                        save(fullfile(results_folder, ['DCM_model_t' num2str(true_model) '_e' num2str(estimated_model) '_' num2str(subject) '.mat']), 'output')
                    end
                end
                
            elseif ( dcm_variant == 2 )
                
                % run the model inversion
                DCM = spm_dcm_fmri_csd(DCM);
                
                % store the results
                if ( ~isempty(DCM) )
                    if ( subject < 10 )
                        save(fullfile(results_folder, ['DCM_model_t' num2str(true_model) '_e' num2str(estimated_model) '_0' num2str(subject) '.mat']), 'DCM')
                    else
                        save(fullfile(results_folder, ['DCM_model_t' num2str(true_model) '_e' num2str(estimated_model) '_' num2str(subject) '.mat']), 'DCM')
                    end
                end
                
            end
            
            % line breaks
            fprintf('\n\n')
            
        end
    end
end

end
