function get_VOI_coordinates_MainExperiment(mode,parcellation_type)
% Collects the center coordinate of the regions of interest (ROIs) from
% each subjects of the NESDA study.
% 
% Input:
%   mode                -- analysis: (1) saccade network, (2) resting state networks, (3) whole-brain network
%   parcellation_type   -- parcellation scheme: (1) modes (RSN), (2) nodes (RSN)
%                                               (1) Glasser 2016, (2) AAL, (3) Brainnetome 2016, (4) Glasser 2016 & Buckner 2011
% 
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2020
% Copyright 2020 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------



% set the different parcellation options
switch mode    
    case 2
        parcellations = {'modes','nodes'};
    case 3
        parcellations = {'Glasser2016','AAL','Brainnetome2016','Glasser2016_Buckner2011'};
end


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% add SPM to path
addpath(FilenameInfo.SPM_Path)


% initialize spm
spm_jobman('initcfg')


% get the site list
Site_List = {'Baltimore','Chicago','Dallas','Hartford'};

% get the mode list
mode_List = {'timeseries_VOI_saccade',['timeseries_VOI_RSN_' parcellations{parcellation_type}],['timeseries_VOI_' parcellations{parcellation_type}]};


% set the foldername
foldername = FilenameInfo.Data_Path;


% specify cell for subjects with missing ROIs
dimension_mismatch = cell(1,1);

% get the mask information
if ( mode == 1 )
    mask_info{1,1} = 'MFG';
    mask_info{2,1} = 'IPS_left';
    mask_info{3,1} = 'IPS_right';
    mask_info{4,1} = 'FEF_left';
    mask_info{5,1} = 'FEF_right';
elseif ( mode == 2 )
    mask_info = create_RSN_information_MainExperiment(parcellation_type);
elseif ( mode == 3 )
    mask_info = create_mask_information_MainExperiment(parcellation_type);
end

% specify the coordinate array
center_allregions = cell(1,size(mask_info,1));

% all subjects
Subject_List_all = cell(1);

for site = 1:length(Site_List)
    
    % get the subject list
    Subject_List = dir(fullfile(foldername,Site_List{site},'derivatives','fmriprep','sub*html'));
    
    % define the subjects to analyze
    subjects_analyze = 1:length(Subject_List);

    % iterate over subjects
    for s = subjects_analyze

        % subject name
        Subject = Subject_List(s).name(1:end-5);

        % specify the SPM folder
        SPM_foldername = fullfile(foldername,Site_List{site},'derivatives','fmriprep',Subject,'firstlevel');

        % check if subject has data
        folder_VOI = dir(fullfile(SPM_foldername,mode_List{mode}));
        
        % display the progress
        disp(['Processing: ' Subject])

        % perform BOLD signal time series extraction
        if ( ~isempty(folder_VOI) )

            % time series folder
            timeseries_folder = fullfile(SPM_foldername,mode_List{mode});
            
            % extract the time series for each region of interest
            for number_of_regions = 1:size(mask_info,1)

                % load the VOI to get the coordinates
                if ( exist(fullfile(timeseries_folder,['VOI_' mask_info{number_of_regions,1} '_1.mat']),'file') )

                    % load the VOI
                    load(fullfile(timeseries_folder,['VOI_' mask_info{number_of_regions,1} '_1.mat']))

                    % get the center coordinates for the extracted time series
                    if ( isempty(center_allregions{1,1}) )
                        center_allregions{1,number_of_regions} = xY.xyz';
                    else
                        if ( number_of_regions == 1 )
                            center_allregions{end+1,number_of_regions} = xY.xyz';
                        else
                            center_allregions{end,number_of_regions} = xY.xyz';
                        end
                    end

                else

                    % set the coordinate to NaN
                    if ( isempty(center_allregions{1,1}) )
                        center_allregions{1,number_of_regions} = NaN;
                    else
                        if ( number_of_regions == 1 )
                            center_allregions{end+1,number_of_regions} = NaN;
                        else
                            center_allregions{end,number_of_regions} = NaN;
                        end
                    end

                    % store the subject as not completed
                    if ( isempty(dimension_mismatch{1}) )
                        dimension_mismatch{1} = Subject;
                    else
                        dimension_mismatch{end+1} = Subject;
                    end
                end
            end
        else
            % set all coordinate to NaN
            for number_of_regions = 1:size(mask_info,1)
                if ( isempty(center_allregions{1,1}) )
                    center_allregions{1,number_of_regions} = NaN;
                else
                    if ( number_of_regions == 1 )
                        center_allregions{end+1,number_of_regions} = NaN;
                    else
                        center_allregions{end,number_of_regions} = NaN;
                    end
                end

                % store the subject as not completed
                if ( isempty(dimension_mismatch{1}) )
                    dimension_mismatch{1} = Subject;
                else
                    dimension_mismatch{end+1} = Subject;
                end
            end
        end
        
        % store the subejct name
        if ( isempty(Subject_List_all{1}) )
            Subject_List_all{1} = Subject;
            Site_List_all{1}    = Site_List{site};
        else
            Subject_List_all{end+1} = Subject;
            Site_List_all{end+1}    = Site_List{site};
        end
    end
end

% foldername for the center coordinates
VoI_foldername = fullfile(foldername,'VoI_coordinates',mode_List{mode});


% create the VoI folder
if ( ~exist(VoI_foldername,'dir') )
    mkdir(VoI_foldername)
end

% save the center coordinates in a file
save(fullfile(VoI_foldername,'VoI_CenterCoordinates_AllSubjects.mat'),'center_allregions','dimension_mismatch','Subject_List_all','Site_List_all')

end
