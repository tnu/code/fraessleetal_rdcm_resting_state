#!/bin/bash

# ---------------------------------------------------------------------------
# ---------------------------------------------------------------------------
# 
# This bash script comprises the full analysis pipeline associated with the following paper:
#
# Title: Regression dynamic causal modeling for resting-state fMRI
# Authors: Stefan Frässle, Samuel J. Harrison, Jakob Heinzle, Brett A. Clementz, Carol A. Tamminga, John A. Sweeney, Matcheri S. Keshavan, Godfrey D. Pearlson, Albert Powers, Klaas E. Stephan
#
# In brief, the analysis comprises four different steps:
#    (i)   simulations
#    (ii)  modes analysis
#    (iii) nodes analysis
#    (iv)  whole-brain analysis
#
# ---------------------------------------------------------------------------
# ---------------------------------------------------------------------------


# -----------------------------------------
# Environment
# -----------------------------------------


# switch to new software stack (mainly to enable ghostscript)
. /cluster/apps/local/env2lmod.sh


# Load the ghostscript and matlab module
module load ghostscript/9.21
module load matlab


# output the new software stack (should have ghostscript and Matlab included)
module list


# create logs folder
mkdir logs



# -----------------------------------------
# Simulation studies
# -----------------------------------------


# define the TR and SNR settings
TRs="0.5 1 2"
SNRs="0.5 1 3"


# generate synthetic data for the simulations
for model in {1..4}
do
	for TR in $TRs
	do
		for SNR in $SNRs
		do
			bsub -W 2:00 -J "job_create_synth_DCM_$model,$TR,$SNR" matlab -singleCompThread -r "generate_syntheticDCMs_timeseries_MainExperiment($model,[],$TR,$SNR)"
		done
	done
done


# fit the synthetic data using rDCM
for TR in $TRs
do
	for SNR in $SNRs
	do
		bsub -W 4:00 -J "job_synth_rDCM_$TR,$SNR" -w 'ended("job_create_synth_DCM_*")' matlab -singleCompThread -r "estimate_synthetic_DCM_MainExperiment(1,[],$TR,$SNR)"
	done
done


# fit the synthetic data using spDCM
for TR in $TRs
do
	for SNR in $SNRs
	do
		bsub -W 4:00 -J "job_synth_DCM_$TR,$SNR" -w 'ended("job_create_synth_DCM_*")' matlab -singleCompThread -r "estimate_synthetic_DCM_MainExperiment(2,[],$TR,$SNR)"
	done
done


# get all results of simulations for rDCM
bsub -W 4:00 -J "job_get_synth_rDCM" -w 'ended("job_synth_rDCM_*")' matlab -singleCompThread -r "get_synthetic_DCM_MainExperiment(1)"


# get all results of simulations for spDCM
bsub -W 4:00 -J "job_get_synth_DCM" -w 'ended("job_synth_DCM_*")' matlab -singleCompThread -r "get_synthetic_DCM_MainExperiment(2)"


# get summary of simulations for rDCM
bsub -W 4:00 -J "job_summary_synth_rDCM" -w 'ended("job_get_synth_rDCM")' matlab -singleCompThread -r "summarize_synthetic_DCM_MainExperiment(1)"


# get summary of simulations for spmDCM
bsub -W 4:00 -J "job_get_synth_DCM" -w 'ended("job_get_synth_DCM")' matlab -singleCompThread -r "summarize_synthetic_DCM_MainExperiment(2)"



# -----------------------------------------
# Empirical analysis: 4-mode network
# -----------------------------------------


# need to copy all data from project folder
for site in {1..4}
do
	bsub -W 4:00 -J "job_copy_$site" -w 'ended("job_create_synth_DCM_*")' matlab -singleCompThread -r "copy_data_MainExperiment($site)"
done


# Extract BOLD signal time series for the 4-mode networks
for ID in {1..383}
do
	for site in {1..4}
	do
		bsub -W 4:00 -J "job_timeseries_mode_$ID" -w 'ended("job_copy_*")' matlab -singleCompThread -r "extract_VOI_RSN_timeseries_MainExperiment(2,$ID,$site)"
	done
done


# Get all center coordinates of the BOLD signal time series
bsub -W 4:00 -J "job_get_timeseries_mode" -w 'ended("job_timeseries_mode_*")' matlab -singleCompThread -r "get_VOI_coordinates_MainExperiment(2,1)"


# check whether all subjects have data from all modes available 
bsub -W 2:00 -J "job_check_timeseries_mode" -w 'ended("job_get_timeseries_mode")' matlab -singleCompThread -r "check_indvidual_coordinates_MainExperiment(2,1,1)"


# Fit rs-fMRI data using rDCM 
for ID in {1..383}
do
	bsub -W 4:00 -J "job_rDCM_mode_$ID" -w 'ended("job_check_timeseries_mode")' matlab -singleCompThread -r "create_and_estimate_rDCM_MainExperiment(2,$ID,[],1)"
done


# Fit rs-fMRI data using spectral DCM
for ID in {1..383}
do
	for site in {1..4}
	do
		bsub -W 4:00 -J "job_spDCM_mode_$ID" -w 'ended("job_check_timeseries_mode")' matlab -singleCompThread -r "create_and_estimate_DCM_MainExperiment(2,$ID,$site,1)"
	done
done


# Collect all results from the rDCM inversion
bsub -W 4:00 -J "job_get_rDCM_mode" -w 'ended("job_rDCM_mode_*")' -oo "logs/log_rDCM_collect_mode" matlab -singleCompThread -r "get_rDCM_parameter_estimates_MainExperiment(2,1)"


# Collect all results from the spDCM inversion
bsub -W 4:00 -J "job_get_spDCM_mode" -w 'ended("job_spDCM_mode_*")' -oo "logs/log_spDCM_collect_mode" matlab -singleCompThread -r "get_DCM_parameter_estimates_MainExperiment(2,1)"


# Summarize the results from the rDCM inversion
bsub -W 4:00 -J "job_summary_rDCM_mode" -w 'ended("job_get_rDCM_mode")' -oo "logs/log_rDCM_summary_mode" matlab -singleCompThread -r "summarize_rDCM_MainExperiment(2,1,1,0)"


# Summarize the results from the spDCM inversion
bsub -W 4:00 -J "job_summary_spDCM_mode" -w 'ended("job_get_spDCM_mode")' matlab -singleCompThread -r "summarize_DCM_MainExperiment(2,1,1,0)"


# Compare rDCM and spDCM results
bsub -W 4:00 -J "job_rDCM_vs_spDCM_mode" -w 'ended("job_summary*")' matlab -singleCompThread -r "compare_rDCM_spDCM_MainExperiment(2,1,1)"



# -----------------------------------------
# Empirical analysis: 15-node network
# -----------------------------------------


# Extract BOLD signal time series for the 15-nodes networks
for ID in {1..383}
do
	for site in {1..4}
	do
		bsub -W 4:00 -J "job_timeseries_node_$ID" -w 'ended("job_timeseries_mode_*")' matlab -singleCompThread -r "extract_VOI_RSN_timeseries_MainExperiment(3,$ID,$site)"
	done
done


# Get all center coordinates of the BOLD signal time series
bsub -W 4:00 -J "job_get_timeseries_node" -w 'ended("job_timeseries_node_*")' matlab -singleCompThread -r "get_VOI_coordinates_MainExperiment(2,2)"


# check whether all subjects have data from all nodes available
bsub -W 2:00 -J "job_check_timeseries_node" -w 'ended("job_get_timeseries_node")' matlab -singleCompThread -r "check_indvidual_coordinates_MainExperiment(2,2,1)"


# Fit rs-fMRI data using rDCM 
for ID in {1..383}
do
	bsub -W 4:00 -J "job_rDCM_node_$ID" -w 'ended("job_check_timeseries_node")' matlab -singleCompThread -r "create_and_estimate_rDCM_MainExperiment(3,$ID,[],1)"
done


# Fit rs-fMRI data using spectral DCM
for ID in {1..383}
do
	for site in {1..4}
	do
		bsub -W 24:00 -J "job_spDCM_node_$ID" -w 'ended("job_check_timeseries_node")' matlab -singleCompThread -r "create_and_estimate_DCM_MainExperiment(3,$ID,$site,1)"
	done
done


# Collect all results from the rDCM inversion
bsub -W 4:00 -J "job_get_rDCM_node" -w 'ended("job_rDCM_node_*")' -oo "logs/log_rDCM_collect_node" matlab -singleCompThread -r "get_rDCM_parameter_estimates_MainExperiment(3,1)"


# Collect all results from the spDCM inversion
bsub -W 4:00 -J "job_get_spDCM_node" -w 'ended("job_spDCM_node_*")' -oo "logs/log_spDCM_collect_node" matlab -singleCompThread -r "get_DCM_parameter_estimates_MainExperiment(3,1)"


# Summarize the results from the rDCM inversion
bsub -W 4:00 -J "job_summary_rDCM_node" -w 'ended("job_get_rDCM_node")' -oo "logs/log_rDCM_summary_node" matlab -singleCompThread -r "summarize_rDCM_MainExperiment(3,1,1,0)"


# Summarize the results from the spDCM inversion
bsub -W 4:00 -J "job_summary_spDCM_node" -w 'ended("job_get_spDCM_node")' matlab -singleCompThread -r "summarize_DCM_MainExperiment(3,1,1,0)"


# Compare rDCM and spDCM results
bsub -W 4:00 -J "job_rDCM_vs_spDCM_node" -w 'ended("job_summary*")' matlab -singleCompThread -r "compare_rDCM_spDCM_MainExperiment(3,1,1)"



# -----------------------------------------
# Empirical analysis: whole-brain network
# -----------------------------------------


# Extract BOLD signal time series for the Human Brainnetome atlas
for ID in {1..383}
do
	bsub -W 4:00 -J "job_timeseries_whole_$ID,1" -w 'ended("job_timeseries_node_*")' matlab -singleCompThread -r "extract_VOI_wholeBrain_timeseries_MainExperiment($ID,1,3)"
	bsub -W 4:00 -J "job_timeseries_whole_$ID,2" -w 'ended("job_timeseries_whole_*,1")' matlab -singleCompThread -r "extract_VOI_wholeBrain_timeseries_MainExperiment($ID,2,3)"
	bsub -W 4:00 -J "job_timeseries_whole_$ID,3" -w 'ended("job_timeseries_whole_*,2")' matlab -singleCompThread -r "extract_VOI_wholeBrain_timeseries_MainExperiment($ID,3,3)"
	bsub -W 4:00 -J "job_timeseries_whole_$ID,4" -w 'ended("job_timeseries_whole_*,3")' matlab -singleCompThread -r "extract_VOI_wholeBrain_timeseries_MainExperiment($ID,4,3)"
done


# Get all center coordinates of the BOLD signal time series
bsub -W 12:00 -J "job_get_timeseries_whole" -w 'ended("job_timeseries_whole_*")' matlab -singleCompThread -r "get_VOI_coordinates_MainExperiment(3,3)"


# check which Brainnetome regions are missing in which subjects
bsub -W 2:00 -J "job_check_timeseries_whole" -w 'ended("job_get_timeseries_whole")' matlab -singleCompThread -r "check_indvidual_coordinates_MainExperiment(3,3,1)"


# Fit rs-fMRI data using rDCM
for ID in {1..383}
do
	for site in {1..4}
	do
		bsub -W 4:00 -J "job_rDCM_whole_$ID" -w 'ended("job_check_timeseries_whole")' matlab -singleCompThread -r "estimate_rDCM_fixed_MainExperiment($site,$ID,1,3,1)"
	done
done


# Collect all results from the rDCM inversion
bsub -W 4:00 -J "job_get_rDCM_whole" -w 'ended("job_rDCM_whole_*")' -oo "logs/log_rDCM_collect_whole" matlab -singleCompThread -r "get_rDCM_parameter_estimates_fixed_MainExperiment(1,3,1)"


# Summarize the results from the whole-brain rDCM analysis
bsub -W 4:00 -J "job_summary_rDCM_whole" -w 'ended("job_get_rDCM_whole")' -oo "logs/log_rDCM_summary_whole" matlab -singleCompThread -r "summarize_rDCM_fixed_MainExperiment(3,1,1,1,0)"



# -----------------------------------------
# Demographics and Run Times
# -----------------------------------------


# output demographic information
bsub -W 1:00 -J "job_demographics" -oo "logs/log_demographics" -w 'ended("job_summary_rDCM_whole")' matlab -singleCompThread -r "demographic_information_MainExperiment(1)"


# compute and output run times of rDCM and spDCM
bsub -W 4:00 -J "job_time_mode" -oo "logs/log_time_mode" -w 'ended("job_rDCM_vs_spDCM_mode")' matlab -singleCompThread -r "time_rDCM_vs_spDCM_MainExperiment(2,1,1)"
bsub -W 4:00 -J "job_time_node" -oo "logs/log_time_node" -w 'ended("job_rDCM_vs_spDCM_node")' matlab -singleCompThread -r "time_rDCM_vs_spDCM_MainExperiment(3,1,1)"


# output run times of rDCM for the whole-brain analysis
bsub -W 4:00 -J "job_time_whole" -oo "logs/log_time_whole" -w 'ended("job_summary_rDCM_whole")' matlab -singleCompThread -r "time_rDCM_fixed_MainExperiment(1,3,1)"
