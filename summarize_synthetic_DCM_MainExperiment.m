function summarize_synthetic_DCM_MainExperiment(dcm_variant)
% Obtains the estimates from the inversion of the synthetic DCMs based on 
% either regression DCM or spectral DCM. This function computes the 
% root mean squared error (RMSE) and the correlation between true and 
% estimated parameter values.
% 
% Input:
%   dcm_variant         -- DCM variant utilized for model inversion
%                           (1) regression DCM; (2) spectral DCM
%   nr_simulations      -- simulated variations of true model ("subjects")
% 
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2020
% Copyright 2020 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% add SPM and other toolboxed
addpath(FilenameInfo.SPM_Path)
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'PBAR')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'fdr_bh')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'cbrewer')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'append_pdfs')))


% initialize spm
spm_jobman('initcfg')


% method name
method_folder = {'regressionDCM','spectralDCM'};
method_name   = {'rDCM','spDCM'};


% load the results
temp = load(fullfile(FilenameInfo.Data_Path,'simulations_dcm','small_network',method_folder{dcm_variant},'Simulations.mat'));

% asign the results
RMSE_allSubjects = temp.results.RMSE_allSubjects;
CORR_allSubjects = temp.results.CORR_allSubjects;


% close all figures
close all


% TR counter
TR_counter = 0;

% iterate over true models
for TR = [2 1 0.5]
    
    % TR counter
    TR_counter = TR_counter + 1;
    
    
    % figure folder
    figure_folder = fullfile(FilenameInfo.Data_Path,'Figures','simulations_dcm',method_folder{dcm_variant});

    % create the directory for the figures
    if ( ~exist(figure_folder,'dir') ) 
        mkdir(figure_folder)
    else
        if ( exist(fullfile(figure_folder,'Simulations_ParameterRecovery.pdf'),'file') )
            delete(fullfile(figure_folder,'Simulations_ParameterRecovery.pdf'))
        end
    end
    
    
    % define nice colormap
    [cbrewer_colormap]      = cbrewer('seq', 'OrRd', 105, 'PCHIP');
    cbrewer_colormap        = flipud(cbrewer_colormap);
    cbrewer_colormap        = cbrewer_colormap([1:20 31:50 61:80],:);
    
    
    figure('units','normalized','outerposition',[0 0 1 1])
    hold on
    
    for model = 1:4
        for SNR = 1:3
        
            % get sorted version (for displaying purpose)
            RMSE_allSubjects_sort = sort(RMSE_allSubjects(:,model,TR_counter,SNR));

            % plot the root mean squared error
            for int = 1:size(RMSE_allSubjects_sort,1)
                plot(0.05*randn(1,1)+SNR+(model-1)*4,RMSE_allSubjects_sort(int),'o','MarkerFaceColor',cbrewer_colormap(int+(SNR-1)*20,:),'Color',[1 1 1],'MarkerSize',20)
            end
            plot(SNR+(model-1)*4,mean(RMSE_allSubjects_sort),'o','MarkerFaceColor',[0.2 0.2 0.2],'Color',[1 1 1],'MarkerSize',34)
            
            % output the results
            if ( SNR == 1 && TR_counter == 1 )
                
                % statistics in z-space
                RMSE_mean  = mean(RMSE_allSubjects_sort);
                RMSE_ste   = std(RMSE_allSubjects_sort)/sqrt(length(RMSE_allSubjects_sort));
                RMSE_lower = RMSE_mean - 1.96 * RMSE_ste;
                RMSE_upper = RMSE_mean + 1.96 * RMSE_ste;
                
                % output results
                fprintf('Model %u: RMSE = %1.2f [%1.3f %1.3f] | ',model,round(RMSE_mean,2),round(RMSE_lower,3),round(RMSE_upper,3))
            end
        end
    end
    
    axis square
    ylabel('root mean squared error (RMSE)','FontSize',18)
    xlim([0 16])
    set(gca,'xtick',2:4:16)
    set(gca,'xticklabel',{'model 1', 'model 2', 'model 3', 'model 4'},'FontSize',16)
    xtickangle(30)
    ylim([0 0.4])
    set(gca,'ytick',0:0.1:0.4)
    set(gca, 'TickDir', 'out')
    title(['Performance (' method_name{dcm_variant} '): RMSE (TR=' num2str(TR) ')'],'FontSize',18)
    print(gcf, '-dpdf', fullfile(figure_folder,['Simulations_RMSE_TR' num2str(TR*1000)]), '-fillpage')
    
    if ( TR_counter == 1 )
        fprintf('\n')
    end
    
    
    figure('units','normalized','outerposition',[0 0 1 1])
    hold on
    
    for model = 1:4
        for SNR = 1:3
        
            % get sorted version (for displaying purpose)
            CORR_allSubjects_sort = sort(CORR_allSubjects(:,model,TR_counter,SNR));

            % plot the root mean squared error
            for int = 1:size(CORR_allSubjects_sort,1)
                plot(0.05*randn(1,1)+SNR+(model-1)*4,CORR_allSubjects_sort(int),'o','MarkerFaceColor',cbrewer_colormap(int+(SNR-1)*20,:),'Color',[1 1 1],'MarkerSize',20)
            end
            plot(SNR+(model-1)*4,tanh(mean(atanh(CORR_allSubjects_sort))),'o','MarkerFaceColor',[0.2 0.2 0.2],'Color',[1 1 1],'MarkerSize',34)
            
            % output the results
            if ( SNR == 1 && TR_counter == 1 )
                
                % statistics in z-space
                Z_mean  = mean(atanh(CORR_allSubjects_sort));
                Z_std   = std(atanh(CORR_allSubjects_sort))/sqrt(length(CORR_allSubjects_sort));
                Z_lower = Z_mean - 1.96 * Z_std;
                Z_upper = Z_mean + 1.96 * Z_std;
                
                % back-transformation
                R_mean = tanh(Z_mean);
                R_lower = tanh(Z_lower);
                R_upper = tanh(Z_upper);
                
                % output results
                fprintf('Model %u: r = %1.2f [%1.2f %1.2f] | ',model,round(R_mean,2),round(R_lower,2),round(R_upper,2))
            end
        end
    end
    
    axis square
    ylabel('correlation coefficient (R)','FontSize',16)
    xlim([0 16])
    set(gca,'xtick',2:4:16)
    set(gca,'xticklabel',{'model 1', 'model 2', 'model 3', 'model 4'},'FontSize',16)
    xtickangle(30)
    ylim([0 1])
    set(gca,'ytick',0:0.5:1)
    set(gca, 'TickDir', 'out')
    title(['Performance (' method_name{dcm_variant} '): R (TR=' num2str(TR) ')'],'FontSize',18)
    print(gcf, '-dpdf', fullfile(figure_folder,['Simulations_R_TR' num2str(TR*1000)]), '-fillpage')
    
    if ( TR_counter == 1 )
        fprintf('\n')
    end    
    
end


% append all PDFs
append_pdfs(fullfile(figure_folder,'Simulations_ParameterRecovery.pdf'),...
    fullfile(figure_folder,'Simulations_RMSE_TR2000.pdf'),...
    fullfile(figure_folder,'Simulations_RMSE_TR1000.pdf'),...
    fullfile(figure_folder,'Simulations_RMSE_TR500.pdf'),...
    fullfile(figure_folder,'Simulations_R_TR2000.pdf'),...
    fullfile(figure_folder,'Simulations_R_TR1000.pdf'),...
    fullfile(figure_folder,'Simulations_R_TR500.pdf'))

% delete the temporary files
delete(fullfile(figure_folder,'Simulations_R*'))

end
