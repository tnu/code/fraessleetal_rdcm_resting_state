function get_subject_clinical_information_MainExperiment()
% Reads the clinical information for each subject individually from the 
% B_SNIP1 study. The function reads the xls File and gets the label of each
% subject.
% 
% Input:
% 
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2020
% Copyright 2020 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% define the foldername
foldername = FilenameInfo.Data_Path;


% read the xls file
[NUM,TXT,~]     = xlsread(fullfile(foldername, 'Information', 'B-SNIP_1_Biomarker_AD_20171012.xlsx'));
Subject_ID      = TXT(2:end,3);
Subject_TYPE    = TXT(2:end,9);
Subject_SUBTYPE = TXT(2:end,11);
Subject_PID     = NUM(1:end,1);

% find the indices containing the clinical scores
indices_PANSS = sort([find(contains(TXT(1,:),'PANSS_p')) find(contains(TXT(1,:),'PANSS_n')) find(contains(TXT(1,:),'PANSS_g'))]);
indices_MADRS = find(contains(TXT(1,:),'MADRS_'));
indices_YOUNG = find(contains(TXT(1,:),'Young_'));

% get clinical scores
Subject_PANSS = NUM(1:end,indices_PANSS); Subject_PANSS(Subject_PANSS(:,1)==999,:) = NaN;
Subject_MADRS = NUM(1:end,indices_MADRS(2:end-1)); Subject_MADRS(Subject_MADRS(:,1)==999,:) = NaN;
Subject_YOUNG = NUM(1:end,indices_YOUNG(2:end-1)); Subject_YOUNG(Subject_YOUNG(:,1)==999,:) = NaN;


% get the complete fMRI subject list
temp             = load(fullfile(foldername,'grouplevel_dcm','RSN_modes','PosteriorParameterEstimates_model1.mat'));
Subject_List_all = temp.results.AllSubjects;

% create array for all existing subjects
Subject_exist   = NaN(length(Subject_List_all),1);
Subject_type    = NaN(length(Subject_List_all),1);
Subject_subtype = NaN(length(Subject_List_all),1);
Subject_pid     = NaN(length(Subject_List_all),1);
Subject_panss 	= NaN(length(Subject_List_all),size(Subject_PANSS,2));
Subject_madrs 	= NaN(length(Subject_List_all),size(Subject_MADRS,2));
Subject_young 	= NaN(length(Subject_List_all),size(Subject_YOUNG,2));


% iterate over subjects and compare
for s = 1:length(Subject_List_all)
    
    % get the subject ID
    sub_temp = Subject_List_all{s}(5:end-1);
    
    % check whether subject has information in xlsx sheet
    Subject_exist(s) = any(contains(Subject_ID,sub_temp));
    
    % get the subject type (label)
    % code: NaN = empty, 1 = Patient, 2 = Family, 3 = normal control
    if ( Subject_exist(s) == 0 )
        Subject_type(s) = NaN;
    else
        
        % find the index in the xlsx sheet
        sub_id = find(contains(Subject_ID,sub_temp));
        
        % find the label
        if ( strcmp(Subject_TYPE{sub_id},'Proband') )
            Subject_type(s) = 1;
        elseif ( strcmp(Subject_TYPE{sub_id},'Family') )
            Subject_type(s) = 2;
        elseif ( strcmp(Subject_TYPE{sub_id},'NC') ) 
            Subject_type(s) = 3;
        end
    end
    
    % get the subject subtype (label)
    % code: NaN = empty, 1 = SZ, 2 = BP, 3 = SAD
    if ( Subject_exist(s) == 0 )
        Subject_subtype(s) = NaN;
        Subject_pid(s)     = NaN;
    else
        
        % get the PID
        Subject_pid(s) = Subject_PID(sub_id);
        
        if ( strcmp(Subject_SUBTYPE{sub_id},'SZP') )
            Subject_subtype(s) = 1;
        elseif ( strcmp(Subject_SUBTYPE{sub_id},'SZR') )
            Subject_subtype(s) = 4;
        elseif ( strcmp(Subject_SUBTYPE{sub_id},'BPP') )
            Subject_subtype(s) = 2;
        elseif ( strcmp(Subject_SUBTYPE{sub_id},'BPR') )
            Subject_subtype(s) = 5;
        elseif ( strcmp(Subject_SUBTYPE{sub_id},'SADP') )
            Subject_subtype(s) = 3;
        elseif ( strcmp(Subject_SUBTYPE{sub_id},'SADR') )
            Subject_subtype(s) = 6;
        elseif ( strcmp(Subject_SUBTYPE{sub_id},'NC') )
            Subject_subtype(s) = 7;
        end
    end
    
    % get the subject confounds (age, gender, site)
    if ( Subject_exist(s) == 0 )
        Subject_panss(s)	= NaN;
        Subject_madrs(s)	= NaN;
        Subject_young(s)	= NaN;
    else
        Subject_panss(s,:)	= Subject_PANSS(sub_id,:);
        Subject_madrs(s,:)	= Subject_MADRS(sub_id,:);
        Subject_young(s,:)	= Subject_YOUNG(sub_id,:);
    end
end
    

% asign the subject PID
Subject_name = Subject_List_all;


% create the directory
if ( ~exist(fullfile(foldername,'Information'),'dir') )
    mkdir(fullfile(foldername,'Information'))
end
    
% store the results
if ( ~isempty(Subject_exist) && ~isempty(Subject_type) && ~isempty(Subject_subtype) && ~isempty(Subject_name) )
    save(fullfile(foldername,'Information','Subject_ClinicalInformation.mat'),'Subject_exist','Subject_type','Subject_subtype','Subject_panss','Subject_madrs','Subject_young','Subject_name')
end

end
