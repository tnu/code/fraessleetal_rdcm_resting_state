function summarize_rDCM_MainExperiment(mode,model,subgroup,computeGT)
% Evaluates the mean effective connectivity parameters for the different 
% patient subgroups from the BSNIP study as inferred using the regression
% dynamic causal modeling (rDCM) toolbox, as well as across all different
% groups. Additionally, it computes graph-theoretical measures from these
% effective connectivity patterns.
%
% Input:
%   mode          	-- (1) saccade network, (2) RSN (modes), (3) RSN (nodes)
%   model           -- model to be analyzed
%   subgroup       	-- (0) all participants, (1) restrict to healthy controls
%   computeGT       -- compute graph theoretical measures
% 
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2020
% Copyright 2020 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% close all figures
close all

% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% add path to several relevant toolboxes
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'PBAR')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'fdr_bh')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'cbrewer')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'append_pdfs')))

% analysis file- and foldernames
mode_List       = {'saccade','RSN_modes','RSN_nodes'};
subgroup_name   = {'','_HC'};


% set the SPM path and the path to the experiment
foldername = fullfile(FilenameInfo.Data_Path,'grouplevel_dcm',mode_List{mode});


% load the results array
temp    = load(fullfile(foldername,['PosteriorParameterEstimates_model' num2str(model) '_rDCM.mat']));
results = temp.results;


% generate subject infromation if necessary
if ( ~exist(fullfile(FilenameInfo.Data_Path,'Information','Subject_Information.mat'),'file') )
    get_subject_information_MainExperiment()
end


% load the subject information
temp            = load(fullfile(FilenameInfo.Data_Path,'Information','Subject_Information.mat'));
Subject_type    = temp.Subject_type;


% load the subject information regarding confounds
temp2           = load(fullfile(FilenameInfo.Data_Path,'Information','Subject_Confounds.mat'));
Subject_age     = temp2.Subject_age;
Subject_gender  = temp2.Subject_gender;
Subject_site    = temp2.Subject_site;

% number of different sites
NR_sites        = max(Subject_site)-1;

% create a confound matrix
confounds = [Subject_age, Subject_gender];
for int = 1:NR_sites
    if ( sum(Subject_site==int) ~= 0 )
        confounds = [confounds, Subject_site==int];
    end
end

% different tests
Subject_type_test = Subject_type;


% find the subjects that are not included in the analysis
Subject_type_test((~isfinite(results.A_Matrix_AllSubjects{1,1}))==1) = NaN;


% restrict to healthy controls only
if ( subgroup == 1 )
    Subject_type_test(Subject_type_test~=3) = NaN;
end


% asign the data
A_matrix_allSubjects     = results.A_Matrix_AllSubjects;


% check whether you have the right subjects (should not be necessary)
if ( length(temp.Subject_name) ~= length(A_matrix_allSubjects{1,1}) )
    for int = 1:length(results.AllSubjects)
        vector(int) = any(strcmp(results.AllSubjects(int),temp.Subject_name));
    end
    
    for int = 1:size(A_matrix_allSubjects,1)
        for int2 = 1:size(A_matrix_allSubjects,2)
            A_matrix_allSubjects{int,int2} = A_matrix_allSubjects{int,int2}(vector==1);
        end
    end
end

% excluded subjects
excluded_subjects = ~isfinite(A_matrix_allSubjects{1,1}) | ~isfinite(Subject_type_test) | ~isfinite(Subject_age) | ~isfinite(Subject_gender) | ~isfinite(Subject_site);


% define results arrays
mean_Amatrix_allSubjects = NaN(size(A_matrix_allSubjects));
pVal_Amatrix_allSubjects = NaN(size(A_matrix_allSubjects));


% display the number of subjects
disp(['Found: ' num2str(sum(excluded_subjects==0)) ' out of ' num2str(length(Subject_type_test)) ' subjects'])


% get endogenous parameters
for int = 1:size(A_matrix_allSubjects,1)
    for int2 = 1:size(A_matrix_allSubjects,2)
        
        % get the mean endogenous connection strength
        mean_Amatrix_allSubjects(int,int2) = mean(A_matrix_allSubjects{int,int2}(excluded_subjects==0));

        % asign the p value
        [~,p] = ttest(A_matrix_allSubjects{int,int2}(excluded_subjects==0),0);
        pVal_Amatrix_allSubjects(int,int2) = p;
    end
end


% figure folder
figure_folder = fullfile(FilenameInfo.Data_Path,'Figures',mode_List{mode},'regressionDCM');

% create the directory for the figures
if ( ~exist(figure_folder,'dir') ) 
    mkdir(figure_folder)
else
    if ( exist(fullfile(figure_folder,['Model' num2str(model) subgroup_name{subgroup+1} '.pdf']),'file') )
        delete(fullfile(figure_folder,['Model' num2str(model) subgroup_name{subgroup+1} '.pdf']))
    end
end


% significance threshold (FDR-correction)
p_all               = pVal_Amatrix_allSubjects(:);
p_all               = p_all(isfinite(p_all));
[~, crit_p, ~, ~]   = fdr_bh(p_all,0.05,'dep','no');
threshold           = crit_p+eps;


% define nice colormap
switch mode
    case 1
        [cbrewer_colormap] = cbrewer('div', 'RdBu', 41, 'PCHIP');
        cbrewer_colormap   = flipud(cbrewer_colormap);
    case 2
        [cbrewer_colormap] = cbrewer('div', 'RdBu', 41, 'PCHIP');
        cbrewer_colormap   = flipud(cbrewer_colormap);
    case 3
        % define nice colormap
        [cbrewer_colormap] = cbrewer('div', 'RdBu', 71, 'PCHIP');
        cbrewer_colormap   = flipud(cbrewer_colormap);
        cbrewer_colormap   = cbrewer_colormap([1:35 37:71],:);
end
       


% set color axis
switch mode
    case 1
        c_lim = [-0.2 0.2];
    case 2
        c_lim = [-0.2 0.2] + subgroup*[0.05 -0.05];
    case 3
        c_lim = [-0.1 0.1];
end


% plot average endogenous connectivity
figure('units','normalized','outerposition',[0 0 1 1])
imagesc(mean_Amatrix_allSubjects)
axis square
colormap(cbrewer_colormap)
colorbar
caxis(c_lim)
set(gca,'xtick',1:size(mean_Amatrix_allSubjects,2))
set(gca,'xticklabels',results.AllRegions,'FontSize',16)
xtickangle(30)
set(gca,'ytick',1:size(mean_Amatrix_allSubjects,1))
set(gca,'yticklabels',results.AllRegions,'FontSize',16)
ytickangle(30)
title('Endogenous connectivity [Hz]','FontSize',18)
print(gcf, '-dpdf', fullfile(figure_folder,['Model' num2str(model) '_temp1']), '-fillpage')


% plot average endogenous connectivity (significant)
figure('units','normalized','outerposition',[0 0 1 1])
imagesc(mean_Amatrix_allSubjects .* (pVal_Amatrix_allSubjects < threshold))
axis square
colormap(cbrewer_colormap)
colorbar
caxis(c_lim)
set(gca,'xtick',1:size(mean_Amatrix_allSubjects,2))
set(gca,'xticklabels',results.AllRegions,'FontSize',16)
xtickangle(30)
set(gca,'ytick',1:size(mean_Amatrix_allSubjects,1))
set(gca,'yticklabels',results.AllRegions,'FontSize',16)
ytickangle(30)
title('Endogenous connectivity (sig.) [Hz]','FontSize',18)
print(gcf, '-dpdf', fullfile(figure_folder,['Model' num2str(model) '_temp2']), '-fillpage')



% adjacency matrix
adjcency_matrix = mean_Amatrix_allSubjects;
adjcency_matrix = adjcency_matrix - diag(diag(adjcency_matrix));

% define colormap
c_opt.cmap = cbrewer_colormap;

% limits of colormap
c_opt.clim = c_lim;

% center coordinates
switch mode
    case 1
        c_opt.centre = [0.33 0.6; 0.2 0.37; 0.37 0.40; 0.27 0.20];
    case 2
        c_opt.centre = [0.33 0.6; 0.2 0.37; 0.37 0.40; 0.27 0.20];
    case 3
        c_opt.centre = [0.48 1; 0.46 0.6; 0.4 0.79; 0.56 0.82;...
                        0.8 0.55; 0.7 0.34; 0.9 0.37; 0.7 0.15; 0.9 0.15;...
                        0.05 0.4; 0.25 0.4; 0.03 0.19; 0.27 0.22; 0.05 0.0; 0.25 0.0;];
end

% define coordinates for text
switch mode
    case 1
        c_opt.txt_centre = [0.33 0.64; 0.173 0.37; 0.38 0.40; 0.27 0.16];
    case 2
        c_opt.txt_centre = [0.33 0.64; 0.173 0.37; 0.38 0.40; 0.27 0.16];
    case 3
        c_opt.txt_centre = [0.48 1.08; 0.46 0.54; 0.28 0.79; 0.6 0.82;...
                            0.8 0.63; 0.75 0.34; 0.95 0.37; 0.7 0.07; 0.9 0.07;...
                            -0.02 0.48; 0.18 0.48; -0.09 0.19; 0.15 0.22; -0.02 -0.08; 0.18 -0.08;];
end

% sizes
c_opt.NodeSize  = 300;
c_opt.ArrowSize = 20;
c_opt.EdgeScale = 4;

% color
switch mode
    case 1
        
    case 2
        c_opt.NodeColor = [255 244 177; 245 177 119; 231 110 103; 172 43 96]/255;
        c_opt.NodeName  = {'DMN','DAN','SAN','CEN'};
    case 3
        c_opt.NodeColor = [125 112 177; 110 127 190; 93 144 199; 50 143 188;...
                           200 223 174; 225 233 176; 236 237 173; 229 215 158; 237 217 151;...
                           233 118 100; 207 99 97; 189 84 96; 174 68 92; 147 54 84; 130 41 78]/255;
        c_opt.NodeName  = {'PCC','aMPF','lAG','rAG',...
                           'dACC','lAI','rAI','lPFC','rPFC',...
                           'lFEF','rFEF','lIFG','rIFG','lIPS','rIPS'};
end

% plot average endogenous connectivity as a graph
plot_network_MainExperiment(adjcency_matrix,c_opt)

% print the figure
print(gcf, '-dpdf', fullfile(figure_folder,['Model' num2str(model) '_temp3']), '-fillpage')


% append all PDFs
append_pdfs(fullfile(figure_folder,['Model' num2str(model) subgroup_name{subgroup+1} '.pdf']),...
    fullfile(figure_folder,['Model' num2str(model) '_temp1.pdf']),...
    fullfile(figure_folder,['Model' num2str(model) '_temp2.pdf']),...
    fullfile(figure_folder,['Model' num2str(model) '_temp3.pdf']))


% delete the temporary files
delete(fullfile(figure_folder,['Model' num2str(model) '_temp*']))


% remove the diagonal
mean_Amatrix_allSubjects_nodiag = mean_Amatrix_allSubjects-diag(diag(mean_Amatrix_allSubjects));
mean_Amatrix_allSubjects_nodiag = abs(mean_Amatrix_allSubjects_nodiag);

% empty results array
HierarchyStrength = NaN(1,size(mean_Amatrix_allSubjects_nodiag,1));

% output results
fprintf('Hierarchy Strength: ')

% compute the hierarchy strength and output
for int = 1:size(mean_Amatrix_allSubjects_nodiag,1)
    HierarchyStrength(int) = sum(mean_Amatrix_allSubjects_nodiag(:,int)) - sum(mean_Amatrix_allSubjects_nodiag(int,:));
    fprintf('%s: %f | ',results.AllRegions{int},HierarchyStrength(int))
end

% line break
fprintf('\n')


% take a mode perspective for the nodes analysis
if ( mode == 3 )

    % number of regions per mode
    NR_regions = [1, 5, 10, 16];
    
    % empty mode array
    mean_Amatrix_allSubjects_nodiag_mode = zeros(length(NR_regions)-1);
    
    % compute average afferents and efferents to mode
    for int = 1:length(NR_regions)-1
        for int2 = 1:length(NR_regions)-1
        
            % average connection strength
            if ( int ~= int2 )
                sub_matrix = mean_Amatrix_allSubjects(NR_regions(int):NR_regions(int+1)-1,NR_regions(int2):NR_regions(int2+1)-1);
                mean_Amatrix_allSubjects_nodiag_mode(int,int2) = mean(sub_matrix(:));
            end
            
        end
    end
    
    
    % define nice colormap
    [cbrewer_colormap] = cbrewer('div', 'RdBu', 71, 'PCHIP');
    cbrewer_colormap   = flipud(cbrewer_colormap);
    cbrewer_colormap   = cbrewer_colormap([1:33 36 39:71],:);
    
    % color limits for modes computation
    c_lim_modes = [-0.01 0.01];
    
    % plot average endogenous connectivity
    figure('units','normalized','outerposition',[0 0 1 1])
    imagesc(mean_Amatrix_allSubjects_nodiag_mode)
    axis square
    colormap(cbrewer_colormap)
    colorbar
    caxis(c_lim_modes)
    set(gca,'xtick',1:size(mean_Amatrix_allSubjects_nodiag_mode,2))
    set(gca,'xticklabels',{'DMN','SAN','DAN'},'FontSize',16)
    xtickangle(30)
    set(gca,'ytick',1:size(mean_Amatrix_allSubjects_nodiag_mode,1))
    set(gca,'yticklabels',{'DMN','SAN','DAN'},'FontSize',16)
    ytickangle(30)
    title('Endogenous connectivity [Hz]','FontSize',18)
    print(gcf, '-dpdf', fullfile(figure_folder,['Model' num2str(model) '_temp1']), '-fillpage')
    
    % append all PDFs
    append_pdfs(fullfile(figure_folder,['Model' num2str(model) subgroup_name{subgroup+1} '.pdf']),...
        fullfile(figure_folder,['Model' num2str(model) '_temp1.pdf']))
    
    % delete the temporary files
    delete(fullfile(figure_folder,['Model' num2str(model) '_temp*']))
    
    
    % define colormap
    c_opt.cmap = cbrewer_colormap;

    % limits of colormap
    c_opt.clim = c_lim_modes;

    % center coordinates
    c_opt.centre = [0.33 0.6; 0.37 0.40; 0.2 0.37];
    
    % define coordinates for text
    c_opt.txt_centre = [0.33 0.62; 0.38 0.40; 0.173 0.37];

    % sizes
    c_opt.NodeSize  = 300;
    c_opt.ArrowSize = 30;
    c_opt.EdgeScale = 10;

    % color
    c_opt.NodeColor = [255 244 177; 231 110 103; 245 177 119]/255;
    c_opt.NodeName  = {'DMN','SAN','DAN'};
    
    % plot average endogenous connectivity as a graph
    plot_network_MainExperiment(mean_Amatrix_allSubjects_nodiag_mode,c_opt)
    
    % take the absolute values
    mean_Amatrix_allSubjects_nodiag_mode = abs(mean_Amatrix_allSubjects_nodiag_mode);
    
    % empty results array
    HierarchyStrengthMode = NaN(1,size(mean_Amatrix_allSubjects_nodiag_mode,1));
    
    % print hierarchy strength
    fprintf('Hierarchy Strength: ')
    
    % display the hierarchy scores
    for int = 1:size(mean_Amatrix_allSubjects_nodiag_mode,1)
        HierarchyStrengthMode(int) = sum(mean_Amatrix_allSubjects_nodiag_mode(:,int)) - sum(mean_Amatrix_allSubjects_nodiag_mode(int,:));
        fprintf('%s: %f | ',c_opt.NodeName{int},HierarchyStrengthMode(int))
    end 
end

% line break
fprintf('\n')


% clear the previous results
results_names = results; clear results


% store the results
results.mean_Amatrix_allSubjects = mean_Amatrix_allSubjects;
results.pVal_Amatrix_allSubjects = pVal_Amatrix_allSubjects;

% store the results
if ( ~isempty(results) )
    save(fullfile(foldername,['SummaryStatistics_model' num2str(model) '_rDCM' subgroup_name{subgroup+1} '.mat']),'results')
end


% graph theory
if ( computeGT )

    % display the progress
    fprintf('\nComputing graph theoretical measures...\n')

    % clear results array
    clear results
    
    % compute graph theoretical measures
    for s = 1:length(A_matrix_allSubjects{1,1})

        % get graph theoretical results or write empty array
        if ( excluded_subjects(s) == 0 )

            % display with subject is processed
            disp(['Processing: ' results_names.AllSubjects{s}])

            % empty single subject matrix
            A_matrix_Subject = NaN(size(A_matrix_allSubjects));

            % create matrix from subject
            for int = 1:size(A_matrix_allSubjects,1)
                for int2 = 1:size(A_matrix_allSubjects,2)
                    A_matrix_Subject(int,int2) = A_matrix_allSubjects{int,int2}(s);
                end
            end

            % remove diagonal
            A_matrix_Subject = A_matrix_Subject - diag(diag(A_matrix_Subject));

            % transpose A-matrix (as Brain Connectivity toolbox uses switched notation)
            A_matrix_Subject = A_matrix_Subject';
            
            % compute graph theoretical measures
            if ( any(A_matrix_Subject(:)~=0) )
                results_temp = compute_graph_theoretical_measures(A_matrix_Subject);
                results(s)   = results_temp;
            end

        else
            
            % display with subject is processed
            disp(['Processing: ' results_names.AllSubjects{s} ' (excluded)'])
            
            % compute graph theoretical measures
            results_temp = compute_graph_theoretical_measures([]);
            results(s)   = results_temp;
            
        end
    end
    
    % store the graph theoretical measures
    save(fullfile(foldername,['GraphTheory_model' num2str(model) '_rDCM.mat']),'results')
    
end

end