function generate_band_circos_MainExperiment(FilenameInfo,parcellation_type)
% Helper script that generates the band file for the respective CIRCOS
% analysis, exluding all the regions that had to be omitted in the
% whole-brain effective connectivity analysis.
% 
% Input:
%   FilenameInfo        -- important definitions for the experiment
%   parcellation_type   -- parcellation scheme: (1) Glasser 2016, (2) AAL, (3) Brainetome 2016, (4) Glasser 2016 & Buckner 2011
% 
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2020
% Copyright 2020 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% if FilenameInfo not specified, try to load
if ( isempty(FilenameInfo) )
    
    % get the path
    m_path = mfilename('fullpath');
    m_path = m_path(1:find(m_path=='/',1,'last'));
    
    try
        load(fullfile(m_path,'ConfigFile.mat'))
    catch err
        disp('Need to specify FilenameInfo containing paths!')
        rethrow(err)
    end
end

% set the parcellation folder
parcellations     = {'Glasser2016','AAL','Brainnetome2016','Glasser2016_Buckner2011'};

% circos path
circos_path = fullfile(FilenameInfo.Parcellation_path,'parcellation',parcellations{parcellation_type},'circos_atlas-master');


% generate band.txt
part = importdata(fullfile(circos_path,'subregion_name.txt'));

% define the lobes
lobe={'FRO_L';'INS_L';'LIM_L';'TEM_L';'PAR_L';'OCC_L';'SUB_L';'SUB_R';'OCC_R';'PAR_R';'TEM_R';'LIM_R';'INS_R';'FRO_R'};

% regions in each lobe
num_lobe=[34;6;7;28;19;11;18];
num_lobe=[num_lobe;num_lobe(end:-1:1)];

% specify the indices
index=ones(1,246);  %1
index(163:174)=2;   %4
index(175:188)=3;   %5
index(69:124)=4;    %2
index(125:162)=5;   %3
index(189:210)=6;   %6
index(211:246)=7;   %7
tmp=index(2:2:end);
tmp=15-tmp;
index(2:2:end)=tmp;

% load the missing regions and select only those regions that are present
load(fullfile(FilenameInfo.Data_Path,'VoI_coordinates',['timeseries_VOI_' parcellations{parcellation_type}],'MissingROIs.mat'))
all_regions     = 1:246;
all_regions_cut = setdiff(all_regions,allMissing_regions_ind);

% cut the index
index_old = index;
index(allMissing_regions_ind) = NaN;

% load additional CIRCOS information
[a1,b1,c1,d1]       = textread(fullfile(circos_path,'label_region_orig.txt'),'%s%d%d%s');
[a2,b2,c2,d2,e2]    = textread(fullfile(circos_path,'region_band_orig.txt'),'%s%d%d%d%s');
[a3,b3,c3,d3,e3,f3] = textread(fullfile(circos_path,'subregion_band_name_color_orig.txt'),'%s%d%s%d%d%s');

% define some additional variables
b1_half     = b1(1:length(b1)/2);
c1_half     = c1(1:length(c1)/2);
b1_zeros    = find(b1_half==0);
zero_index  = [1 4 5 2 3 6 7];

% cut out the respective regions
for int = 1:7
    index_last = find(index_old==15-int,1,'last');
    index_first = find(index_old==int,1,'first');
    int_all = index(index_first:index_last);
    int_all = int_all(1:2:end);
    nan_value = find(~isfinite(int_all));
    
    if ( zero_index(int)+1 <= length(b1_zeros) )
        b1_half_cut = b1_half(b1_zeros(int):b1_zeros(int+1)-1);
        c1_half_cut = c1_half(b1_zeros(int):b1_zeros(int+1)-1);
    else
        b1_half_cut = b1_half(b1_zeros(int):end);
        c1_half_cut = c1_half(b1_zeros(int):end);
    end
    
    if ( ~isempty(nan_value) )

        vector_helper = NaN(1,c1_half_cut(end));

        for int2 = 1:length(b1_half_cut)
            vector_helper(b1_half_cut(int2)+1:c1_half_cut(int2)) = int2;
        end

        for int2 = 1:length(nan_value)
            vector_helper(nan_value(int2)) = NaN;
        end

        b1_half_cut_new = [];
        c1_half_cut_new = [];
        
        for int2 = 1:length(b1_half_cut)
            if ( int2 == 1 )
                c1_half_cut_new(int2,1) = sum(vector_helper==int2);
                b1_half_cut_new(int2,1) = 0;
            else
                c1_half_cut_new(int2,1) = sum(vector_helper==int2)+c1_half_cut_new(int2-1);
                b1_half_cut_new(int2,1) = c1_half_cut_new(int2-1,1);
            end
        end

        if ( int == 1 ) 
            b1_half_new = b1_half_cut_new;
            c1_half_new = c1_half_cut_new;
        else
            b1_half_new = [b1_half_new; b1_half_cut_new];
            c1_half_new = [c1_half_new; c1_half_cut_new];
        end
    else
        if ( int == 1 ) 
            b1_half_new = b1_half_cut;
            c1_half_new = c1_half_cut;
        else
            b1_half_new = [b1_half_new; b1_half_cut];
            c1_half_new = [c1_half_new; c1_half_cut];
        end
    end
end

% create full arrays
b1_new = [b1_half_new; b1_half_new];
c1_new = [c1_half_new; c1_half_new];


% open a new file with the adjusted region lables
fid = fopen(fullfile(circos_path,'label_region.txt'),'w');

% write the new file with the adjusted region lables
for int = 1:length(b1_new)
    fprintf(fid,'%s %d %d %s\n',a1{int},b1_new(int),c1_new(int),d1{int});
end

% close file
fclose(fid);

% open a new file with the adjusted region bands
fid2 = fopen(fullfile(circos_path,'region_band.txt'),'w');

% write the new file with the adjusted region bands
for int = 1:length(b1_new)
    fprintf(fid2,'%s %d %d %d %s\n',a2{int},b1_new(int),c1_new(int),d2(int),e2{int});
end

% close file
fclose(fid2);

% open a new file with the adjusted region colors
fid3 = fopen(fullfile(circos_path,'subregion_band_name_color.txt'),'w');

% write the new file with the adjusted region colors
counter = 0;
allindices = [sort([find(index==2,1,'first'), find(index==3,1,'first'), find(index==4,1,'first'),find(index==5,1,'first'),find(index==6,1,'first'),find(index==7,1,'first')]) length(index)+1];
counter2 = 0;

for int = 1:length(a3)
    
    if (int >= allindices(1) )
        if ( length(allindices) > 1 )
            allindices = allindices(2:end);
        end
        counter2 = 0;
    end
    
    if ( isfinite(index(int)) )
        counter = counter + 1;
        fprintf(fid3,'%s %d %s %d %d %s\n',a3{int},counter,c3{int},counter2,counter2+1,f3{int});
        
        if ( mod(int,2) == 0 )
            counter2 = counter2 + 1;
        end 
    end
end

% close file
fclose(fid3);


% get indices
num_lobe = [sum(index==1); sum(index==2); sum(index==3); sum(index==4); sum(index==5); sum(index==6); sum(index==7)];
num_lobe = [num_lobe;num_lobe(end:-1:1)];
index    = index(isfinite(index));
part     = part(all_regions_cut);

% colorbrewer palette
warm={'reds';'oranges';'purd';'rdpu';'ylorrd';'ylorbr';'orrd'};
cold={'ylgn';'blues';'purples';'greens';'ylgnbu';'gnbu';'bugn';'bupu';'pubugn';'greys'};

rand_cold=rand(10,8);
[b,c_I]=sort(rand_cold(:));
[c_x,c_y]=ind2sub(size(rand_cold),c_I(1:78));
rand_warm=rand(7,8);
[b,w_I]=sort(rand_warm(:));
[w_x,w_y]=ind2sub(size(rand_warm),w_I(1:45));

num_w=1;
num_c=1;
color=cell(length(index),1);
for i=1:2:length(index)-1
    if mod(index(i),2)~=0 %cold
        color{i}=strcat(cold{c_x(num_c)},'-9-seq-',num2str(c_y(num_c)+1));
        color{i+1}=color{i};
        num_c=num_c+1;
    else
        color{i}=strcat(warm{w_x(num_w)},'-9-seq-',num2str(w_y(num_w)+1));
        color{i+1}=color{i};
        num_w=num_w+1;
    end
end

% open new files
fid_1=fopen(fullfile(circos_path,'band.txt'),'w');
fid_2=fopen(fullfile(circos_path,'label_bands.txt'),'w');
fid_3=fopen(fullfile(circos_path,'color.txt'),'w');

% write all relevant information
for i = 1:numel(lobe)
    fprintf(fid_1,'chr - lobe%d %s 0 %d chr1\n',i,lobe{i},num_lobe(i));
end

% write all relevant information
num=ones(1,14);
for i = 1:length(index)
    fprintf(fid_1,'band lobe%d %d %s %d %d %s\n',index(i),i,part{i},num(index(i))-1,num(index(i)),color{i});
    fprintf(fid_2,'lobe%d %d %d %s\n',index(i),num(index(i))-1,num(index(i)),part{i});
    fprintf(fid_3,'%d %s\n',i,color{i});
    num(index(i))=num(index(i))+1;
end

% close all files
fclose(fid_1);
fclose(fid_2);
fclose(fid_3);

end