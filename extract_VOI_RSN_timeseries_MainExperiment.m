function extract_VOI_RSN_timeseries_MainExperiment(mode,subject,site)
% Extracts the first principle eigenvariate from pre-specified regions of
% interest (ROI) that represent resting-state network, such as the default
% mode network, salience network, dorsal attention network, and the central
% execution network.
%
% Input:
%   subject             -- subject to be analyzed
%   site                -- site to be analyzed
% 
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2019
% Copyright 2019 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% add SPM to path
addpath(FilenameInfo.SPM_Path)

% initialize spm
spm_jobman('initcfg')


% List of all recording sites 
Site_List = {'Baltimore','Chicago','Dallas','Hartford'};

% List of extraction variants
mode_List = {'timeseries_VOI_saccade','timeseries_VOI_RSN_modes','timeseries_VOI_RSN_nodes'};
mask_List = {'','modes','nodes'};


% get the resting-state networks
if ( mode == 2 )
    ROI_names = {'DMN','DAN','SAN','CEN'};
elseif ( mode == 3 )
    ROI_names = {'PCC','aMPFC','lAG','rAG','dACC','lAI','rAI','laPFC','raPFC','lFEF','rFEF','lIFG','rIFG','lIPS','rIPS'};
end

% confound regressor names
confound_names = {'csf','white_matter','global_signal','dvars','framewise','cosine','trans_','rot_','motion_outlier'};


% specified sites
if ( isempty(site) )
    site_analyze = 1:length(Site_List);
else
    site_analyze = site;
end


% iterate over sites
for site_NR = 1:length(site_analyze)
    
    % get the site
    site = site_analyze(site_NR);
    
    % specify the foldername for respective site
    foldername_site = fullfile(FilenameInfo.Data_Path,Site_List{site},'derivatives','fmriprep');
    
    % get a list of subjects for each site
    Subject_List = dir(fullfile(foldername_site,'sub*.html'));
    
    % specified subject
    if ( isempty(subject) )
        subject_analyze = 1:length(Subject_List);
    else
        subject_analyze = subject;
    end
    
    % iterate over subjects
    for subject = subject_analyze
        
        % asign subject name
        try 
            Subject = Subject_List(subject).name(1:end-5);
            disp(['Processing: ' Subject ' (' Site_List{site} ')'])
        catch
            disp(['No more subjects (' Site_List{site} ')'])
            break
        end
        
        % specify first level folder
        foldername_firstlevel = fullfile(foldername_site,Subject,'firstlevel');
        
        % create first level folder
        if ( ~exist(foldername_firstlevel,'dir') )
            mkdir(foldername_firstlevel)
        end
        
        
        % read in the counfound txv-file
        fid       = fopen(fullfile(foldername_site,Subject,'func',[Subject '_task-rest_desc-confounds_regressors.tsv']));
        confounds = textscan(fid, repmat('%s ',1,1000));
        fclose(fid);
        
        % select the respective columns
        confound_vector = NaN(1,length(confounds));
        for nr_confound = 1:length(confounds)
            confound_vector(nr_confound) = contains(confounds{nr_confound}{1},confound_names);
        end
        
        % get all the columns
        confound_indices = find(confound_vector==1);
        
        % specify the confound matrix
        all_confounds = NaN(length(confounds{1}(2:end)),length(confound_indices));
        for ind_confound = 1:length(confound_indices)
            all_confounds(:,ind_confound) = cell2mat(cellfun(@str2double,confounds{confound_indices(ind_confound)}(2:end),'un',0));
        end
        
        % remove NaNs
        all_confounds(~isfinite(all_confounds)) = 0;
        
        % write confound regressors in text file
        fid = fopen(fullfile(foldername_site,Subject,'func',[Subject '_task-rest_desc-confounds_regressors_final.txt']),'wt');
        for int = 1:size(all_confounds,1)
            fprintf(fid,'%d\t',all_confounds(int,:));
            fprintf(fid,'\n');
        end
        fclose(fid);
        
        % delete potential former files
        if ( exist(fullfile(foldername_site,Subject,'func',['s' Subject '_task-rest_space-MNI152NLin2009cAsym_desc-preproc_bold.nii']),'file') )
            delete(fullfile(foldername_site,Subject,'func',['s' Subject '_task-rest_space-MNI152NLin2009cAsym_desc-preproc_bold.nii']))
        end
        
        % unzip nifti volume
        disp('Extracting 4-D NIFTI volume')
        gunzip(fullfile(foldername_site,Subject,'func',[Subject '_task-rest_space-MNI152NLin2009cAsym_desc-preproc_bold.nii.gz']))

        % get all files from the 4-D nifti volume
        f = spm_select('expand', fullfile(foldername_site,Subject,'func',[Subject '_task-rest_space-MNI152NLin2009cAsym_desc-preproc_bold.nii']));

        % smooth images
        matlabbatch{1}.spm.spatial.smooth.data   = cellstr(f);
        matlabbatch{1}.spm.spatial.smooth.fwhm   = [8 8 8];
        matlabbatch{1}.spm.spatial.smooth.dtype  = 0;
        matlabbatch{1}.spm.spatial.smooth.im     = 0;
        matlabbatch{1}.spm.spatial.smooth.prefix = 's';

        % run the batch and extract the time series
        spm_jobman('run',matlabbatch);

        % clear matlabbatch
        clear matlabbatch

        % delete BOLD signal
        delete(fullfile(foldername_site,Subject,'func',[Subject '_task-rest_space-MNI152NLin2009cAsym_desc-preproc_bold.nii']))
        
        
        % get all files from the 4-D nifti volume
        f = spm_select('expand', fullfile(foldername_site,Subject,'func',['s' Subject '_task-rest_space-MNI152NLin2009cAsym_desc-preproc_bold.nii']));
        
        
        % get the repetition time
        fid         = fopen(fullfile(foldername_site,Subject,'func',[Subject '_task-rest_space-MNI152NLin2009cAsym_desc-preproc_bold.json']));
        json_text   = textscan(fid,'%s');
        TR          = str2double(json_text{1}{3});
        
        % close the file again
        fclose(fid);
        
        
        % delete previous SPM.mat if present
        if ( exist(fullfile(foldername_firstlevel,'SPM.mat'),'file') )
            delete(fullfile(foldername_firstlevel,'SPM.mat'))
        end
        
        
        % specify settings of the firstlevel batch
        matlabbatch{1}.spm.stats.fmri_spec.dir              = {foldername_firstlevel};
        matlabbatch{1}.spm.stats.fmri_spec.timing.units     = 'secs';
        matlabbatch{1}.spm.stats.fmri_spec.timing.RT        = TR;
        matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t    = 16;
        matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t0   = 8;
        
        % asign the nifti volumes
        matlabbatch{1}.spm.stats.fmri_spec.sess.scans = cellstr(f);
        
        % specify additional settings of the firstlevel batch
        matlabbatch{1}.spm.stats.fmri_spec.sess.cond        = struct('name', {}, 'onset', {}, 'duration', {}, 'tmod', {}, 'pmod', {}, 'orth', {});
        matlabbatch{1}.spm.stats.fmri_spec.sess.multi       = {''};
        matlabbatch{1}.spm.stats.fmri_spec.sess.regress     = struct('name', {}, 'val', {});
        matlabbatch{1}.spm.stats.fmri_spec.sess.multi_reg   = {fullfile(foldername_site,Subject,'func',[Subject '_task-rest_desc-confounds_regressors_final.txt'])};
        matlabbatch{1}.spm.stats.fmri_spec.sess.hpf         = 128;
        matlabbatch{1}.spm.stats.fmri_spec.fact             = struct('name', {}, 'levels', {});
        matlabbatch{1}.spm.stats.fmri_spec.bases.hrf.derivs = [0 0];
        matlabbatch{1}.spm.stats.fmri_spec.volt             = 1;
        matlabbatch{1}.spm.stats.fmri_spec.global           = 'None';
        matlabbatch{1}.spm.stats.fmri_spec.mthresh          = 0.8;
        matlabbatch{1}.spm.stats.fmri_spec.mask             = {''};
        matlabbatch{1}.spm.stats.fmri_spec.cvi              = 'AR(1)';
        
        
        % estiamtion of the firstlevel GLM
        matlabbatch{2}.spm.stats.fmri_est.spmmat(1)         = cfg_dep('fMRI model specification: SPM.mat File', substruct('.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
        matlabbatch{2}.spm.stats.fmri_est.write_residuals   = 0;
        matlabbatch{2}.spm.stats.fmri_est.method.Classical  = 1;
        
        
        % delete potential old time series
        if ( ~isempty(dir(fullfile(foldername_firstlevel,'VOI*'))) )
            delete(fullfile(foldername_firstlevel,'VOI*'))
        end
        
        
        % time series folder
        timeseries_folder = fullfile(foldername_firstlevel, mode_List{mode});
        
        % check whether folder exists
        if ( exist(timeseries_folder,'dir') )
            rmdir(timeseries_folder, 's')
        end
        
        
        % extract the time series for each region of interest
        for number_of_regions = 1:length(ROI_names)
            matlabbatch{2+number_of_regions}.spm.util.voi.spmmat(1)                = cfg_dep('Model estimation: SPM.mat File', substruct('.','val', '{}',{2}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
            matlabbatch{2+number_of_regions}.spm.util.voi.adjust                   = NaN;
            matlabbatch{2+number_of_regions}.spm.util.voi.session                  = 1;
            matlabbatch{2+number_of_regions}.spm.util.voi.name                     = ROI_names{number_of_regions};
            matlabbatch{2+number_of_regions}.spm.util.voi.roi{1}.mask.image(1)     = cellstr(fullfile(FilenameInfo.RSN_Path,mask_List{mode},[ROI_names{number_of_regions} '.nii']));
            matlabbatch{2+number_of_regions}.spm.util.voi.roi{1}.mask.threshold    = 0.5;
            matlabbatch{2+number_of_regions}.spm.util.voi.roi{2}.mask.image(1)     = cfg_dep('Model estimation: Analysis Mask', substruct('.','val', '{}',{2}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','mask'));
            matlabbatch{2+number_of_regions}.spm.util.voi.roi{2}.mask.threshold    = 0.5;
            matlabbatch{2+number_of_regions}.spm.util.voi.expression               = 'i1 & i2';
        end
        
        
        % run the batch and extract the time series
        spm_jobman('run',matlabbatch);
        
        % clear matlabbatch
        clear matlabbatch
        
        % create folder
        mkdir(timeseries_folder)
        
        % move the extracted time series to timeseries folder
        movefile(fullfile(foldername_firstlevel, 'VOI*.mat'),timeseries_folder)

        % delete the remaining files
        delete(fullfile(foldername_firstlevel, 'VOI*'))
        
        % delete the smoothed functional volumes
        delete(fullfile(foldername_site,Subject,'func',['s' Subject '_task-rest_space-MNI152NLin2009cAsym_desc-preproc_bold.nii']))
        
        % delete the functional volumes
        if ( exist(fullfile(foldername_site,Subject,'func',[Subject '_task-rest_space-MNI152NLin2009cAsym_desc-preproc_bold.nii']),'file') )
            delete(fullfile(foldername_site,Subject,'func',[Subject '_task-rest_space-MNI152NLin2009cAsym_desc-preproc_bold.nii']))
        end
    end
end

end
