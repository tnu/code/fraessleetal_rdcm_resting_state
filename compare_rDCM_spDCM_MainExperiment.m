function compare_rDCM_spDCM_MainExperiment(mode,model,subgroup)
% Compares the effective connectivity parameters across the different 
% patient subgroups from the BSNIP study as inferred using the regression
% dynamic causal modeling (rDCM) toolbox and spectral DCM (spDCM) in SPM12.
%
% Input:
%   mode          	-- (1) saccade network, (2) RSN (modes), (3) RSN (nodes)
%   model           -- model to be analyzed
%   subgroup       	-- (0) all participants, (1) restrict to healthy controls
% 
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2020
% Copyright 2020 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% close all figures
close all

% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% add path to several relevant toolboxes
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'PBAR')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'fdr_bh')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'cbrewer')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'append_pdfs')))

% analysis file- and foldernames
mode_List       = {'saccade','RSN_modes','RSN_nodes'};
subgroup_name   = {'','_HC'};


% set the SPM path and the path to the experiment
foldername = fullfile(FilenameInfo.Data_Path,'grouplevel_dcm',mode_List{mode});


% load the results array for rDCM
temp          = load(fullfile(foldername,['PosteriorParameterEstimates_model' num2str(model) '_rDCM.mat']));
results_rDCM  = temp.results;

% load the results array for spDCM
temp          = load(fullfile(foldername,['PosteriorParameterEstimates_model' num2str(model) '.mat']));
results_spDCM = temp.results;


% generate subject infromation if necessary
if ( ~exist(fullfile(FilenameInfo.Data_Path,'Information','Subject_Information.mat'),'file') )
    get_subject_information_MainExperiment()
end


% load the subject information
temp            = load(fullfile(FilenameInfo.Data_Path,'Information','Subject_Information.mat'));
Subject_type    = temp.Subject_type;


% load the subject information regarding confounds
temp2           = load(fullfile(FilenameInfo.Data_Path,'Information','Subject_Confounds.mat'));
Subject_age     = temp2.Subject_age;
Subject_gender  = temp2.Subject_gender;
Subject_site    = temp2.Subject_site;

% number of different sites
NR_sites        = max(Subject_site)-1;

% create a confound matrix
confounds = [Subject_age, Subject_gender];
for int = 1:NR_sites
    if ( sum(Subject_site==int) ~= 0 )
        confounds = [confounds, Subject_site==int];
    end
end

% different tests
Subject_type_test = Subject_type;


% find the subjects that are not included in the analysis
Subject_type_test((~isfinite(results_rDCM.A_Matrix_AllSubjects{1,1}))==1) = NaN;


% restrict to healthy controls only
if ( subgroup == 1 )
    Subject_type_test(Subject_type_test~=3) = NaN;
end


% display the number of subjects
disp(['Found: ' num2str(sum(isfinite(Subject_type_test))) ' out of ' num2str(length(Subject_type_test)) ' subjects'])


% asign the data
A_matrix_allSubjects_rDCM	= results_rDCM.A_Matrix_AllSubjects;
A_matrix_allSubjects_spDCM	= results_spDCM.A_Matrix_AllSubjects;


% check whether you have the right subjects (should not be necessary)
if ( length(temp.Subject_name) ~= length(A_matrix_allSubjects_rDCM{1,1}) )
    for int = 1:length(results_rDCM.AllSubjects)
        vector(int) = any(strcmp(results_rDCM.AllSubjects(int),temp.Subject_name));
    end
    
    for int = 1:size(A_matrix_allSubjects_rDCM,1)
        for int2 = 1:size(A_matrix_allSubjects_rDCM,2)
            A_matrix_allSubjects_rDCM{int,int2} = A_matrix_allSubjects_rDCM{int,int2}(vector==1);
        end
    end
    
    for int = 1:size(A_matrix_allSubjects_spDCM,1)
        for int2 = 1:size(A_matrix_allSubjects_spDCM,2)
            A_matrix_allSubjects_spDCM{int,int2} = A_matrix_allSubjects_spDCM{int,int2}(vector==1);
        end
    end
end

% excluded subjects
excluded_subjects = ~isfinite(A_matrix_allSubjects_rDCM{1,1}) | ~isfinite(Subject_type_test) | ~isfinite(Subject_age) | ~isfinite(Subject_gender) | ~isfinite(Subject_site);


% define results arrays
mean_Amatrix_allSubjects_rDCM  = NaN(size(A_matrix_allSubjects_rDCM));
mean_Amatrix_allSubjects_spDCM = NaN(size(A_matrix_allSubjects_spDCM));



% get endogenous parameters
for int = 1:size(A_matrix_allSubjects_rDCM,1)
    for int2 = 1:size(A_matrix_allSubjects_rDCM,2)
        
        % get the mean endogenous connection strength
        mean_Amatrix_allSubjects_rDCM(int,int2) = mean(A_matrix_allSubjects_rDCM{int,int2}(excluded_subjects==0));
        mean_Amatrix_allSubjects_spDCM(int,int2) = mean(A_matrix_allSubjects_spDCM{int,int2}(excluded_subjects==0));
        
    end
end


% figure folder
figure_folder = fullfile(FilenameInfo.Data_Path,'Figures',mode_List{mode},'rDCM_vs_spDCM');

% create the directory for the figures
if ( ~exist(figure_folder,'dir') ) 
    mkdir(figure_folder)
else
    if ( exist(fullfile(figure_folder,['Model' num2str(model) subgroup_name{subgroup+1} '.pdf']),'file') )
        delete(fullfile(figure_folder,['Model' num2str(model) subgroup_name{subgroup+1} '.pdf']))
    end
end


% get the group-level parameters values
temp_rDCM  = mean_Amatrix_allSubjects_rDCM - diag(diag(mean_Amatrix_allSubjects_rDCM)); temp_rDCM = temp_rDCM(temp_rDCM~=0);
temp_spDCM = mean_Amatrix_allSubjects_spDCM - diag(diag(mean_Amatrix_allSubjects_spDCM)); temp_spDCM = temp_spDCM(temp_spDCM~=0);
            
% compute the correlation
[R,p] = corrcoef(temp_rDCM,temp_spDCM);

% compute the linear fit
P     = polyfit(temp_rDCM,temp_spDCM,1);
Y     = polyval(P,[-0.5 0.5]);


% define nice colormpa
[cbrewer_colormap] = cbrewer('div', 'RdBu', 41, 'PCHIP');
cbrewer_colormap   = flipud(cbrewer_colormap);


% set color axis
switch mode
    case 1
        c_lim = [-0.2 0.2];
    case 2
        c_lim = [-0.2 0.2] + subgroup*[0.05 -0.05];
        a_lim = [-0.25 0.25];
    case 3
        c_lim = [-0.1 0.1];
        a_lim = [-0.1 0.2];
end


% specify colormap indices
switch mode
    case 1
        c_vals = linspace(c_lim(1),c_lim(2),41);
    case 2
        c_vals = linspace(c_lim(1),c_lim(2),41);
    case 3
        c_vals = linspace(c_lim(1),c_lim(2),86);
end


% define nice colormap
switch mode
    case 1
        [cbrewer_colormap] = cbrewer('div', 'RdBu', 41, 'PCHIP');
        cbrewer_colormap   = flipud(cbrewer_colormap);
    case 2
        [cbrewer_colormap] = cbrewer('div', 'RdBu', 41, 'PCHIP');
        cbrewer_colormap   = flipud(cbrewer_colormap);
    case 3
        [cbrewer_colormap] = cbrewer('div', 'RdBu', 91, 'PCHIP');
        cbrewer_colormap   = cbrewer_colormap([1:43 49:91],:);
        cbrewer_colormap   = flipud(cbrewer_colormap);
end


% plot feature vector (rDCM)
figure('units','normalized','outerposition',[0 0 1 1])
imagesc(temp_rDCM)
axis square
colormap(cbrewer_colormap)
colorbar
caxis([-0.15 0.15])

% plot feature vector (spDCM)
figure('units','normalized','outerposition',[0 0 1 1])
imagesc(temp_spDCM)
axis square
colormap(cbrewer_colormap)
colorbar
caxis([-0.2 0.2])


% plot the correlation
figure('units','normalized','outerposition',[0 0 1 1])
for int = 1:length(temp_rDCM)
    [~,idx] = min(abs(c_vals-temp_rDCM(int)));
    scatter(temp_rDCM(int),temp_spDCM(int), 400, cbrewer_colormap(idx(1),:), 'filled', 'MarkerEdgeColor', 'w')
    hold on
end
plot([-0.5 0.5],Y,'-','Color',[0.3 0.3 0.3])
if ( p(2,1) < 0.001 )
    text(0.1,a_lim(1)+0.05,['r = ' num2str(round(R(2,1),3)) ', p < 0.001'],'FontSize',12)
else
    text(0.1,a_lim(1)+0.05,['r = ' num2str(round(R(2,1),3)) ', p = ' num2str(round(p(2,1),3))],'FontSize',12)
end
axis square
box off
xlim(a_lim)
ylim(a_lim)
set(gca,'xtick',a_lim(1):0.05:a_lim(2))
set(gca,'ytick',a_lim(1):0.05:a_lim(2))
xlabel('mean parameter estimate (rDCM) [Hz]')
ylabel('mean parameter estimate (spDCM) [Hz]')
title('Across Connections','FontSize',18)

% write a PDF file of the figure
print(gcf, '-dpdf', fullfile(figure_folder,['Model' num2str(model) subgroup_name{subgroup+1}]), '-fillpage')


% define nice colormap
[cbrewer_colormap] = cbrewer('div', 'RdBu', 91, 'PCHIP');
cbrewer_colormap   = cbrewer_colormap([1:40 51:91],:);
cbrewer_colormap   = flipud(cbrewer_colormap);


% specify colormap indices and x-axis
switch mode
    case 1
        c_vals = linspace(-0.5,0.5,80);
        a_lim  = [-0.5 0.5];
    case 2
        c_vals = linspace(-0.5,0.5,80);
        a_lim  = [-0.5 0.5];
    case 3
        c_vals = linspace(-0.25,0.25,80);
        a_lim  = [-0.25 0.25];
end


% which connections to show
switch mode
    case 1
        mask = ones(size(A_matrix_allSubjects_rDCM));
    case 2
        mask = ones(size(A_matrix_allSubjects_rDCM));
    case 3
        sort_temp_rDCM = sort(abs(temp_rDCM),'descend');
        threshold      = sort_temp_rDCM(15);
        mask           = abs(mean_Amatrix_allSubjects_rDCM) >= threshold;
        mask           = mask-diag(diag(mask));
end


% create empty array
R_all = [];


% correlation per parameter
for int = 1:size(A_matrix_allSubjects_rDCM,1)
    for int2 = 1:size(A_matrix_allSubjects_rDCM,2)
        
        % exclude self-connections
        if ( int ~= int2 )
            
            % get the single-subject values
            temp_rDCM  = A_matrix_allSubjects_rDCM{int,int2}(excluded_subjects==0);
            temp_spDCM = A_matrix_allSubjects_spDCM{int,int2}(excluded_subjects==0);
            
            % compute the correlation
            [R,p] = corrcoef(temp_rDCM,temp_spDCM);
            
            % asign correlation coefficient
            R_all(end+1) = R(2,1);
            R_matrix(int,int2) = R(2,1);
            
            % compute the linear fit
            P     = polyfit(temp_rDCM,temp_spDCM,1);
            Y     = polyval(P,[-0.5 0.5]);
            
            if ( mask(int,int2) == 1 )
            
                % plot the correlation
                figure('units','normalized','outerposition',[0 0 1 1])
                for int3 = 1:length(temp_rDCM)
                    [~,idx] = min(abs(c_vals-temp_rDCM(int3)));
                    scatter(temp_rDCM(int3),temp_spDCM(int3), 300, cbrewer_colormap(idx(1),:), 'filled', 'MarkerEdgeColor', 'w')
                    hold on
                end
                hold on
                plot([-0.5 0.5],Y,'-','Color',[0.3 0.3 0.3])
                if ( p(2,1) < 0.001 )
                    text(0.1,-0.8,['r = ' num2str(round(R(2,1),3)) ', p < 0.001'],'FontSize',12)
                else
                    text(0.1,-0.8,['r = ' num2str(round(R(2,1),3)) ', p = ' num2str(round(p(2,1),3))],'FontSize',12)
                end
                axis square
                box off
                xlim(a_lim)
                ylim([-1 1])
                set(gca,'xtick',a_lim(1):a_lim(2):a_lim(2))
                set(gca,'ytick',-1:0.5:1)
                xlabel('parameter estimate (rDCM) [Hz]')
                ylabel('parameter estimate (spDCM) [Hz]')
                title(['Connection: ' results_rDCM.AllRegions{int2} ' -> ' results_rDCM.AllRegions{int}],'FontSize',18)

                % write a PDF file of the figure
                print(gcf, '-dpdf', fullfile(figure_folder,['Model' num2str(model) '_temp1' subgroup_name{subgroup+1}]), '-fillpage')

                % append all PDFs
                append_pdfs(fullfile(figure_folder,['Model' num2str(model) subgroup_name{subgroup+1} '.pdf']),...
                    fullfile(figure_folder,['Model' num2str(model) '_temp1' subgroup_name{subgroup+1} '.pdf']))

                % delete the temporary files
                delete(fullfile(figure_folder,['Model' num2str(model) '_temp1' subgroup_name{subgroup+1} '.pdf']))
            
            end
        end
    end
end


% also plot correlation for the lowest and highest correlation for nodes
if ( mode == 3 )
    
    % iterate over min and max
    for extremes = 1:2

        % find lowest and highest correlation
        switch extremes
            case 1
                [~, index] = min(R_matrix(:));
                x_lim_ext  = [-0.2 0.2];
                y_lim_ext  = [-1 1];
                c_vals     = linspace(-0.2,0.2,80);
            case 2
                [~, index] = max(R_matrix(:));
                x_lim_ext  = [-0.1 0.1];
                y_lim_ext  = [-0.5 0.5];
                c_vals     = linspace(-0.1,0.1,80);
        end

        % find row and column
        [row, column] = ind2sub(size(R_matrix), index);

        % get the single-subject values
        temp_rDCM  = A_matrix_allSubjects_rDCM{row,column}(excluded_subjects==0);
        temp_spDCM = A_matrix_allSubjects_spDCM{row,column}(excluded_subjects==0);

        % compute the correlation
        [R,p] = corrcoef(temp_rDCM,temp_spDCM);

        % compute the linear fit
        P     = polyfit(temp_rDCM,temp_spDCM,1);
        Y     = polyval(P,[-0.5 0.5]);

        % plot the correlation
        figure('units','normalized','outerposition',[0 0 1 1])
        for int3 = 1:length(temp_rDCM)
            [~,idx] = min(abs(c_vals-temp_rDCM(int3)));
            scatter(temp_rDCM(int3),temp_spDCM(int3), 300, cbrewer_colormap(idx(1),:), 'filled', 'MarkerEdgeColor', 'w')
            hold on
        end
        hold on
        plot([-0.5 0.5],Y,'-','Color',[0.3 0.3 0.3])
        if ( p(2,1) < 0.001 )
            text(x_lim_ext(2)/2,y_lim_ext(1)*0.75,['r = ' num2str(round(R(2,1),3)) ', p < 0.001'],'FontSize',12)
        else
            text(x_lim_ext(2)/2,y_lim_ext(1)*0.75,['r = ' num2str(round(R(2,1),3)) ', p = ' num2str(round(p(2,1),3))],'FontSize',12)
        end
        axis square
        box off
        xlim(x_lim_ext)
        ylim(y_lim_ext)
        set(gca,'xtick',x_lim_ext(1):x_lim_ext(2):x_lim_ext(2))
        set(gca,'ytick',y_lim_ext(1):y_lim_ext(2):y_lim_ext(2))
        xlabel('parameter estimate (rDCM) [Hz]')
        ylabel('parameter estimate (spDCM) [Hz]')
        title(['Connection: ' results_rDCM.AllRegions{column} ' -> ' results_rDCM.AllRegions{row}],'FontSize',18)

        % write a PDF file of the figure
        print(gcf, '-dpdf', fullfile(figure_folder,['Model' num2str(model) '_temp1' subgroup_name{subgroup+1}]), '-fillpage')

        % append all PDFs
        append_pdfs(fullfile(figure_folder,['Model' num2str(model) subgroup_name{subgroup+1} '.pdf']),...
            fullfile(figure_folder,['Model' num2str(model) '_temp1' subgroup_name{subgroup+1} '.pdf']))

        % delete the temporary files
        delete(fullfile(figure_folder,['Model' num2str(model) '_temp1' subgroup_name{subgroup+1} '.pdf']))

    end
end

% create a mask indicating within module connections
if ( mode == 3 )
    within_module_mask              = zeros(15);
    within_module_mask(1:4,1:4)     = 1;
    within_module_mask(5:9,5:9)     = 1;
    within_module_mask(10:15,10:15)	= 1;
    within_module_mask              = within_module_mask + eye(15);
    within_module_mask              = within_module_mask(:);
    R_WithinModule                  = R_matrix(within_module_mask==1);
    R_BetweenModule                	= R_matrix(within_module_mask==0);
end
    
% change colormap
added_value = length(R_all)/3;

% new colormap
[cbrewer_colormap2] = cbrewer('seq', 'Oranges', length(R_all)+added_value, 'PCHIP');

% sort the order
R_all = sort(R_all);

% sort the order
if ( mode == 3 )
    R_WithinModule  = sort(R_WithinModule);
    R_BetweenModule	= sort(R_BetweenModule);
end


% Fisher r-to-z transformation
Z_all = atanh(R_all);

% assess significance of correlations
[~,p,~,tstats] = ttest(Z_all);

% compute back-transformed mean and standard deviation
R_mean = tanh(mean(Z_all));
R_std  = tanh(std(Z_all));


% output the test results
fprintf('Average Pearson correlation (r): %f (%f) \n', R_mean, R_std)
fprintf('One-sample t-test: t(%d,%d) = %f, p = %f \n', 1, tstats.df, tstats.tstat, p)


% look at whether there is a difference in within or between module connections
if ( mode == 3 )
    
    % line break
    fprintf('\n')

    % Fisher r-to-z transformation
    Z_WithinModule  = atanh(R_WithinModule);
    Z_BetweenModule = atanh(R_BetweenModule);
    
    % assess significance of correlations
    [~,p,~,tstats] = ttest2(Z_WithinModule,Z_BetweenModule);
    
    % compute back-transformed mean and standard deviation
    R_WithinModule_mean = tanh(mean(Z_WithinModule));
    R_BetweenModule_mean = tanh(mean(Z_BetweenModule));
    
    % output the test results
    fprintf('Two sample t-test: t(%d,%d) = %f, p = %f \n', 1, tstats.df, tstats.tstat, p)

end



% plot all correlation coefficients and the average
figure('units','normalized','outerposition',[0 0 1 1])
hold on
for int = 1:length(R_all)
    scatter(mvnrnd(1,0.00001,1),R_all(int), 300, cbrewer_colormap2(int+added_value,:), 'filled', 'MarkerEdgeColor', 'w')
end
scatter(1,R_mean, 500, [0.3 0.3 0.3], 'filled', 'MarkerEdgeColor', 'w')
text(1.2,R_mean,['mean r = ' num2str(round(R_mean,2))],'FontSize',12,'Color',[0.3 0.3 0.3])

% add between and within network connection analysis
if ( mode == 3 )
    for int = 1:length(R_WithinModule)
        scatter(mvnrnd(2,0.00001,1),R_WithinModule(int), 300, cbrewer_colormap2(int+added_value,:), 'filled', 'MarkerEdgeColor', 'w')
    end
    scatter(2,R_WithinModule_mean, 500, [0.3 0.3 0.3], 'filled', 'MarkerEdgeColor', 'w')

    for int = 1:length(R_BetweenModule)
        scatter(mvnrnd(2.5,0.00001,1),R_BetweenModule(int), 300, cbrewer_colormap2(int+added_value,:), 'filled', 'MarkerEdgeColor', 'w')
    end
    scatter(2.5,R_BetweenModule_mean, 500, [0.3 0.3 0.3], 'filled', 'MarkerEdgeColor', 'w')
end

axis square
box off
ylim([-0.1 1])
set(gca,'ytick',0:0.5:1)
ylabel('correlation coefficient')
title('Mean Correlation Coefficient','FontSize',18)

% specify x-axis settings
switch mode
    case 2
        xlim([0.5 1.5])
        set(gca,'xtick',1)
        set(gca,'xticklabels',{''},'FontSize',16)
    case 3
        xlim([0.5 3])
        set(gca,'xtick',[1 2 2.5])
        set(gca,'xticklabels',{'all','within','between'},'FontSize',16)
        xtickangle(30)
end

% write a PDF file of the figure
print(gcf, '-dpdf', fullfile(figure_folder,['Model' num2str(model) '_temp1' subgroup_name{subgroup+1}]), '-fillpage')

% append all PDFs
append_pdfs(fullfile(figure_folder,['Model' num2str(model) subgroup_name{subgroup+1} '.pdf']),...
    fullfile(figure_folder,['Model' num2str(model) '_temp1' subgroup_name{subgroup+1} '.pdf']))

% delete the temporary files
delete(fullfile(figure_folder,['Model' num2str(model) '_temp1' subgroup_name{subgroup+1} '.pdf']))


end