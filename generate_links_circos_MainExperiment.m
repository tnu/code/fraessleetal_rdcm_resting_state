function generate_links_circos_MainExperiment(adjacency_matrix,parcellation_type,filename,circos_opt)
% Takes an adjacency matrix and creates the link file that is needed by
% Circos to create a connectogram. The filename of the link_file also has
% to be provided to the function.
% 
% Input:
%   adjacency_matrix    -- adjacency matrix
%   parcellation_type   -- parcellation scheme: (1) Glasser 2016, (2) AAL
%   filename            -- filename for storing the Circos link file
%   circos_opt          -- different options that specify the Circos plot
%   
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2020
% Copyright 2020 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% set the parcellation
parcellations = {'Glasser2016','AAL','Brainnetome2016','Glasser2016_Buckner2011'};


% represent strength of connection 
if ( strcmp(circos_opt.normalize,'relative') )
    adjacency_matrix_normalized = abs(adjacency_matrix);
    binary_mask = adjacency_matrix_normalized ~= 0;
    x(1)        = min(adjacency_matrix_normalized(adjacency_matrix_normalized~=0));
    x(2)        = max(adjacency_matrix_normalized(adjacency_matrix_normalized~=0));
    P           = polyfit(x,[1 circos_opt.scaling],1);
    adjacency_matrix_normalized = P(1) * adjacency_matrix_normalized + P(2);
    adjacency_matrix_normalized = zeros(size(adjacency_matrix_normalized)) + binary_mask .* adjacency_matrix_normalized;
elseif ( strcmp(circos_opt.normalize,'absolute') )
    adjacency_matrix_normalized = abs(adjacency_matrix);
    min_value                   = min(adjacency_matrix_normalized(adjacency_matrix_normalized~=0));
    adjacency_matrix_normalized = adjacency_matrix_normalized ./ min_value;
elseif ( strcmp(circos_opt.normalize,'none') )
    binary_mask                 = adjacency_matrix ~= 0;
    adjacency_matrix_normalized = zeros(size(adjacency_matrix)) + binary_mask .* circos_opt.scaling;
end

% represent the color of connections
if ( strcmp(circos_opt.col_type,'relative') )
    
    % get the adjacency matrix
    adjacency_matrix_color = adjacency_matrix;
    binary_mask = adjacency_matrix_color ~= 0;
    
    % get min and max of values
    x(1)        = min(adjacency_matrix_color(adjacency_matrix_color~=0));
    x(2)        = max(adjacency_matrix_color(adjacency_matrix_color~=0));
    
    % get color scaling
    x_abs       = max(abs(x));
    P           = polyfit([-1*x_abs x_abs],[1 size(circos_opt.col,1)],1);
    adjacency_matrix_color = P(1) * adjacency_matrix_color + P(2);
    adjacency_matrix_color = zeros(size(adjacency_matrix_color)) + binary_mask .* adjacency_matrix_color;
    adjacency_matrix_color = round(adjacency_matrix_color);
    
elseif ( strcmp(circos_opt.col_type,'absolute') )
    
    % get the adjacency matrix
    adjacency_matrix_color = adjacency_matrix;
    
    % limit the range of values for plotting
    for int = 1:size(adjacency_matrix_color,1)
        for int2 = 1:size(adjacency_matrix_color,2)
            if ( adjacency_matrix_color(int,int2) > circos_opt.col_scale )
                adjacency_matrix_color(int,int2) = circos_opt.col_scale;
            elseif ( adjacency_matrix_color(int,int2) < -1*circos_opt.col_scale )
                adjacency_matrix_color(int,int2) = -1*circos_opt.col_scale;
            end
        end
    end
    
    % get present connections
    binary_mask = adjacency_matrix_color ~= 0;
    
    % get color scaling
    P = polyfit([-1*circos_opt.col_scale circos_opt.col_scale],[1 size(circos_opt.col,1)],1);
    adjacency_matrix_color = P(1) * adjacency_matrix_color + P(2);
    adjacency_matrix_color = zeros(size(adjacency_matrix_color)) + binary_mask .* adjacency_matrix_color;
    adjacency_matrix_color = round(adjacency_matrix_color);
    
elseif ( strcmp(circos_opt.col_type,'relative_sep') )
    
    % get the adjacency matrix
    adjacency_matrix_color = adjacency_matrix;
    binary_mask            = adjacency_matrix_color ~= 0;
    
    % get positive and negative part
    adjacency_matrix_color_neg = adjacency_matrix_color .* (adjacency_matrix_color < 0);
    adjacency_matrix_color_pos = adjacency_matrix_color .* (adjacency_matrix_color > 0);
    
    % get min of negative and max of positive
    x(1)        = min(adjacency_matrix_color_neg(adjacency_matrix_color_neg~=0));
    x(2)        = max(adjacency_matrix_color_pos(adjacency_matrix_color_pos~=0));
    
    % get negative slope and positive slope seperately
    P_neg = polyfit([x(1) 0],[1 ceil(size(circos_opt.col,1)/2)],1);
    P_pos = polyfit([0 x(2)],[ceil(size(circos_opt.col,1)/2) size(circos_opt.col,1)],1);
    
    % transform the weights into colors
    adjacency_matrix_color_neg = P_neg(1) * adjacency_matrix_color_neg + P_neg(2); 
    adjacency_matrix_color_pos = P_pos(1) * adjacency_matrix_color_pos + P_pos(2);
    
    % restricted to negative values
    adjacency_matrix_color_neg = zeros(size(adjacency_matrix_color_neg)) + (adjacency_matrix_color < 0) .* adjacency_matrix_color_neg;
    adjacency_matrix_color_pos = zeros(size(adjacency_matrix_color_pos)) + (adjacency_matrix_color > 0) .* adjacency_matrix_color_pos;
    
    % bring negative and positive together
    adjacency_matrix_color = round(adjacency_matrix_color_neg + adjacency_matrix_color_pos);
    
else
    
    % seperate positive and negative values
    adjacency_matrix_color = adjacency_matrix;
    adjacency_matrix_color = (adjacency_matrix_color ~= 0) + (adjacency_matrix_color > 0);
    
end


% select the Circos folder for the respective parcellation
foldername = fullfile(FilenameInfo.Parcellation_path,'parcellation',parcellations{parcellation_type},'circos_atlas-master');

% get the names of the brain regions and the start/end
if ( parcellation_type == 3 )
    [lobe,~,~,st,en,~]  = textread(fullfile(foldername,'subregion_band_name_color.txt'),'%s%d%s%d%d%s');
end

% open the textfile to store the results
fid = fopen(filename,'w');

% write the links with red/green color (red = inhibitory, green = exitatory) 
for n = 1:size(adjacency_matrix,1)
    for i = 1:size(adjacency_matrix,2)
        if adjacency_matrix(n,i) ~= 0
            
            % set the color
            col     = circos_opt.col(adjacency_matrix_color(n,i),:);
            col_txt = [num2str(col(1)) ',' num2str(col(2)) ',' num2str(col(3)) ',1']; 
            
            % write the Circos text file
            fprintf(fid,'%s %d %d %s %d %d val=%f,col=%s\n',lobe{n},st(n),en(n),lobe{i},st(i),en(i),abs(adjacency_matrix_normalized(n,i)),col_txt);
            
        end
    end
end

% close the file
fclose(fid);

% display the next step for Cicros analysis
disp(' ')
disp('For CIRCOS Analysis: Change path to link file:')
disp(filename)

end