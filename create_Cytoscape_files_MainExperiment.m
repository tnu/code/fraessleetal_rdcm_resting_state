function create_Cytoscape_files_MainExperiment(adjacency_matrix,parcellation_type,filename_Cytoscape)
% Takes an adjacency matrix and creates the node and edge files that are 
% needed by BrainNet to create a volume plot. The folder and filename of 
% the BrainNet file also have to be provided to the function.
% 
% Input:
%   adjacency_matrix    -- matrix encoding the connectivity pattern
%   parcellation_type   -- parcellation scheme: (1) Glasser 2016, (2) AAL, (3) Brainetome 2016, (4) Glasser 2016 & Buckner 2011
%   filename_Cytoscape 	-- filename of Cytoscape files
% 
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2020
% Copyright 2020 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% set the parcellation
parcellations       = {'Glasser2016','AAL','Brainnetome2016','Glasser2016_Buckner2011'};


% add the SPM path
addpath(FilenameInfo.SPM_Path)


% set the parcellation folder 
parcellation_folder = fullfile(FilenameInfo.Parcellation_path,'parcellation',parcellations{parcellation_type},'all_masks');

% set the project folder
foldername = FilenameInfo.Data_Path;


% load the brain regions that entered the DCM analyses
files = dir(fullfile(parcellation_folder, '/*nii'));

% load the missing regions
MissingRegions = load(fullfile(foldername, 'VoI_coordinates', ['timeseries_VOI_' parcellations{parcellation_type}], 'MissingROIs.mat'));

% get all the regions
region_ind = 1:length(files);

% get the included regions
region_ind_cut = setdiff(region_ind,MissingRegions.allMissing_regions_ind);


% restrict to only the regions that are not missing
files = files(region_ind_cut);


% get the lobes
if ( parcellation_type == 3 )
    [lobe,~,~,~,~,~]  = textread(fullfile(FilenameInfo.Parcellation_path,'parcellation',parcellations{parcellation_type},'circos_atlas-master','subregion_band_name_color.txt'),'%s%d%s%d%d%s');
end

% set the lobe names
if ( parcellation_type == 3 )
    lobe_names = {'FRO';'INS';'LIM';'TEM';'PAR';'OCC';'SUB'};
end


% delete previous Cytoscape file
if ( exist(filename_Cytoscape,'file') )
    delete(filename_Cytoscape)
end


% create a txt file that can be read by Cytoscape where each row is an edge
fileID = fopen(filename_Cytoscape,'w');


% write the title
fprintf(fileID,'%s\t%s\t%s\t%s\n','Source','Target','directed','value');


% write the links with red/green color (red = inhibitory, green = exitatory) 
for n = 1:size(adjacency_matrix,1)
    for i = 1:size(adjacency_matrix,2)
        
        % write a connection if not self-connection or connection absent
        if ( (n ~= i) && (adjacency_matrix(n,i) ~= 0) )
            
            % adapt source names to have them ordered
            i_txt = ['00' num2str(i)];
            i_txt = i_txt(end-2:end);
            
            % adapt target names to have them ordered
            n_txt = ['00' num2str(n)];
            n_txt = n_txt(end-2:end);
            
            % write the next edge
            fprintf(fileID,'%s\t%s\tTRUE\t%d\n',i_txt,n_txt,adjacency_matrix(n,i));
            
        end
    end
end

% close the file
fclose(fileID);


% cell object containing lobe indices
lobe_indices = cell(1,7);

% line break
fprintf('\n')

% display association of regions and lobes
for int = 1:length(lobe_names)
    for int2 = 1:length(lobe)
        if( (str2double(lobe{int2}(5:end)) == int) | (str2double(lobe{int2}(5:end)) == 15-int) )
            lobe_indices{int} = [lobe_indices{int} int2];
        end
    end
    
    fprintf('Lobe %d (%s): min=%d - max=%d \n',int,lobe_names{int},min(lobe_indices{int}),max(lobe_indices{int}))
    
end

end