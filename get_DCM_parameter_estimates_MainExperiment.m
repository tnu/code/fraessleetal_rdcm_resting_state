function get_DCM_parameter_estimates_MainExperiment(mode,model)
% Get the individual parameter estimates from the spectral Dynamic Causal 
% Models (DCMs) for the resting state dataset of the B_SNIP1 study.
% Parameter estimates have been computed using standard routines in SPM.
% 
% This function reads the DCM files that have been estimated using
% spectral DCM. The function stores the individual parameter estimates
% for the effective connectivity patterns.
% 
% Input:
%   mode                -- (1) saccade network, (2) RSN (modes), (3) RSN (nodes)
%   model               -- model to analyze
%   
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2020
% Copyright 2020 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% add SPM to path
addpath(FilenameInfo.SPM_Path)

% initialize spm
spm_jobman('initcfg')


% get the site list
Site_List = {'Baltimore','Chicago','Dallas','Hartford'};

% get names
mode_List = {'saccade','RSN_modes','RSN_nodes'};


% define the filename
filename = ['DCM_RestingState_model' num2str(model) '.mat'];


% get the excluded subjects
exclude_subjects  = textread(fullfile(FilenameInfo.Data_Path,'exclusions.txt'),'%s');
exclude_subjects2 = textread(fullfile(FilenameInfo.Data_Path,'exclusions_rDCM.txt'),'%s');
exclude_subjects  = [exclude_subjects; exclude_subjects2];


% foldername for the center coordinates
VoI_foldername = fullfile(FilenameInfo.Data_Path,'VoI_coordinates',['timeseries_VOI_' mode_List{mode}]);

% get complete subject list
temp             = load(fullfile(VoI_foldername,'VoI_CenterCoordinates_AllSubjects.mat'));
Subject_List_all = temp.Subject_List_all;


% iterate over all subjects
vector = ones(length(Subject_List_all),1);
for int = 1:length(Subject_List_all)
    for int2 = 1:length(exclude_subjects)
        if ( strcmp(Subject_List_all{int},exclude_subjects{int2}) )
            vector(int) = 0;
        end
    end
end

% iterate over all subjects
for int = 1:length(Subject_List_all)
    for int2 = 1:length(exclude_subjects2)
        if ( strcmp(Subject_List_all{int},exclude_subjects2{int2}) )
            vector(int) = 0;
        end
    end
end

% binarize vector
vector = vector == 1;


% iterate over sites
for site = 1:length(Site_List)
    
    % specify the foldername for respective site
    foldername_site = fullfile(FilenameInfo.Data_Path, Site_List{site}, 'derivatives', 'fmriprep');
    
    % get a list of subjects for each site
    Subject_List = dir(fullfile(foldername_site,'sub*html'));
    
    % iterate over all subjects
    for subject = 1:length(Subject_List)

        % subject name
        Subject = Subject_List(subject).name(1:end-5);

        % set the directory
        foldername_DCM = fullfile(foldername_site, Subject, 'firstlevel_dcm', mode_List{mode});
        
        % get the data
        if ( ~any(strcmp(Subject,exclude_subjects)) && exist(fullfile(foldername_DCM,filename),'file') )

            try
            
                % load the DCM
                warning off
                temp    = load(fullfile(foldername_DCM,filename));
                DCM     = temp.DCM;
                warning on
                
                % load a dummy result to get network size
                foldername_dummy = fullfile(FilenameInfo.Data_Path, 'Baltimore', 'derivatives', 'fmriprep', 'sub-S0015SRH1', 'firstlevel_dcm', mode_List{mode});
                file_dummy = dir(fullfile(foldername_dummy, filename));
                warning off
                temp = load(fullfile(foldername_dummy,file_dummy(1).name));
                warning on
                
                % define dummy connectivity and driving input matrices
                if ( size(DCM.Ep.A,1) ~= size(temp.DCM.Ep.A,1) )
                    DCM.Ep.A = NaN(size(temp.DCM.Ep.A,1),size(temp.DCM.Ep.A,2));
                    DCM.Ep.a = NaN(size(temp.DCM.Ep.a,1),size(temp.DCM.Ep.a,2));
                    DCM.Ep.b = NaN(size(temp.DCM.Ep.b,1),size(temp.DCM.Ep.b,2));
                    DCM.Ep.c = NaN(size(temp.DCM.Ep.c,1),size(temp.DCM.Ep.c,2));

                    % define dummy time series
                    DCM.Y.csd              = NaN(size(temp.DCM.Y.csd));
                    DCM.Hc                 = NaN(size(temp.DCM.Hc));
                    DCM.Rc                 = NaN(size(temp.DCM.Rc));

                    % define a dummy negative free energy
                    DCM.F = NaN;
                    
                    % define a dummy time
                    DCM.time = NaN;
                    
                    % display subject name
                    disp(['Subject: ' Subject ' (' Site_List{site} ' - ' num2str(subject) ') - missing (wrong)'])
                
                else
                    
                    % display subject name
                    disp(['Subject: ' Subject ' (' Site_List{site} ' - ' num2str(subject) ') - found'])
                    
                end
                
            catch
                
                % load a dummy result to get network size
                foldername_dummy = fullfile(FilenameInfo.Data_Path, 'Baltimore', 'derivatives', 'fmriprep', 'sub-S0015SRH1', 'firstlevel_dcm', mode_List{mode});
                file_dummy = dir(fullfile(foldername_dummy, filename));
                warning off
                temp = load(fullfile(foldername_dummy,file_dummy(1).name));
                warning on
                
                % define dummy connectivity and driving input matrices
                DCM.Ep.A = NaN(size(temp.DCM.Ep.A,1),size(temp.DCM.Ep.A,2));
                DCM.Ep.a = NaN(size(temp.DCM.Ep.a,1),size(temp.DCM.Ep.a,2));
                DCM.Ep.b = NaN(size(temp.DCM.Ep.b,1),size(temp.DCM.Ep.b,2));
                DCM.Ep.c = NaN(size(temp.DCM.Ep.c,1),size(temp.DCM.Ep.c,2));

                % define dummy time series
                DCM.Y.csd              = NaN(size(temp.DCM.Y.csd));
                DCM.Hc                 = NaN(size(temp.DCM.Hc));
                DCM.Rc                 = NaN(size(temp.DCM.Rc));
                
                % define a dummy negative free energy
                DCM.F = NaN;
                
                % define a dummy time
                DCM.time = NaN;
                
                % display subject name
                disp(['Subject: ' Subject ' (' Site_List{site} ' - ' num2str(subject) ') - missing (wrong)'])
                
            end

            % clear the DCM file
            clear temp

        else

            % display subject name
            disp(['Subject: ' Subject ' (' Site_List{site} ' - ' num2str(subject) ') - missing'])

            % load a dummy result to get network size
            foldername_dummy = fullfile(FilenameInfo.Data_Path, 'Baltimore', 'derivatives', 'fmriprep', 'sub-S0015SRH1', 'firstlevel_dcm', mode_List{mode});
            file_dummy = dir(fullfile(foldername_dummy, filename));
            warning off
            temp = load(fullfile(foldername_dummy,file_dummy(1).name));
            warning on

            % define dummy connectivity and driving input matrices
            DCM.Ep.A = NaN(size(temp.DCM.Ep.A,1),size(temp.DCM.Ep.A,2));
            DCM.Ep.a = NaN(size(temp.DCM.Ep.a,1),size(temp.DCM.Ep.a,2));
            DCM.Ep.b = NaN(size(temp.DCM.Ep.b,1),size(temp.DCM.Ep.b,2));
            DCM.Ep.c = NaN(size(temp.DCM.Ep.c,1),size(temp.DCM.Ep.c,2));

            % define dummy time series
            DCM.Y.csd              = NaN(size(temp.DCM.Y.csd));
            DCM.Hc                 = NaN(size(temp.DCM.Hc));
            DCM.Rc                 = NaN(size(temp.DCM.Rc));

            % define a dummy negative free energy
            DCM.F = NaN;
            
            % define a dummy time
            DCM.time = NaN;

            % clear the DCM file
            clear temp

        end

        % define cells
        if ( site == 1 && subject == 1 )
            A_Matrix_AllSubjects    = cell(size(DCM.Ep.A,1),size(DCM.Ep.A,2));
            a_Matrix_AllSubjects    = cell(size(DCM.Ep.a,1),size(DCM.Ep.a,2));
            b_Matrix_AllSubjects    = cell(size(DCM.Ep.b,1),size(DCM.Ep.b,2));
            c_Matrix_AllSubjects    = cell(size(DCM.Ep.c,1),size(DCM.Ep.c,2));

            F_AllSubjects         	= cell(1,1);
            
            Time_spDCM_AllSubjects 	= cell(1,1);

            csd_pred_AllSubjects   	= cell(length(Subject_List),1);
            csd_source_AllSubjects  = cell(length(Subject_List),1);
        end

        % asign the a values for the endogenous parameters in each subject
        for int = 1:size(DCM.Ep.A,1)
            for int2 = 1:size(DCM.Ep.A,2)
                A_Matrix_AllSubjects{int,int2} = [A_Matrix_AllSubjects{int,int2}; DCM.Ep.A(int,int2)];
            end 
        end

        % asign the a values for the driving parameters in each subject
        for int = 1:size(DCM.Ep.a,1)
            for int2 = 1:size(DCM.Ep.a,2)
                a_Matrix_AllSubjects{int,int2} = [a_Matrix_AllSubjects{int,int2}; DCM.Ep.a(int,int2)];
            end 
        end
        
        for int = 1:size(DCM.Ep.b,1)
            for int2 = 1:size(DCM.Ep.b,2)
                b_Matrix_AllSubjects{int,int2} = [b_Matrix_AllSubjects{int,int2}; DCM.Ep.b(int,int2)];
            end 
        end
        
        for int = 1:size(DCM.Ep.c,1)
            for int2 = 1:size(DCM.Ep.c,2)
                c_Matrix_AllSubjects{int,int2} = [c_Matrix_AllSubjects{int,int2}; DCM.Ep.c(int,int2)];
            end 
        end


        % asign the measured and predicted time series
        csd_pred_AllSubjects{subject,1}     = DCM.Hc;
        csd_source_AllSubjects{subject,1}	= DCM.Hc + DCM.Rc;


        % asign the negative free energy
        F_AllSubjects{1,1}	= [F_AllSubjects{1,1}; DCM.F];
        
        
        % asign the run-time
        Time_spDCM_AllSubjects{1,1} = [Time_spDCM_AllSubjects{1,1}; DCM.time];


        % asign the subject name
        if ( site == 1 && subject == 1 )
            results.AllSubjects{subject}        = Subject;
            results_signal.AllSubjects{subject} = Subject;
        else
            results.AllSubjects{end+1}          = Subject;
            results_signal.AllSubjects{end+1}   = Subject;
        end
        
        
        % asign the region names
        if ( site == 1 && subject == 1 )
            results.AllRegions        = DCM.Y.name;
            results_signal.AllRegions = DCM.Y.name;
        end
    end
end
    
% asign the results
results.A_Matrix_AllSubjects               = A_Matrix_AllSubjects;
results.a_Matrix_AllSubjects               = a_Matrix_AllSubjects;
results.b_Matrix_AllSubjects               = b_Matrix_AllSubjects;
results.c_Matrix_AllSubjects               = c_Matrix_AllSubjects;
results.F_AllSubjects                      = F_AllSubjects;
results.Time_spDCM_AllSubjects             = Time_spDCM_AllSubjects;

results_signal.csd_pred_AllSubjects        = csd_pred_AllSubjects;
results_signal.csd_source_AllSubjects      = csd_source_AllSubjects;
results_signal.F_AllSubjects               = F_AllSubjects;
results_signal.Time_spDCM_AllSubjects      = Time_spDCM_AllSubjects;


% display whether subject have to be re-run
A_Matrix_IncludedSubjects = A_Matrix_AllSubjects{int,int2}(vector);
disp(['Number of missing subjects: ' num2str(sum(isnan(A_Matrix_IncludedSubjects)))])

% output subjects that are missing
if ( sum(isnan(A_Matrix_IncludedSubjects)) > 0 )
    
    % number of subjects
    NrSub_Site = NaN(length(Site_List),1);
    
    % get number of subjects
    for int = 1:length(Site_List)
    
        % specify the foldername for respective site
        foldername_site = fullfile(FilenameInfo.Data_Path, Site_List{int}, 'derivatives', 'fmriprep');
        
        % get number of subjects for each site
        NrSub_Site(int) = length(dir(fullfile(foldername_site,'sub*html')));
        
    end
    
    % check if subjects match
    if ( sum(NrSub_Site) ~= length(vector) )
        disp('Number of subjects does not agree')
    end

    % find which subjects are missing
    temp = [];
    for int = 1:length(vector)
        if (vector(int)==1 && ~isfinite(results.A_Matrix_AllSubjects{1,1}(int)))
            temp = [temp, int];
        end
    end

    % output subject numbers that are missing
    fprintf(['Number of missing subjects: ' repmat('%d ',1,length(temp)) '\n'],temp)
    
    % get site and subject ID
    for int = 1:length(temp)
        temp_ID = temp(int);
        site_ID = 0;
        int2    = 0;
        while temp_ID > 0
            int2    = int2 + 1;
            site_ID = site_ID + 1;
            temp_ID = temp_ID - NrSub_Site(int2);
        end    
        fprintf('Re-run subject: %d - %d \n',site_ID,temp_ID+NrSub_Site(int2))
    end
    
    fprintf(['Subjects per site: ' repmat('%d ',1,length(NrSub_Site)) '\n'],NrSub_Site)
    
end


% create the results folder
if ( ~exist(fullfile(FilenameInfo.Data_Path,'grouplevel_dcm',mode_List{mode}),'dir') )
    mkdir(fullfile(FilenameInfo.Data_Path,'grouplevel_dcm',mode_List{mode}))
end

% save the estimated result
if ( ~isempty(results) )
    save(fullfile(FilenameInfo.Data_Path,'grouplevel_dcm',mode_List{mode},['PosteriorParameterEstimates_model' num2str(model) '.mat']), 'results', '-v7.3')
end

% save the estimated result
if ( ~isempty(results_signal) )
    save(fullfile(FilenameInfo.Data_Path,'grouplevel_dcm',mode_List{mode},['MeasuredPredictedSignal_model' num2str(model) '.mat']), 'results_signal', '-v7.3')
end

end