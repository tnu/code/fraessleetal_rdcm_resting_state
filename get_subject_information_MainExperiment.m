function get_subject_information_MainExperiment()
% Reads the subject information for each subject individually from the 
% B_SNIP1 study. The function reads the xls File and gets the label of each
% subject.
% 
% Input:
% 
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2020
% Copyright 2020 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% define the foldername
foldername = FilenameInfo.Data_Path;


% list of different sites
site_list = {'GP','GT','JS','MB','MK','CT'};


% read the xls file
[NUM,TXT,~]     = xlsread(fullfile(foldername, 'Information', 'B-SNIP_1_Biomarker_AD_20171012.xlsx'));
Subject_ID      = TXT(2:end,3);
Subject_TYPE    = TXT(2:end,9);
Subject_SUBTYPE = TXT(2:end,11);
Subject_BIOTYPE = TXT(2:end,14);
Subject_PID     = NUM(1:end,1);

% get confound information
Subject_AGE     = NUM(1:end,18); Subject_AGE(Subject_AGE==999) = NaN;
Subject_GENDER  = NUM(1:end,19); Subject_GENDER(Subject_GENDER==999) = NaN;

% get site as a confound
Subject_SITE_t  = TXT(2:end,4);
Subject_SITE    = NaN(length(Subject_SITE_t),1);
for int = 1:length(Subject_SITE_t)
    try
        Subject_SITE(int) = find(strcmp(Subject_SITE_t(int),site_list));
    catch
        Subject_SITE(int) = NaN;
    end
end


% get the complete fMRI subject list
temp             = load(fullfile(foldername,'grouplevel_dcm','RSN_modes','PosteriorParameterEstimates_model1.mat'));
Subject_List_all = temp.results.AllSubjects;

% create array for all existing subjects
Subject_exist   = NaN(length(Subject_List_all),1);
Subject_type    = NaN(length(Subject_List_all),1);
Subject_subtype = NaN(length(Subject_List_all),1);
Subject_biotype = NaN(length(Subject_List_all),1);
Subject_pid     = NaN(length(Subject_List_all),1);
Subject_age     = NaN(length(Subject_List_all),1);
Subject_gender  = NaN(length(Subject_List_all),1);
Subject_site  	= NaN(length(Subject_List_all),1);

% iterate over subjects and compare
for s = 1:length(Subject_List_all)
    
    % get the subject ID
    sub_temp = Subject_List_all{s}(5:end-1);
    
    % check whether subject has information in xlsx sheet
    Subject_exist(s) = any(contains(Subject_ID,sub_temp));
    
    % get the subject type (label)
    % code: NaN = empty, 1 = Patient, 2 = Family, 3 = normal control
    if ( Subject_exist(s) == 0 )
        Subject_type(s) = NaN;
    else
        
        % find the index in the xlsx sheet
        sub_id = find(contains(Subject_ID,sub_temp));
        
        % find the label
        if ( strcmp(Subject_TYPE{sub_id},'Proband') )
            Subject_type(s) = 1;
        elseif ( strcmp(Subject_TYPE{sub_id},'Family') )
            Subject_type(s) = 2;
        elseif ( strcmp(Subject_TYPE{sub_id},'NC') ) 
            Subject_type(s) = 3;
        end
    end
    
    % get the subject subtype (label)
    % code: NaN = empty, 1 = SZ, 2 = BP, 3 = SAD
    if ( Subject_exist(s) == 0 )
        Subject_subtype(s) = NaN;
        Subject_pid(s)     = NaN;
    else
        
        % get the PID
        Subject_pid(s) = Subject_PID(sub_id);
        
        if ( strcmp(Subject_SUBTYPE{sub_id},'SZP') )
            Subject_subtype(s) = 1;
        elseif ( strcmp(Subject_SUBTYPE{sub_id},'SZR') )
            Subject_subtype(s) = 4;
        elseif ( strcmp(Subject_SUBTYPE{sub_id},'BPP') )
            Subject_subtype(s) = 2;
        elseif ( strcmp(Subject_SUBTYPE{sub_id},'BPR') )
            Subject_subtype(s) = 5;
        elseif ( strcmp(Subject_SUBTYPE{sub_id},'SADP') )
            Subject_subtype(s) = 3;
        elseif ( strcmp(Subject_SUBTYPE{sub_id},'SADR') )
            Subject_subtype(s) = 6;
        elseif ( strcmp(Subject_SUBTYPE{sub_id},'NC') )
            Subject_subtype(s) = 7;
        end
    end
    
    
    % get the subject biotype (label)
    % code: 999 = empty, 1P = biotype 1 (patient), 2P = biotype 2 (patient),
    % 3P = biotype 3 (patient), 1R = biotype 1 (relative), 2R = biotype 2
    % (relative), 3R = biotype 3 (relative)
    
    if ( Subject_exist(s) == 0 )
        Subject_biotype(s) = NaN;
        Subject_pid(s)     = NaN;
    else
        
        % get the PID
        Subject_pid(s) = Subject_PID(sub_id);
        
        if ( strcmp(Subject_BIOTYPE{sub_id},'1P') )
            Subject_biotype(s) = 1;
        elseif ( strcmp(Subject_BIOTYPE{sub_id},'1R') )
            Subject_biotype(s) = 4;
        elseif ( strcmp(Subject_BIOTYPE{sub_id},'2P') )
            Subject_biotype(s) = 2;
        elseif ( strcmp(Subject_BIOTYPE{sub_id},'2R') )
            Subject_biotype(s) = 5;
        elseif ( strcmp(Subject_BIOTYPE{sub_id},'3P') )
            Subject_biotype(s) = 3;
        elseif ( strcmp(Subject_BIOTYPE{sub_id},'3R') )
            Subject_biotype(s) = 6;
        elseif ( strcmp(Subject_SUBTYPE{sub_id},'NC') )
            Subject_biotype(s) = 7;
        elseif ( strcmp(Subject_SUBTYPE{sub_id},'999') )
            Subject_biotype(s) = NaN;
        end
    end
    
    
    % get the subject confounds (age, gender, site)
    if ( Subject_exist(s) == 0 )
        Subject_age(s)      = NaN;
        Subject_gender(s)   = NaN;
        Subject_site(s) 	= NaN;
    else
        Subject_age(s)      = Subject_AGE(sub_id);
        Subject_gender(s)   = Subject_GENDER(sub_id);
        Subject_site(s) 	= Subject_SITE(sub_id);
    end
end
    

% asign the subject PID
Subject_name = Subject_List_all;


% create the directory
if ( ~exist(fullfile(foldername,'Information'),'dir') )
    mkdir(fullfile(foldername,'Information'))
end
    
% store the results
if ( ~isempty(Subject_exist) && ~isempty(Subject_type) && ~isempty(Subject_subtype) && ~isempty(Subject_name) )
    save(fullfile(foldername,'Information','Subject_Information.mat'),'Subject_exist','Subject_type','Subject_subtype','Subject_biotype','Subject_name')
end

% store the results
if ( ~isempty(Subject_PID) )
    save(fullfile(foldername,'Information','Subject_PID.mat'),'Subject_pid')
end

% store the results
if ( ~isempty(Subject_AGE) && ~isempty(Subject_GENDER) && ~isempty(Subject_SITE) )
    save(fullfile(foldername,'Information','Subject_Confounds.mat'),'Subject_age','Subject_gender','Subject_site')
end

end
