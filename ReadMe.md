# README

This README contains the minimal information on the analysis pipeline for the paper entitled "Regression dynamic causal modeling for resting-state data" published in Human Brain Mapping. The repository provides all code utilized for data analysis.

# Contributors / Roles
|                               |                                             |
| ----------------------------- | ------------------------------------------- |
| Project lead / analysis:      | Stefan Frässle (PhD)                        |
| Code review:                  | Samuel Harrison (PhD)                       |
| Supervising Prof.:            | Prof. Klaas Enno Stephan (MD Dr. med., PhD) |
| Abbreviation:                 | B\_SNIP1                                    |
| Date:                         | June 13, 2020                               |
| License:                      | GNU General Public License                  |

This software is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details: http://www.gnu.org/licenses/

# Summary
This project demonstrates the utility of regression dynamic causal modeling (rDCM) for inferring effective (directed) connectivity estimates from non-invasive functional
magnetic resonance imaging (fMRI) data acquired during the resting state. So far, rDCM has only been validated for task-based fMRI; here, we demonstrate that it can
also be applied to resting-state fMRI. To this end, we first conduct simulations to demonstrate the face validity of the approach. We then apply rDCM to empirical
resting-state fMRI data from the healthy controls of the B-SNIP1 consortium. First, we infer connectivity in small networks and compare results to spectral DCM. Having demonstrated the
construct validity, we also demonstrate that the model scales to whole-brain networks.

The project was conducted at the Translational Neuromodeling Unit (TNU) by the project lead (SF), in collaboration with colleagues from the B-SNIP consortium.

**Note:** The code in this repository will not run out-of-the-box because no data are provided along with the scripts. This is because B-SNIP data cannot be shared openly on a repository but have to be obtained/requested by each user separately.

# Reference
Frässle S, Harrison SJ, Heinzle J, Clementz BA, Tamminga CA, Sweeney JA, Gershon ES, Keshavan MS, Pearlson GD, Powers A, Stephan KE. Regression dynamic causal modeling for resting-state fMRI. *Human Brain Mapping* 42:2159-2180.

# Requirements
Requirements of the project are MATLAB, regression dynamic causal modeling (rDCM) toolbox, Statistical Parametric Mapping (SPM12), several other MATLAB toolboxes

# Steps for running the analysis
1. Please note again that the fMRI data are **not** provided alongside this repository and, hence, the code will not run out-of-the-box. This is because we are not allowed to share the data openly on a repository. The steps listed below should therefore be seen as a recipe for running the analysis.
2. Download SPM (version 7487) from [here](https://www.fil.ion.ucl.ac.uk/spm/software/). Furthermore, you need to download some other relevant MATLAB toolboxes (which are all listed within the code itself). Put all toolboxes in your preferred location.
3. Download TAPAS, containing the rDCM toolbox, from [here](https://translationalneuromodeling.github.io/tapas/) and put the folder in your preferred location.
4. Download the whole-brain parcellation scheme (i.e., the Human Brainnetome Atlas) from [here](https://atlas.brainnetome.org/download.html) and put the file in your preferred location.
5. Make sure that no other MATLAB, SPM, or TAPAS version is in your path. Everything should be in its vanilla state so that the code can add the respective toolboxes.
6. Get the repository containing the analysis code for the project. For this, clone the GIT repository https://gitlab.ethz.ch/tnu/code/fraessleetal_rdcm_resting_state and put the folder in your preferred location.
7. Change all paths in the `ConfigFile.mat` to the respective paths that you are using. NOTE that `FilenameInfo.LongTermStorage_Path` contains the path to the original data. While the data for this project are not provided alongside with the code, this would be the path that you need to adapt when using the code for your own projects.
8. NOTE: `FilenameInfo.LongTermStorage_Path` contains the path to the original data (e.g., fMRI scans, GLM regressors, etc.) whereas `FilenameInfo.Data_Path` contains the path where all results should be stored.
9. Run the bash script using the code `./launcher_rDCM_raw2final.sh`. This is the master script that runs all the jobs in the correct sequence, going all the way from fMRI data to final results (e.g., figures, log files, etc.).
10. Once all jobs are finished, you should check whether all DCMs have been estimated correctly. This is important since some jobs may unpredictably crash; hence, not all DCMs might have been estimated. To check whether everything is fine, inspect the `log_*_collect_*` files in `xxx/B_SNIP1/logs`. At the end of these log files, it will list all the subjects that need to be re-run for whatever reason. If no subjects are listed here, everything should be fine.
11. If no subjects need to be re-run, you can inspect the results. Figures will be stored in `xxx/B_SNIP1/figures` and additional summary information (e.g., hierarchy strengths, run-times) will be stored in `xxx/B_SNIP1/logs`.
12. NOTE that no exact replicates of the figures from the paper are produced. However, all individual components are plotted and figures need to be compared like this.
13. Furthermore, manuscript figures containing the connectogram (Circos) and edge-weighted spring embedding (Cytoscape) cannot be plotted easily using scripts, but need manual interventions. Hence, these are not included in the pipeline. However, figures are based on the endogenous connectivity matrix, which is stored in the mat-file `SummaryStatistics_model1_HC.mat`.
