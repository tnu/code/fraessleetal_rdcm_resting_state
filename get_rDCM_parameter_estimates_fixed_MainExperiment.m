function get_rDCM_parameter_estimates_fixed_MainExperiment(model,parcellation_type,scale)
% Get the individual parameter estimates from the whole-brain Dynamic Causal 
% Models (DCMs) for the resting state dataset of the B_SNIP1 study.
% Parameter estimates have been computed using regression DCM (rDCM).
% 
% This function reads the DCM files that have been estimated using
% regression DCM. The function stores the individual parameter estimates
% for the sparse effective connectivity patterns.
% 
% Input:
%   model               -- model to analyze
%   parcellation_type   -- parcellation scheme: (1) Glasser 2016, (2) AAL, (3) Brainetome 2016, (4) Glasser 2016 & Buckner 2011
%   scale               -- scaling of the data: (0) no scale, (1) scale
%   
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2016
% Copyright 2018 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% define the different names
scale_name      = {'_noScale',''};


% get the site list
Site_List = {'Baltimore','Chicago','Dallas','Hartford'};

% set the parcellation
parcellations     = {'Glasser2016','AAL','Brainnetome2016','Glasser2016_Buckner2011'};


% define the filename
filename = ['DCM_RestingState_model' num2str(model) '_rDCM.mat'];


% get the excluded subjects
exclude_subjects  = textread(fullfile(FilenameInfo.Data_Path,'exclusions.txt'),'%s');
exclude_subjects2 = textread(fullfile(FilenameInfo.Data_Path,'exclusions_rDCM.txt'),'%s');
exclude_subjects  = [exclude_subjects; exclude_subjects2];


% foldername for the center coordinates
VoI_foldername = fullfile(FilenameInfo.Data_Path,'VoI_coordinates',['timeseries_VOI_' parcellations{parcellation_type}]);

% get complete subject list
temp             = load(fullfile(VoI_foldername,'VoI_CenterCoordinates_AllSubjects.mat'));
Subject_List_all = temp.Subject_List_all;


% iterate over all subjects
vector = ones(length(Subject_List_all),1);
for int = 1:length(Subject_List_all)
    for int2 = 1:length(exclude_subjects)
        if ( strcmp(Subject_List_all{int},exclude_subjects{int2}) )
            vector(int) = 0;
        end
    end
end

% iterate over all subjects
for int = 1:length(Subject_List_all)
    for int2 = 1:length(exclude_subjects2)
        if ( strcmp(Subject_List_all{int},exclude_subjects2{int2}) )
            vector(int) = 0;
        end
    end
end

% binarize vector
vector = vector == 1;


for site = 1:length(Site_List)
    
    % define the different names
    foldername = fullfile(FilenameInfo.Data_Path, Site_List{site}, 'derivatives', 'fmriprep');
    
    % get the subject list
    Subject_List = dir(fullfile(foldername,'sub*html'));
    
    % iterate over all subjects
    for subject = 1:length(Subject_List)

        % subject name
        Subject = Subject_List(subject).name(1:end-5);

        % set the directory
        foldername_rDCM = fullfile(foldername, Subject, 'firstlevel_dcm', parcellations{parcellation_type}, ['regressionDCM' scale_name{scale+1} '_connectome']);
        
        % get the data
        if ( ~any(strcmp(Subject,exclude_subjects)) && exist(fullfile(foldername_rDCM,filename),'file') )

            try
            
                % load the DCM
                temp           = load(fullfile(foldername_rDCM,filename));
                output         = temp.output;
                
                % display subject name
                disp(['Subject: ' Subject ' (' Site_List{site} ' - ' num2str(subject) ') - found'])
                
            catch
                
                % load a dummy result to get network size
                foldername_dummy = fullfile(FilenameInfo.Data_Path, 'Baltimore', 'derivatives', 'fmriprep', 'sub-S0015SRH1', 'firstlevel_dcm', parcellations{parcellation_type}, ['regressionDCM' scale_name{scale+1} '_connectome']);
                file_dummy = dir(fullfile(foldername_dummy, filename));
                temp = load(fullfile(foldername_dummy,file_dummy(1).name));

                % define dummy connectivity and driving input matrices
                output.Ep.A = NaN(size(temp.output.Ep.A,1),size(temp.output.Ep.A,2));
                output.Ep.C = NaN(size(temp.output.Ep.C,1),size(temp.output.Ep.C,2));

                % define dummy time series
                output.signal.y_source    = NaN(size(temp.output.signal.y_source,1),size(temp.output.signal.y_source,2));
                output.signal.y_pred_rdcm = NaN(size(temp.output.signal.y_pred_rdcm,1),size(temp.output.signal.y_pred_rdcm,2));

                % define a dummy array
                output.logF = NaN;
                
                % define a dummy time
                output.time_rDCM = NaN;
                
                % display subject name
                disp(['Subject: ' Subject ' (' Site_List{site} ' - ' num2str(subject) ') - missing (wrong)'])
                
            end

            % clear the DCM file
            clear temp

        else

            % display subject name
            disp(['Subject: ' Subject ' (' Site_List{site} ' - ' num2str(subject) ') - missing'])

            % load a dummy result to get network size
            foldername_dummy = fullfile(FilenameInfo.Data_Path, 'Baltimore', 'derivatives', 'fmriprep', 'sub-S0015SRH1', 'firstlevel_dcm', parcellations{parcellation_type}, ['regressionDCM' scale_name{scale+1} '_connectome']);
            file_dummy = dir(fullfile(foldername_dummy, filename));
            temp = load(fullfile(foldername_dummy,file_dummy(1).name));

            % define dummy connectivity and driving input matrices
            output.Ep.A = NaN(size(temp.output.Ep.A,1),size(temp.output.Ep.A,2));
            output.Ep.C = NaN(size(temp.output.Ep.C,1),size(temp.output.Ep.C,2));

            % define dummy time series
            output.signal.y_source    = NaN(size(temp.output.signal.y_source,1),size(temp.output.signal.y_source,2));
            output.signal.y_pred_rdcm = NaN(size(temp.output.signal.y_pred_rdcm,1),size(temp.output.signal.y_pred_rdcm,2));

            % define a dummy array
            output.logF = NaN;
            
            % define a dummy time
            output.time_rDCM = NaN;

            % clear the DCM file
            clear temp

        end

        % define cells
        if ( site == 1 && subject == 1 )
            A_Matrix_AllSubjects    = cell(size(output.Ep.A,1),size(output.Ep.A,2));
            C_Matrix_AllSubjects    = cell(size(output.Ep.C,1),size(output.Ep.C,2));

            F_AllSubjects         	= cell(1,1);
            
            Time_rDCM_AllSubjects 	= cell(1,1);

            y_source_AllSubjects    = cell(length(Subject_List),1);
            y_pred_rdcm_AllSubjects	= cell(length(Subject_List),1);
        end

        % asign the a values for the endogenous parameters in each subject
        for int = 1:size(output.Ep.A,1)
            for int2 = 1:size(output.Ep.A,2)
                A_Matrix_AllSubjects{int,int2} = [A_Matrix_AllSubjects{int,int2}; output.Ep.A(int,int2)];
            end 
        end

        % asign the a values for the driving parameters in each subject
        for int = 1:size(output.Ep.C,1)
            for int2 = 1:size(output.Ep.C,2)
                C_Matrix_AllSubjects{int,int2} = [C_Matrix_AllSubjects{int,int2}; output.Ep.C(int,int2)];
            end 
        end


        % asign the measured and predicted time series
        y_source_AllSubjects{subject,1}    = output.signal.y_source';
        y_pred_rdcm_AllSubjects{subject,1} = output.signal.y_pred_rdcm';


        % asign the negative free energy
        F_AllSubjects{1,1}           = [F_AllSubjects{1,1}; output.logF];
        
        
        % asign the run-time
        Time_rDCM_AllSubjects{1,1} = [Time_rDCM_AllSubjects{1,1}; output.time_rDCM];
        

        % asign the subject name
        if ( site == 1 && subject == 1 )
            results.AllSubjects{subject}        = Subject;
            results_signal.AllSubjects{subject} = Subject;
        else
            results.AllSubjects{end+1}          = Subject;
            results_signal.AllSubjects{end+1}   = Subject;
        end
        

        % asign the region names
        if ( site == 1 && subject == 1 )
            results.AllRegions        = output.signal.name;
            results_signal.AllRegions = output.signal.name;
        end
    end
end
    
% asign the results
results.A_Matrix_AllSubjects               = A_Matrix_AllSubjects;
results.C_Matrix_AllSubjects               = C_Matrix_AllSubjects;
results.F_AllSubjects                      = F_AllSubjects;
results.Time_rDCM_AllSubjects              = Time_rDCM_AllSubjects;

results_signal.y_source_AllSubjects        = y_source_AllSubjects;
results_signal.y_pred_rdcm_AllSubjects     = y_pred_rdcm_AllSubjects;
results_signal.F_AllSubjects               = F_AllSubjects;
results_signal.Time_rDCM_AllSubjects       = Time_rDCM_AllSubjects;


% display whether subject have to be re-run
A_Matrix_IncludedSubjects = A_Matrix_AllSubjects{int,int2}(vector);
disp(['Number of missing subjects: ' num2str(sum(isnan(A_Matrix_IncludedSubjects)))])

if ( sum(isnan(A_Matrix_IncludedSubjects)) > 0 )

    % find which subjects are missing
    temp = [];
    for int = 1:length(vector)
        if (vector(int)==1 && ~isfinite(results.A_Matrix_AllSubjects{1,1}(int)))
            temp = [temp, int];
        end
    end

    % output subject numbers that are missing
    fprintf(['Number of missing subjects: ' repmat('%d ',1,length(temp)) '\n'],temp)
    
end

% create the results folder
if ( ~exist(fullfile(FilenameInfo.Data_Path,'grouplevel_dcm',parcellations{parcellation_type},['regressionDCM' scale_name{scale+1} '_connectome']),'dir') )
    mkdir(fullfile(FilenameInfo.Data_Path,'grouplevel_dcm',parcellations{parcellation_type},['regressionDCM' scale_name{scale+1} '_connectome']))
end

% save the estimated result
if ( ~isempty(results) )
    save(fullfile(FilenameInfo.Data_Path,'grouplevel_dcm',parcellations{parcellation_type},['regressionDCM' scale_name{scale+1} '_connectome'], ['PosteriorParameterEstimates_model' num2str(model) '.mat']), 'results', '-v7.3')
end

% save the estimated result
if ( ~isempty(results_signal) )
    save(fullfile(FilenameInfo.Data_Path,'grouplevel_dcm',parcellations{parcellation_type},['regressionDCM' scale_name{scale+1} '_connectome'], ['MeasuredPredictedSignal_model' num2str(model) '.mat']), 'results_signal', '-v7.3')
end

end