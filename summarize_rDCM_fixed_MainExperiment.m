function summarize_rDCM_fixed_MainExperiment(parcellation_type,model,scale,subgroup,computeGT)
% Evaluates the mean effective connectivity parameters from the whole-brain 
% Dynamic Causal Models (DCMs) for the resting state dataset of the B_SNIP1 
% study. Additionally, it computes graph-theoretical measures from these
% effective connectivity patterns.
%
% Input:
%   parcellation_type   -- parcellation scheme: (1) Glasser 2016, (2) AAL, (3) Brainetome 2016, (4) Glasser 2016 & Buckner 2011
%   model               -- model to be analyzed
%   scale               -- scaling of the data: (0) no scale, (1) scale
%   subgroup            -- (0) all participants, (1) restrict to healthy controls
%   computeGT           -- compute graph theoretical measures
% 
% Output:
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2020
% Copyright 2020 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% close all figures
close all

% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% add path
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'PBAR')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'fdr_bh')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'cbrewer')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'append_pdfs')))


% define the different names
scale_name      = {'_noScale',''};

% set the parcellation and subgroup name
parcellations  	= {'Glasser2016','AAL','Brainnetome2016','Glasser2016_Buckner2011'};
subgroup_name   = {'','_HC'};


% set the SPM path and the path to the experiment
foldername = fullfile(FilenameInfo.Data_Path,'grouplevel_dcm',parcellations{parcellation_type},['regressionDCM' scale_name{scale+1} '_connectome']);


% load the results array
temp    = load(fullfile(foldername,['PosteriorParameterEstimates_model' num2str(model) '.mat']));
results = temp.results;


% generate subject infromation if necessary
if ( ~exist(fullfile(FilenameInfo.Data_Path,'Information','Subject_Information.mat'),'file') )
    get_subject_information_MainExperiment()
end


% load the subject information
temp            = load(fullfile(FilenameInfo.Data_Path,'Information','Subject_Information.mat'));
Subject_type    = temp.Subject_type;


% load the subject information regarding confounds
temp2           = load(fullfile(FilenameInfo.Data_Path,'Information','Subject_Confounds.mat'));
Subject_age     = temp2.Subject_age;
Subject_gender  = temp2.Subject_gender;
Subject_site    = temp2.Subject_site;

% number of different sites
NR_sites        = max(Subject_site)-1;

% create a confound matrix
confounds = [Subject_age, Subject_gender];
for int = 1:NR_sites
    if ( sum(Subject_site==int) ~= 0 )
        confounds = [confounds, Subject_site==int];
    end
end

% different tests
Subject_type_test = Subject_type;


% find the subjects that are not included in the analysis
Subject_type_test((~isfinite(results.A_Matrix_AllSubjects{1,1}))==1) = NaN;


% restrict to healthy controls only
if ( subgroup == 1 )
    Subject_type_test(Subject_type_test~=3) = NaN;
end


% asign the data
A_matrix_allSubjects     = results.A_Matrix_AllSubjects;


% check whether you have the right subjects
if ( length(temp.Subject_name) ~= length(A_matrix_allSubjects{1,1}) )
    for int = 1:length(results.AllSubjects)
        vector(int) = any(strcmp(results.AllSubjects(int),temp.Subject_name));
    end
    
    for int = 1:size(A_matrix_allSubjects,1)
        for int2 = 1:size(A_matrix_allSubjects,2)
            A_matrix_allSubjects{int,int2} = A_matrix_allSubjects{int,int2}(vector==1);
        end
    end
end

% excluded subjects
excluded_subjects = ~isfinite(A_matrix_allSubjects{1,1}) | ~isfinite(Subject_type_test) | ~isfinite(Subject_age) | ~isfinite(Subject_gender) | ~isfinite(Subject_site);

% initialize the temporary arrays
mean_Amatrix_allSubjects = NaN(size(A_matrix_allSubjects));
pVal_Amatrix_allSubjects = NaN(size(A_matrix_allSubjects));


% display the number of subjects
disp(['Found: ' num2str(sum(excluded_subjects==0)) ' out of ' num2str(length(Subject_type_test)) ' subjects'])


% get endogenous parameters
for int = 1:size(A_matrix_allSubjects,1)
    for int2 = 1:size(A_matrix_allSubjects,2)
        
        % get the mean endogenous connection strength
        mean_Amatrix_allSubjects(int,int2) = mean(A_matrix_allSubjects{int,int2}(excluded_subjects==0));

        % asign the p value
        [~,p] = ttest(A_matrix_allSubjects{int,int2}(excluded_subjects==0),0);
        pVal_Amatrix_allSubjects(int,int2) = p;
    end
end


% figure folder
figure_folder = fullfile(FilenameInfo.Data_Path,'Figures',parcellations{parcellation_type},'regressionDCM');

% create the directory for the figures
if ( ~exist(figure_folder,'dir') ) 
    mkdir(figure_folder)
else
    if ( exist(fullfile(figure_folder,['Model' num2str(model) subgroup_name{subgroup+1} '.pdf']),'file') )
        delete(fullfile(figure_folder,['Model' num2str(model) subgroup_name{subgroup+1} '.pdf']))
    end
end


% significance threshold (FDR-correction)
p_all               = pVal_Amatrix_allSubjects(:);
p_all               = p_all(isfinite(p_all));
[~, crit_p, ~, ~]   = fdr_bh(p_all,0.05,'dep','no');
threshold           = crit_p+eps;


% define nice colormpa
[cbrewer_colormap] = cbrewer('div', 'RdBu', 71, 'PCHIP');
cbrewer_colormap   = flipud(cbrewer_colormap);
cbrewer_colormap   = cbrewer_colormap([1:33 36 39:71],:);


% plot average endogenous connectivity
figure('units','normalized','outerposition',[0 0 1 1])
imagesc(mean_Amatrix_allSubjects)
axis square
colormap(cbrewer_colormap)
colorbar
caxis([-0.008 0.008])
set(gca,'xtick',[1 size(mean_Amatrix_allSubjects,2)/2 size(mean_Amatrix_allSubjects,2)])
set(gca,'ytick',[1 size(mean_Amatrix_allSubjects,1)/2 size(mean_Amatrix_allSubjects,1)])
title('Endogenous connectivity [Hz]','FontSize',18)
print(gcf, '-dpdf', fullfile(figure_folder,['Model' num2str(model) '_temp1']), '-fillpage')


% plot average endogenous connectivity (significant)
figure('units','normalized','outerposition',[0 0 1 1])
imagesc(mean_Amatrix_allSubjects .* (pVal_Amatrix_allSubjects < threshold))
axis square
colormap(cbrewer_colormap)
colorbar
caxis([-0.008 0.008])
set(gca,'xtick',[1 size(mean_Amatrix_allSubjects,2)/2 size(mean_Amatrix_allSubjects,2)])
set(gca,'ytick',[1 size(mean_Amatrix_allSubjects,1)/2 size(mean_Amatrix_allSubjects,1)])
title('Endogenous connectivity (sig.) [Hz]','FontSize',18)
print(gcf, '-dpdf', fullfile(figure_folder,['Model' num2str(model) '_temp2']), '-fillpage')


% get all non-zero and non-diagonal absolut values
temp = mean_Amatrix_allSubjects - diag(diag(mean_Amatrix_allSubjects));
mean_Amatrix_allSubjects_abs = abs(temp(temp~=0));

% plot the distribution of edge weights
h = figure('units','normalized','outerposition',[0 0 1 1]);
subplot(1,2,1)
histogram(mean_Amatrix_allSubjects_abs,'BinWidth',0.0008,'FaceColor',[0.1 0.1 0.1],'Normalization','pdf')
axis square
box off
set(gca,'xtick',[0 0.01 0.02])
xlim([0 0.02])
xlabel('endogenous connectivity [Hz]')
ylabel('probability density function (PDF)')
title('Weight distribution','FontSize',18)

subplot(1,2,2)
histogram(log(mean_Amatrix_allSubjects_abs),'BinWidth',0.5,'FaceColor',[0.1 0.1 0.1],'FaceAlpha',0.1,'EdgeColor',[0.8 0.8 0.8],'Normalization','pdf');
pd = fitdist(log(mean_Amatrix_allSubjects_abs),'Normal'); x = -20:0.01:0; y = pdf(pd,x);
hold on,
ha = area(x,y,'EdgeColor','none','FaceColor',[0.5 0.1 0.1],'FaceAlpha',0.4);
axis square
box off
xlim([-20 0])
xlabel('log endogenous connectivity [Hz]')
ylabel('probability density function (PDF)')
legend(ha,'log-normal fit','Location','NW')
legend boxoff
title('log Weight distribution','FontSize',18)
set(h,'PaperOrientation','landscape');
print(gcf, '-dpdf', fullfile(figure_folder,['Model' num2str(model) '_temp3']), '-fillpage')


% sample from priors of the model to demonstrate that this implicit by the model
prior_samples     = mvnrnd(0,1/(64*size(mean_Amatrix_allSubjects_abs,1)),sum(mean_Amatrix_allSubjects_abs(:)~=0));
prior_samples_abs = abs(prior_samples);

% plot the distribution of edge weights
h = figure('units','normalized','outerposition',[0 0 1 1]);
subplot(1,2,1)
histogram(prior_samples_abs,'BinWidth',0.0008,'FaceColor',[0.1 0.1 0.1],'Normalization','pdf')
axis square
box off
set(gca,'xtick',[0 0.01 0.02])
xlim([0 0.02])
xlabel('sampled endogenous connectivity [Hz]')
ylabel('probability density function (PDF)')
title('Sampled weight distribution','FontSize',18)

subplot(1,2,2)
histogram(log(prior_samples_abs),'BinWidth',0.5,'FaceColor',[0.1 0.1 0.1],'FaceAlpha',0.1,'EdgeColor',[0.8 0.8 0.8],'Normalization','pdf');
pd = fitdist(log(prior_samples_abs),'Normal'); x = -20:0.01:0; y2 = pdf(pd,x);
hold on,
ha = area(x,y2,'EdgeColor','none','FaceColor',[0.5 0.1 0.1],'FaceAlpha',0.4);
plot(x,y,'--','Color',[0.5 0.1 0.1 0.6]);
axis square
box off
xlim([-20 0])
xlabel('log sampled endogenous connectivity [Hz]')
ylabel('probability density function (PDF)')
legend(ha,'log-normal fit','Location','NW')
legend boxoff
title('log sampled weight distribution','FontSize',18)
set(h,'PaperOrientation','landscape');
print(gcf, '-dpdf', fullfile(figure_folder,['Model' num2str(model) '_temp4']), '-fillpage')


% append all PDFs
append_pdfs(fullfile(figure_folder,['Model' num2str(model) subgroup_name{subgroup+1} '.pdf']),...
    fullfile(figure_folder,['Model' num2str(model) '_temp1.pdf']),...
    fullfile(figure_folder,['Model' num2str(model) '_temp2.pdf']),...
    fullfile(figure_folder,['Model' num2str(model) '_temp3.pdf']),...
    fullfile(figure_folder,['Model' num2str(model) '_temp4.pdf']))


% delete the temporary files
delete(fullfile(figure_folder,['Model' num2str(model) '_temp*']))


% clear the previous results
results_names = results; clear results


% store the results
results.mean_Amatrix_allSubjects = mean_Amatrix_allSubjects;
results.pVal_Amatrix_allSubjects = pVal_Amatrix_allSubjects;

% store the results
if ( ~isempty(results) )
    save(fullfile(foldername,['SummaryStatistics_model' num2str(model) subgroup_name{subgroup+1} '.mat']),'results')
end



% make directory for Circos
if ( ~exist(fullfile(figure_folder,'Circos'),'dir') )
    mkdir(fullfile(figure_folder,'Circos'))
end

% set the filename for the link files
filename_CIRCOS = fullfile(figure_folder,'Circos',['Model' num2str(model) subgroup_name{subgroup+1} '.txt']);

% option for circos file
circos_opt.scaling   = 16;
circos_opt.normalize = 'relative';
circos_opt.col       = cbrewer_colormap*255;
circos_opt.col_type  = 'relative_sep';
circos_opt.col_scale = 0.005;

% only show the significant connections
mean_Amatrix_allSubjects_Circos = mean_Amatrix_allSubjects .* (pVal_Amatrix_allSubjects < threshold);
mean_Amatrix_allSubjects_Circos = mean_Amatrix_allSubjects_Circos - diag(diag(mean_Amatrix_allSubjects_Circos));

% create the Circos link file
generate_links_circos_MainExperiment(mean_Amatrix_allSubjects_Circos,parcellation_type,filename_CIRCOS,circos_opt)


% set the filename for the Circos link files (Top 1000 connections)
filename_CIRCOS = fullfile(figure_folder,'Circos',['Model' num2str(model) subgroup_name{subgroup+1} '_Top2000.txt']);

% get the Top-1000 connections
mean_Amatrix_allSubjects_Circos = mean_Amatrix_allSubjects - diag(diag(mean_Amatrix_allSubjects));
mean_Amatrix_allSubjects_Circos = compute_top_connections_MainExperiment(mean_Amatrix_allSubjects_Circos,[],2000);

% create the Circos link file
generate_links_circos_MainExperiment(mean_Amatrix_allSubjects_Circos,parcellation_type,filename_CIRCOS,circos_opt)


% make directory for Circos
if ( ~exist(fullfile(figure_folder,'Cytoscape'),'dir') )
    mkdir(fullfile(figure_folder,'Cytoscape'))
end

% set the filename for the link files
filename_CYTOSCAPE = fullfile(figure_folder,'Cytoscape',['Model' num2str(model) subgroup_name{subgroup+1} '.txt']);


% full matrix for Cytoscape
mean_Amatrix_allSubjects_Cytoscape = mean_Amatrix_allSubjects - diag(diag(mean_Amatrix_allSubjects));
mean_Amatrix_allSubjects_Cytoscape = compute_top_connections_MainExperiment(mean_Amatrix_allSubjects_Cytoscape,[],2000);
mean_Amatrix_allSubjects_Cytoscape = abs(mean_Amatrix_allSubjects_Cytoscape);

% create the Cytoscape file
create_Cytoscape_files_MainExperiment(mean_Amatrix_allSubjects_Cytoscape,parcellation_type,filename_CYTOSCAPE)


% graph theory
if ( computeGT )

    % display the progress
    fprintf('\nComputing graph theoretical measures...\n')

    % clear results array
    clear results
    
    % compute graph theoretical measures
    for s = 1:length(A_matrix_allSubjects{1,1})

        % get graph theoretical results or write empty array
        if ( excluded_subjects(s) == 0 )

            % display with subject is processed
            disp(['Processing: ' results_names.AllSubjects{s}])

            % empty single subject matrix
            A_matrix_Subject = NaN(size(A_matrix_allSubjects));

            % create matrix from subject
            for int = 1:size(A_matrix_allSubjects,1)
                for int2 = 1:size(A_matrix_allSubjects,2)
                    A_matrix_Subject(int,int2) = A_matrix_allSubjects{int,int2}(s);
                end
            end

            % remove diagonal
            A_matrix_Subject = A_matrix_Subject - diag(diag(A_matrix_Subject));

            % transpose A-matrix (as Brain Connectivity toolbox uses switched notation)
            A_matrix_Subject = A_matrix_Subject';
            
            % compute graph theoretical measures
            if ( any(A_matrix_Subject(:)~=0) )
                results_temp = compute_graph_theoretical_measures(A_matrix_Subject);
                results(s)   = results_temp;
            end

        else
            
            % display with subject is processed
            disp(['Processing: ' results_names.AllSubjects{s} ' (excluded)'])
            
            % compute graph theoretical measures
            results_temp = compute_graph_theoretical_measures([]);
            results(s)   = results_temp;
            
        end
    end
    
    % store the graph theoretical measures
    save(fullfile(foldername,['GraphTheory_model' num2str(model) '.mat']),'results')
    
end

end