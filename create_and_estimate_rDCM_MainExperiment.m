function create_and_estimate_rDCM_MainExperiment(mode,subject,site,model)
% Creates a DCM structure from scratch by asigning the extracted BOLD signal 
% time series for the resting-state dataset of the B-SNIP1. The function 
% specifies the driving inputs and the model structure (i.e., A and C matrices). 
% To model the resting state, the spectral implementation is used. The function 
% then estimates the respective DCM using the regression dynamic causal 
% modeling (rDCM) toolbox. 
% 
% Input:
%   mode                -- (1) saccade network, (2) modes (RSN), (3) nodes (RSN)
%   site                -- location: (1) Baltimore, (2) Boston, (3) Chicago, (4) Dallas, (5) Detroit, (6) Hartford
%   subject             -- subject to analyze
%   model               -- model to analyze
%   
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2020
% Copyright 2020 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end

% add the rDCM toolbox
addpath(genpath(fullfile(FilenameInfo.TAPAS_Path,'rDCM')))
addpath(FilenameInfo.SPM_Path)

% initialize spm
spm_jobman('initcfg')


% get the site list
Site_List = {'Baltimore','Chicago','Dallas','Hartford'};

% get the mode list
mode_List      = {'timeseries_VOI_saccade','timeseries_VOI_RSN_modes','timeseries_VOI_RSN_nodes'};
mode_save_List = {'saccade','RSN_modes','RSN_nodes'};


% specified sites
if ( isempty(site) )
    site_analyze = 1:length(Site_List);
else
    site_analyze = site;
end


% define the filename
filename = ['DCM_RestingState_model' num2str(model) '_rDCM'];

% get the mask information
if ( mode == 1 )
    mask_info{1,1} = 'MFG';
    mask_info{2,1} = 'IPS_left';
    mask_info{3,1} = 'IPS_right';
    mask_info{4,1} = 'FEF_left';
    mask_info{5,1} = 'FEF_right';
else
    mask_info = create_RSN_information_MainExperiment(mode-1);
end


% iterate over sites
for site = site_analyze
    
    % specify the foldername for respective site
    foldername_site = fullfile(FilenameInfo.Data_Path,Site_List{site},'derivatives','fmriprep');
    
    % get a list of subjects for each site
    Subject_List = dir(fullfile(foldername_site,'sub*html'));
    
    % specified subject
    if ( isempty(subject) )
        subject_analyze = 1:length(Subject_List);
    else
        subject_analyze = subject;
    end
    
    % iterate over subjects
    for subject = subject_analyze
        
        % asign subject name
        try 
            Subject = Subject_List(subject).name(1:end-5);
            fprintf(['\nProcessing: ' Subject ' (' Site_List{site} ')\n'])
        catch
            fprintf(['\nNo more subjects (' Site_List{site} ')\n'])
            break
        end
        
        
        % clear any DCM structure
        clear DCM
        clear VOI_files
        
        
        % check whether SPM exists
        if ( ~exist(fullfile(foldername_site,Subject,'firstlevel','SPM.mat'),'file') )
            disp('Loading data...')
            disp('Found number of regions: 0 (skip)')
            continue
        end

        % load SPM
        load(fullfile(foldername_site,Subject,'firstlevel','SPM.mat'))

        % asign the data of the respective subject to the DCM
        VOI_foldername = fullfile(foldername_site,Subject,'firstlevel',mode_List{mode});

        % get the VOI file in the folder
        VOI_files    = dir(fullfile(VOI_foldername, 'VOI_*.mat'));
        
        
        % check whether SPM exists
        if ( length(VOI_files) ~= length(mask_info) )
            disp('Loading data...')
            disp('Found number of regions: 0 (skip)')
            continue
        end
        
        % find given order of regions
        region_order = NaN(1,length(mask_info));
        for int2 = 1:length(mask_info)
            for int = 1:length(VOI_files)
                if( contains(VOI_files(int).name,mask_info{int2}) )
                    region_order(int2) = int;
                end
            end
        end
        
        % reorder regions
        VOI_files = VOI_files(region_order);
        
        % display progress
        disp('Loading data...')
        disp(['Found number of regions: ' num2str(length(VOI_files))])

        % load the VOI time series for all regions of interest
        for number_of_regions = 1:length(VOI_files)
            load(fullfile(VOI_foldername,VOI_files(number_of_regions).name),'xY');
            DCM.xY(number_of_regions) = xY;
        end

        % number of regions
        DCM.n = length(DCM.xY);

        % number of time points
        DCM.v = length(DCM.xY(1).u);

        % specify the TR
        DCM.Y.dt = SPM.xY.RT;

        % specify the Y component of the DCM file
        DCM.Y.X0 = DCM.xY(1).X0;

        % asign the data to the Y structure
        for i = 1:DCM.n
            DCM.Y.y(:,i)  = DCM.xY(i).u;
            DCM.Y.name{i} = DCM.xY(i).name;
        end

        % define the covariance matrix
        DCM.Y.Q = spm_Ce(ones(1,DCM.n)*DCM.v);


        % Experimental inputs (empty input)
        DCM.U.u     = zeros(size(DCM.Y.y,1)*16, 1);
        DCM.U.name  = {'null'};
        DCM.U.dt    = DCM.Y.dt/16;


        % DCM parameters
        DCM.delays = repmat(SPM.xY.RT/2,DCM.n,1);

        % get the echo time
        foldername_mriqc = fullfile(FilenameInfo.Data_Path,Site_List{site},'derivatives','mriqc',Subject,'func');
        fid              = fopen(fullfile(foldername_mriqc,[Subject '_task-rest_bold.json']));
        json_text        = textscan(fid,'%s');
        TE               = str2double(json_text{1}{9});
        
        % asign echo time
        DCM.TE = TE;
        
        % DCM options
        DCM.options.nonlinear  = 0;
        DCM.options.two_state  = 0;
        DCM.options.stochastic = 0;
        DCM.options.centre     = 0;
        DCM.options.induced    = 1;
        DCM.options.nograph    = 0;


        % connectivity structure of respective model:
        if ( mode == 1 )
            if ( model == 1 )
                DCM.a = eye(DCM.n);
                DCM.a(1:5,1:5) = ones(5);
                DCM.a(5,2) = 0;
                DCM.a(4,3) = 0;
                DCM.a(3,4) = 0;
                DCM.a(2,5) = 0;
                DCM.a(6,2) = 1;
                DCM.a(2,6) = 1;
                DCM.a(7,3) = 1;
                DCM.a(3,7) = 1;
                DCM.a(6,1) = 1;
                DCM.a(1,6) = 1;
                DCM.a(7,1) = 1;
                DCM.a(1,7) = 1;
                DCM.b = zeros(DCM.n,DCM.n,size(DCM.U.u,2));
                DCM.c = zeros(DCM.n,size(DCM.U.u,2));
                DCM.d = zeros(DCM.n,DCM.n,0);
            elseif ( model == 2 )
                DCM.a = eye(DCM.n);
                DCM.a(1,2) = 1;
                DCM.a(2,1) = 1;
                DCM.a(1,3) = 1;
                DCM.a(3,1) = 1;
                DCM.a(2,3) = 1;
                DCM.a(3,2) = 1;
                DCM.a(2,4) = 1;
                DCM.a(4,2) = 1;
                DCM.a(3,5) = 1;
                DCM.a(5,3) = 1;
                DCM.a(4,5) = 1;
                DCM.a(5,4) = 1;
                DCM.a(6,2) = 1;
                DCM.a(7,3) = 1;
                DCM.b = zeros(DCM.n,DCM.n,size(DCM.U.u,2));
                DCM.c = zeros(DCM.n,size(DCM.U.u,2));
                DCM.d = zeros(DCM.n,DCM.n,0);
            elseif ( model == 3 )
                DCM.a = ones(DCM.n);
                DCM.b = zeros(DCM.n,DCM.n,size(DCM.U.u,2));
                DCM.c = zeros(DCM.n,size(DCM.U.u,2));
                DCM.d = zeros(DCM.n,DCM.n,0);
            end
        else
            if ( model == 1 )
                DCM.a = ones(DCM.n);
                DCM.b = zeros(DCM.n,DCM.n,size(DCM.U.u,2));
                DCM.c = zeros(DCM.n,size(DCM.U.u,2));
                DCM.d = zeros(DCM.n,DCM.n,0);
            end
        end
        
        
        % detrend data
        DCM.Y.y = spm_detrend(DCM.Y.y);
        
        % scale data
        scale_factor   = max(max((DCM.Y.y))) - min(min((DCM.Y.y)));
        scale_factor   = 4/max(scale_factor,4);
        DCM.Y.y        = DCM.Y.y*scale_factor;
        
        % set the prior type of the endogenous matrix
        DCM.options.wide_priors = 0;
        
        
        % check whether the folder exists
        if ( ~exist(fullfile(FilenameInfo.Data_Path, Site_List{site}, 'derivatives', 'fmriprep', Subject, 'firstlevel_dcm',mode_save_List{mode}),'dir') )
            mkdir(fullfile(FilenameInfo.Data_Path, Site_List{site}, 'derivatives', 'fmriprep', Subject, 'firstlevel_dcm',mode_save_List{mode}))
        end
        
        
        % display the progress
        disp(['Evaluating subject ' num2str(subject) ' - model ' num2str(model)])
        disp(' ')


        % shift the input regressor slightly
        options.u_shift    = 0;
        options.filter_str = 4;

        % empirical analysis
        type = 'r';


        % get time
        currentTimer = tic;
        
        % run the rDCM analysis
        [output, options] = tapas_rdcm_estimate(DCM, type, options, 1);

        % output elapsed time
        time_rDCM = toc(currentTimer);
        disp(['Elapsed time is ' num2str(time_rDCM) ' seconds.'])


        % store the estimation time
        output.time.time_rDCM = time_rDCM;

        
        % save the estimated results
        if ( ~isempty(DCM) )
            save(fullfile(FilenameInfo.Data_Path, Site_List{site}, 'derivatives', 'fmriprep', Subject, 'firstlevel_dcm', mode_save_List{mode}, [filename '.mat']),'output')
        end 
    end
end

end
