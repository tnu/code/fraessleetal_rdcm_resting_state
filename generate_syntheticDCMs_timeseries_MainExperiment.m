function generate_syntheticDCMs_timeseries_MainExperiment(con_structure,nr_models,TR,SNR)
% This function samples "true" parameter values of a DCM for simulations 
% on regression DCM and spectral DCM. Here, endogenous neuronal 
% fluctuations are generated using a AR(1) process and the translated into
% predicted neuronal time courses. The synthetic data should then be used
% to compare regression DCM and spectral DCM.
% 
% Input:
%   con_structure     --  # different connectivity structure
%   nr_models         --  # synthetic subjects to generate (per connectivity structure)
%                         (optional: default equals 20 subjects)
%   TR                -- repetition time
%   SNR               -- signal-to-noise ratio
%  
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2020
% Copyright 2020 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% fix the RNG seed
rng(1111)

% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end

% add SPM to path
addpath(FilenameInfo.SPM_Path)


% set the number of "synthetic" subjects
if ( isempty(nr_models) )
    nr_models = 1:20;
end


% set the number of observations (scans)
if ( isempty(TR) )
    TR = 0.5;
end


% number of regions and inputs
Nr = 4;
Nu = 0;


% "recorded" time session (in seconds)
full_T = 600;

% number of scans
T = full_T/TR;

% observation times
t  = (1:T)*TR;

% endogenous fluctuations
u  = spm_rand_mar(T,Nr,1/2)/4;


% experimental inputs (Cu = 0 to suppress)
Cu  = zeros(Nr,1);
E   = cos(2*pi*TR*(1:T)/24) * 0;


% options
options.nonlinear  = 0;
options.two_state  = 0;
options.stochastic = 0;
options.centre     = 1;
options.induced    = 1;


% network structure
if ( con_structure == 1 )
    DCM.a = ones(Nr,Nr);
elseif ( con_structure == 2 )
    DCM.a = ones(Nr,Nr);
    mask  = [1 1 0 0;
             1 1 1 0;
             1 1 1 1;
             1 1 1 1];
    DCM.a = DCM.a.*mask;
   
elseif ( con_structure == 3 )
    DCM.a = ones(Nr,Nr);
    mask  = [1 1 0 0;
             1 1 1 0;
             0 1 1 1;
             0 0 1 1];
    DCM.a = DCM.a.*mask;
    
elseif ( con_structure == 4 )
    DCM.a = ones(Nr,Nr);
    mask  = [1 0 0 0;
             1 1 0 0;
             0 1 1 0;
             0 0 1 1];
    DCM.a = DCM.a.*mask;
    
elseif (con_structure == 5)
    DCM.a = ones(Nr,Nr);
    mask  = [1 0 0 0;
             0 1 0 0;
             0 0 1 0;
             0 0 0 1];
    DCM.a = DCM.a.*mask;
end

DCM.b   = zeros(Nr,Nr,Nu);
DCM.c   = zeros(Nr,Nu);
DCM.d   = zeros(Nr,Nr,0);

% set options
DCM.options = options;

% priors
[pE,pC]    = spm_dcm_fmri_priors(DCM.a,DCM.b,DCM.c,DCM.d,options);
pE.transit = randn(Nr,1)/16;
pE.C       = eye(Nr,Nr);
DCM.M.pE   = pE;
DCM.M.pC   = pC;

% define the distribution from which endogenous parameters are sampled
var     = diag(DCM.M.pC);
a_std   = sqrt(reshape(var(1:size(DCM.a,1)*size(DCM.a,1)),size(DCM.a,1),size(DCM.a,1)));
a_mean  = DCM.M.pE.A;

% display the progress
disp(['Generating Model ' num2str(con_structure)])

% iterate for the number of desired models
for model = nr_models
    
    disp(['Subject: ' num2str(model)])
    
    % asign the structure
    DCM.Tp = DCM.M.pE;
    
    % sample from the distributions with respective mean and standard deviation
    stable = 0;
    while ~stable
        
        % sample A parameters
        DCM.Tp.A = a_std.*randn(size(a_std,1),size(a_std,2))+a_mean;
        
        % DCM parameters
        DCM.Ep.A = DCM.Tp.A;   
        
        % check whether A-matrix is stable (Lyapunov exponent)
        if ( spm_dcm_check_stability(DCM) )
            stable = 1;
        end
        
        % set modulatory connections to zero
        DCM.Tp.B = zeros(size(DCM.Tp.A,1),size(DCM.Tp.A,2),Nu);
        
        % no small connections
        A_temp = DCM.Tp.A-diag(diag(DCM.Tp.A));
        stable = stable && (~any(abs(A_temp(A_temp~=0))<0.05));
        
        % generate the synthetic data
        if ( stable ) 
            
            % simulate response to endogenous fluctuations integrate states
            M.f  = 'spm_fx_fmri';
            M.x  = sparse(Nr,5);
            U.u  = u + (Cu*E)';
            U.dt = TR;
            x    = spm_int_J(DCM.Tp,M,U);
            
            % haemodynamic observer
            for i = 1:T
                y(i,:) = spm_gx_fmri(spm_unvec(x(i,:),M.x),[],DCM.Tp)';
            end
            
            % get multiplicative factor
            r = diag(std(y)/SNR);
            
            % observation noise process
            e = spm_rand_mar(T,Nr,1/2)*1/4;
             
            % check whether data were generated
            stable = ~any(~isfinite(y(:)));   
        end
    end
    
    % for first "synthetic" subject
    if model == 1
        
        % show simulated response
        i = 1:T;
        spm_figure('Getwin','Figure 1'); clf
        subplot(2,2,1)
        plot(t(i),u(i,:))
        title('Endogenous fluctuations','FontSize',16)
        xlabel('Time (seconds)')
        ylabel('Amplitude')
        axis square

        subplot(2,2,2), hold off
        plot(t(i),x(i,Nr + 1:end),'c'), hold on
        plot(t(i),x(i,1:Nr)), hold off
        title('Hidden states','FontSize',16)
        xlabel('Time (seconds)')
        ylabel('Amplitude')
        axis square

        subplot(2,2,3)
        plot(t(i),y(i,:),t(i),e(i,:),':')
        title('Hemodynamic response and noise','FontSize',16)
        xlabel('Time (seconds)')
        ylabel('Amplitude')
        axis square
  
    end
    
    
    % response
    DCM.Y.y  = y + e*r;
    DCM.Y.dt = TR;
    DCM.U.u  = E';
    DCM.U.dt = TR;
    
    
    % number of regions
    DCM.n = Nr;
    
    
    % generate simulations folder
    if ( ~exist(fullfile(FilenameInfo.Data_Path,'simulations_dcm','small_network','data',['TR_' num2str(TR) '_SNR_' num2str(SNR)]),'dir') )
        mkdir(fullfile(FilenameInfo.Data_Path,'simulations_dcm','small_network','data',['TR_' num2str(TR) '_SNR_' num2str(SNR)]))
    end
    
    % save the DCMs
    if ( model < 10 )
        save(fullfile(FilenameInfo.Data_Path,'simulations_dcm','small_network','data',['TR_' num2str(TR) '_SNR_' num2str(SNR)],['DCM_model' num2str(con_structure) '_0' num2str(model) '.mat']),'DCM')
    else
        save(fullfile(FilenameInfo.Data_Path,'simulations_dcm','small_network','data',['TR_' num2str(TR) '_SNR_' num2str(SNR)],['DCM_model' num2str(con_structure) '_' num2str(model) '.mat']),'DCM')
    end
end

end
