function plot_network_MainExperiment(adjcency_matrix,c_opt)
% Plots adjacency matrices (e.g., encoding network structure, functional, 
% or effective connectivity) as a graph using MATLAB's digraph function.
% The function allows for various plotting options.
%
% Input:
%   adjcency_matrix   -- adjacency matrix encoding network connectivity
%   c_opt             -- option structure containing relevant infomration
%                        for plotting the network graph. If not specified
%                        the function will generate a default c_opt
%                        structure.
% 
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2020
% Copyright 2020 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% add path to several relevant toolboxes
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'cbrewer')))
addpath(genpath(fullfile(FilenameInfo.Matlab_Path,'append_pdfs')))


% set the colormap
if ( nargin < 2 )
    
    % define colormap
    c_opt.cmap = [1 1 1; 0.6 0.6 0.6];
    
    % limits of colormap
    c_opt.clim = [min(adjcency_matrix(:)) max(adjcency_matrix(:))];
    
    % center coordinates (default: four regions)
    c_opt.centre     = [0.33 0.6; 0.2 0.37; 0.37 0.40; 0.27 0.20];
    
    % define coordinates for text
    c_opt.txt_centre = [0.33 0.64; 0.186 0.37; 0.38 0.40; 0.27 0.16];
    
    % sizes
    c_opt.NodeSize  = 300;
    c_opt.ArrowSize = 20;
    c_opt.EdgeScale = 5;
    
    % color
    c_opt.NodeColor = [0.5725 0.0314 0.1373];
    
end


% set the position of the nodes in the space, each with x and y coordinate
centre = c_opt.centre;

% generate a digraph object with adjacencey matrix
G = digraph(adjcency_matrix');


% set the line width
LWidths = abs(c_opt.EdgeScale*G.Edges.Weight/max(G.Edges.Weight));

% set the colors
if ( ~isfield(c_opt,'EdgeColor'))
    LColors = G.Edges.Weight;
else
    LColors = c_opt.EdgeColor;
end


% plot the digraph
figure
plot(G, 'XData', centre(:,1), 'YData', centre(:,2), 'LineWidth', LWidths, 'EdgeCData', LColors, 'ArrowSize', c_opt.ArrowSize)
axis off
colormap(c_opt.cmap)
caxis(c_opt.clim)
hold on;

% plot nodes
scatter(centre(:,1), centre(:,2), c_opt.NodeSize, c_opt.NodeColor, 'filled', 'MarkerEdgeColor', 'k')

% write the region names
for nodes = 1:size(centre,1)
    text(c_opt.txt_centre(nodes,1),c_opt.txt_centre(nodes,2),num2str(nodes),'FontSize',16)
end

end